<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>::- Hovey Energy -::</title>
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<div class="he_login">
    <div class="login-page sidebar-collapse">
        <div class="content">
            <div class="ml-auto mr-auto">
                <div class="card card-login card-plain">
                    <form class="form" method="" action="">
                      <div class="card-header text-center">
                        <div class="logo">
                          <img src="images/logo-.png" alt="">
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="input-group no-border input-lg">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="fa fa-user"></i>
                            </span>
                          </div>
                          <input type="text" class="form-control" placeholder="First Name...">
                        </div>
                        <div class="input-group no-border input-lg">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="fa fa-key mn_rotate"></i>
                            </span>
                          </div>
                          <input type="text" placeholder="Password..." class="form-control" />
                        </div>
                      </div>
                      <div class="card-footer text-center">
                        <a href="#pablo" class="btn btn-primary btn-round btn-lg btn-block">LOGIN</a>
                          <h6>
                            <a href="#pablo" class="link">Forgot Password?</a>
                          </h6>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-1.12.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>