<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Hovey Energy Tracker Login-::</title>
<%@ include file="/jsp/common/common.jsp"%>

</head>
<body>
<div class="he_login">
    <div class="login-page sidebar-collapse">
        <div class="content">
            <div class="ml-auto mr-auto">
                <div class="card card-login card-plain">
                   <form class="login100-form validate-form" action="<c:url value= '/login' />"  method='POST'>
                   <c:if test="${ not empty status}">
                    	<div class="text-center p-t-136 success_color">
                        <p>${status}</p>
                        </div>
                    </c:if>
                    <c:if test="${empty status && not empty error}">
                    	<div class="text-center p-t-136 success_color">
                        <p>${error}</p>
                        </div>
                    </c:if>
                      <div class="card-header text-center">
                        <div class="logo">
                          <img src="${pageContext.request.contextPath}/images/logo-.png" alt="">
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="input-group no-border input-lg">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="fa fa-user"></i>
                            </span>
                          </div>
                          <input type="text" class="form-control" name="username"placeholder="Username">
                        </div>
                        <div class="input-group no-border input-lg">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="fa fa-key mn_rotate"></i>
                            </span>
                          </div>
                          <input type="password" placeholder="Password" name="password" class="form-control" />
                        </div>
                      </div>
                      <div class="card-footer text-center">
                        <button class="btn btn-primary btn-round btn-lg btn-block">LOGIN</button>
                          <h6>
                            <a href="${pageContext.request.contextPath}/admin/forgotpassword.do" class="link">Forgot Password?</a>
                          </h6>
                   
                </div>
                 </form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>