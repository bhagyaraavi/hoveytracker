<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Congratulations -::</title>
<%@ include file="/jsp/common/common.jsp"%>
</head>
<body>
<div class="he_login">
	
	<!-- Page Header -->
		<%@ include file="/jsp/common/adminHeader.jsp"%>
	
	<!--  END of Page Header -->
	
    <div class="mn_edit">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-5">
                <input id="userType" type="hidden" value="${userType}">
                    <div class="mn_edit_profile mn_congrats">
                      <img src="${pageContext.request.contextPath}/images/congratulations.gif" alt="">

                      <audio  id="playAudio" loop autoplay >
                          <source src="${pageContext.request.contextPath}/images/congratss.ogg" type="audio/ogg">
                          <source src="${pageContext.request.contextPath}/images/congratss.mp3" type="audio/mp3">
                      </audio>
                      <iframe src="${pageContext.request.contextPath}/images/congratss.mp3" allow="autoplay" style="display:none" id="iframeAudio">
                      </iframe> 
                      <embed src="${pageContext.request.contextPath}/images/congratss.mp3" loop="true" autostart="true" width="2" height="0">
                   </embed>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<script>
    setTimeout(function(){
    	var userType=$('#userType').val();
    	if(userType=="Fronter"){
    		 window.location.href = '${pageContext.request.contextPath}/user/fronterboard.do';
    	}
    	else{
    		 window.location.href = '${pageContext.request.contextPath}/user/closerboard.do';
    	}
       
    }, 6000);
</script>
<script type="text/javascript">
   var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
  if (!isChrome){
      $('#iframeAudio').remove()
  }

  else {
      $('#playAudio').remove() // just to make sure that it will not have 2x audio in the background 
  }
</script>
</body>
</html>