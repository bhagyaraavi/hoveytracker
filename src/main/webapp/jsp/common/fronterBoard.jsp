<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Fronter Board -::</title>
<%@ include file="/jsp/common/common.jsp"%>
<script src="${pageContext.request.contextPath}/js/user/user.js"></script>

<script>

$(document).ready(function(){
		var sortBy=$('#sortBy').val();
		var sortDirection=$('#sortDirection').val();
		if(sortDirection=='true'){
			jQuery("#user_name,#mon,#tues,#wed,#thurs,#fri,#total").removeClass('sorting').addClass('sorting_asc');
			
		}
		else{
			jQuery("#user_name,#mon,#tues,#wed,#thurs,#fri,#total").removeClass('sorting').addClass('sorting_desc');
		}
		
});
</script>
</head>
<body>
<div class="he_login mn_fronterboard_logo">

   	<!-- Page Header -->
		<%@ include file="/jsp/common/adminHeader.jsp"%>
	
	<!--  END of Page Header -->

   
    <div class="mn_edit">
        <div class="container">
            <div class="row">
            	<div class="col-md-12">
		                      <h2>Fronter Board</h2>
		         </div>
                
                <div class="col-md-12">
                
                	<form:form commandName="displayListBean" method="POST" class="form-horizontal" action="${pageContext.request.contextPath}/user/fronterboard.do" role="form">
								<form:hidden path="sortBy" value="${displayListBean.sortBy}" id="sortBy" />
								<form:hidden path="searchBy" />
								<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection" />
								<form:hidden path="pagerDto.range" />
								<form:hidden path="pagerDto.pageNo" />

								
								
	                		<input type="hidden" id="adminUserId" value="${userId}">
		                	<div class="mn_edit_profile">
		                      
								<div class="row">
								<div class="col-md-4 col-sm-4 col-4">
                  <div class="mn_week">
                    <label>LastWeek</label>
                    <c:choose>
		               <c:when test="${userType eq 'Admin' }">
		                	<textarea class="form-control" id="lastWeek"rows="2"  onchange="updateFrontersBoardData();">${frontersBoardDataDto.lastWeek}</textarea>
		                	 <span id="lastWeek_error" class="error_msg"></span>
		                </c:when>
		                <c:otherwise>
		                	<textarea class="form-control" rows="2"  readonly="readonly">${frontersBoardDataDto.lastWeek}</textarea>
		                </c:otherwise>
		            </c:choose>
                    
                  </div>
                  
                </div>
                <div class="col-md-4 col-sm-4 col-4">
                 <div class="mn_week">
                    <label>Week#</label>
                   <c:choose>
		                <c:when test="${userType eq 'Admin' }">
		                	<textarea id="weekNum" class="form-control" rows="2"  onchange="updateFrontersBoardData();">${frontersBoardDataDto.weekNum}</textarea>
		                	 <span id="weekNum_error" class="error_msg"></span>
		                </c:when>
		                <c:otherwise>
		                	<textarea   class="form-control" rows="2"  readonly="readonly">${frontersBoardDataDto.weekNum}</textarea>
		                </c:otherwise>
		            </c:choose>
                   
                  </div>
                </div>
                <div class="col-md-4  col-sm-4 col-4">
                  <div class="mn_week">
                    <label>Week'sGoal</label>
                    <c:choose>
		                <c:when test="${userType eq 'Admin' }">
		                	<textarea id="weekGoal" class="form-control" rows="2"  onchange="updateFrontersBoardData();">${frontersBoardDataDto.weekGoal}</textarea>
		                	<span id="weekGoal_error" class="error_msg"></span>
		                </c:when>
		                <c:otherwise>
		                	<textarea class="form-control" rows="2"  readonly="readonly">${frontersBoardDataDto.weekGoal}</textarea>
		                </c:otherwise>
		            </c:choose>
                    
                  </div>
                </div>
                </div>
                		<c:set var="range" value="${displayListBean.pagerDto.range}" />
								<!-- SELECT  PAGE RANGE -->
								<%-- <div class="row">
									<div class="col-xl-6 col-lg-5 col-md-4 col-sm-12">
										<div class="dataTables_length" id="dataTableId_length" 	style="margin-left: 10px;">
											<label style="font-weight: 900;">Show 
											<select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
													<option value="10" ${range=='10' ? 'selected' : '' }>10</option>
													<option value="25" ${range=='25' ? 'selected' : '' }>25</option>
													<option value="50" ${range=='50' ? 'selected' : '' }>50</option>
													<option value="100" ${range=='100' ? 'selected' : '' }>100</option>
											</select> entries
											</label>
										</div>
									</div>
									<!-- END OF SELECT PAGE RANGE -->
									<!-- SEARCH DIVISION -->
									<div class="col-xl-6 col-lg-7 col-md-8 col-sm-12 mn_padding">
										<div class="row">
											<div class="col-lg-2 col-md-3 col-sm-12 no_pad_left">
											<label>Search By:</label>
											</div>
											<div class="col-lg-5 col-md-4 col-sm-6 no_pad_left">
												<input type="text" id="searchText" name="searchText" placeholder="Search for fronters "	value="${displayListBean.searchBy}">
											</div>
											
									<div class="col-lg-5 col-md-5 col-sm-6 no_pad_left">
									
											<div class="col-6 no_pad_left pull-right">
												<button class="btn mn_none" type="button"
													onclick="window.location='${pageContext.request.contextPath}/user/fronterboard.do'">Reset</button>
											</div>
											<div class="col-6 no_pad_left pull-right">
												<button class="btn mn_none1" type="button" value="Search"
													onclick="searchResults();">Search</button>
											</div>
									</div>
										</div>
									</div>
								</div> --%>
								<!-- END OF Search divison -->
								<c:choose>
                				<c:when test="${empty message }">	
		                      <div class="mn_fronter">
		                     <!--  <table id="example" class="table  table-striped " cellspacing="0" width="100%"> -->
		                     <table id="dataTableId" class="table dataTable table-striped"	role="grid" aria-describedby="dataTableId_info"	 cellspacing="0" width="100%">
		                      <thead>
		                        <tr>
		                        <c:choose>
		                         <c:when test="${userType eq 'Admin' }">
		                          <th class="sorting" id="user_name" onclick="sortResults('username')">Name</th>
		                          <th class="sorting" id="mon" onclick="sortResults('mon')">Mon</th>
		                          <th class="sorting" id="tues" onclick="sortResults('tues')">Tue</th>
		                          <th class="sorting" id="wed" onclick="sortResults('wed')">Wed</th>
		                          <th class="sorting" id="thurs" onclick="sortResults('thurs')">Thu</th>
		                          <th class="sorting" id="fri" onclick="sortResults('fri')">Fri</th>
		                          <th class="sorting" id="total" onclick="sortResults('total')">Total</th>
		                         
		                          <th id="delete_fronter">Delete</th>
		                          </c:when>
		                          <c:otherwise>
		                          	<th id="user_name" >Name</th>
		                          	<th  id="mon" >Mon</th>
		                          	<th  id="tues">Tue</th>
		                          	<th  id="wed">Wed</th>
		                          	<th  id="thurs">Thu</th>
		                          	<th  id="fri">Fri</th>
		                          	<th  id="total">Total</th>
		                          </c:otherwise>
		                          </c:choose>
		                          
		                        </tr>
		                      </thead>
		
		                      <tbody>
		                      	<c:forEach var="fronterDto" items="${fronterDtos}">
		                      		 <tr>
			                          <td>${fronterDto.userDto.username}</td>
			                          <c:choose>
			                          	<c:when test="${userType eq 'Admin' }">
			                          		 <td><input id="monCount${fronterDto.fronterId}" onchange="editFronterBoard(${fronterDto.fronterId},'MONDAY',this.value);" type="" name="" value="${fronterDto.mon}"></input></td>
					                          <td><input id="tuesCount${fronterDto.fronterId}" onchange="editFronterBoard(${fronterDto.fronterId},'TUESDAY',this.value);" type="" name="" value="${fronterDto.tues}"></td>
					                          <td><input id="wedCount${fronterDto.fronterId}" onchange="editFronterBoard(${fronterDto.fronterId},'WEDNESDAY',this.value);" type="" name="" value="${fronterDto.wed}"></td>
					                          <td><input id="thursCount${fronterDto.fronterId}" onchange="editFronterBoard(${fronterDto.fronterId},'THURSDAY',this.value);" type="" name=""  value="${fronterDto.thurs}"></td>
					                          <td><input id="friCount${fronterDto.fronterId}" onchange="editFronterBoard(${fronterDto.fronterId},'FRIDAY',this.value);" type="" name=""  value="${fronterDto.fri}"></td>
					                          <td><strong>${fronterDto.total}</strong></td>
			                          	      <td id="delete_Fronter_${fronterDto.fronterId}"><a style="font-size:20px;position:relative;top:-5px;" onclick="deleteFronter(this,${fronterDto.fronterId});"><i class="btn mn_none1 fa fa-trash" aria-hidden="true"></i> </a>	</td>
			                          	</c:when>
			                          	<c:otherwise>
			                          		 <td>${fronterDto.mon}</td>
			                          		 <td>${fronterDto.tues}</td>
			                          		 <td>${fronterDto.wed}</td>
			                          		 <td>${fronterDto.thurs}</td>
			                          		 <td>${fronterDto.fri}</td>
			                          		 <td><strong>${fronterDto.total}</strong></td>
			                          		 
			                          	</c:otherwise>
			                          </c:choose>
			                         	
			                          
			                          	
			                        </tr>
		                      	</c:forEach>
		                        
		                      </tbody>
		                      <c:if test="${empty displayListBean.searchBy }">
		                      <tfoot>
		                        <th>Total</th>
		                        <th>${frontersGrandTotalDto.monTotal}</th>
		                        <th>${frontersGrandTotalDto.tuesTotal}</th>
		                        <th>${frontersGrandTotalDto.wedTotal}</th>
		                        <th>${frontersGrandTotalDto.thursTotal}</th>
		                        <th>${frontersGrandTotalDto.friTotal}</th>
		                        <th>${frontersGrandTotalDto.grandTotal}</th>
		                      </tfoot>
		                      </c:if> 
		                      
		                    </table>
		                    <div class="clearfix"></div>
												
												<c:set var="first" value="0" />
												<c:set var="end"
													value="${displayListBean.pagerDto.pagesNeeded }" />
												<c:set var="page" value="${displayListBean.pagerDto.pageNo}" />
												<c:set var="total"
													value="${displayListBean.pagerDto.totalItems}" />
												<c:set var="firstResult"
													value="${displayListBean.pagerDto.firstResult}" />
											<c:set var="lastResult"
													value="${displayListBean.pagerDto.lastResult}" />
												<div class="dataTables_info" id="dataTableId_info"
													role="status" aria-live="polite">Showing
													${firstResult} to ${lastResult} of ${total} entries</div>
												<div class="dataTables_paginate paging_simple_numbers"
													id="dataTableId_paginate">
	
													<%@ include file="/jsp/common/pager.jsp"%>
	
												</div>
		                    </div>
		                    	</c:when>
				                	<c:otherwise>
				                		<h2>${message}</h2>
				                	</c:otherwise>
				                </c:choose>
	                  		</div>
	                  		</form:form>
	                
	                  
	                </div>
	            </div>
	        </div>
	    </div> 
	</div>
	
	</body>
	</html>