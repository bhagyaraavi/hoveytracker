<nav class="navbar navbar-expand-xl navbar-light">
        <a href="" class="navbar-brand"><img src="images/logo-.png" alt=""></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav topBotomBordersOut mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Dashboard</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              ScoreBoard Management
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <li><a class="dropdown-item" href="#">Daily ScoreBoard</a></li>
                <li><a class="dropdown-item" href="#">Closer board</a></li>
                <li><a class="dropdown-item" href="#">Fronter board</a></li>
                <li><a class="dropdown-item" href="#">Message Center</a></li>
            </ul>
          </li>
        </ul>

        <ul class="navbar-nav mn_right">
            <li class="nav-item"><a href="#" class="nav-link" title="Profile"><i class="fa fa-user"></i>&nbsp;&nbsp; Hi Admin</a></li>
            <li class="nav-item dropdown"><a href="#"  class="nav-link dropdown-toggle" id="swich" title="Settings"><i class="fa fa-cog"></i></a>
                <ul class="dropdown-menu mn_r" id="switch_on">
                    <li><a class="dropdown-item" href="#">
                      <div class="form-group" data-key="disabled">
                          <label class="control-label" for="input-model">Score Notifications</label>
                          <div class="input">
                              <div class="toggle">         
                                  <input type="checkbox" class="toggle-checkbox" id="disabled">   
                                  <label class="toggle-label" for="disabled">    
                                      <span class="toggle-inner"></span>
                                      <span class="toggle-switch"></span>
                                  </label>
                              </div>
                          </div> 
                      </div>
                      </a></li>
                      <li><a class="dropdown-item" href="#">
                      <div class="form-group" data-key="disabled">
                          <label class="control-label" for="input-model">Message Notifications</label>
                          <div class="input">
                              <div class="toggle">         
                                  <input type="checkbox" class="toggle-checkbox" id="disabled1">   
                                  <label class="toggle-label" for="disabled1">    
                                      <span class="toggle-inner"></span>
                                      <span class="toggle-switch"></span>
                                  </label>
                              </div>
                          </div> 
                      </div>
                      </a></li>
                  </ul>
              </li>
              <li class="nav-item"><a href="#" class="nav-link" title="Profile"><i class="fa fa-power-off"></i></a></li>
        </ul>
      </div>
    </nav>