<html>
    <head>
        <title>::- Message Center -::</title>
        <%@ include file="/jsp/common/common.jsp"%>
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/user/user.js"></script>
        <script type="text/javascript">
        var oldDataSize=0;
        var currentDataSize=0;
        window.onload = function(e){ 
        	 $('#message').focus();
        	 scrollBottom();
        	
        	}
        function scrollBottom() {
        	  var elmnt = document.getElementById("chat_div");
        	  elmnt.scrollTop = elmnt.scrollHeight;
        	}
      
     function getMessages(){
    	 
        $.getJSON('/hoveytracker/user/getmessages.do', function(data) {
        	var userId=$('#userId').val();
        	var rowIndex=1;
        	$('#rowIndex').val(rowIndex);
        	jQuery('#chat_div').html('');// clear the chat before refresh  the new data
        	currentDataSize=data.length;
            $.each(data, function(index) {
            	
            	if(userId==data[index].userId){
            		$('#rowIndex').val(rowIndex);
            		
            		 
            		$('#chat_div').append("<div class=d-flex justify-content-start mt-4 mb-4 id=message_div_"+rowIndex+" >")
            		$('#message_div_'+rowIndex).append("<div class=msg_cotainer id=message_inner_div_"+rowIndex+">")
            		
            		$('#message_inner_div_'+rowIndex).append(" <p id=username_"+rowIndex+"_"+data[index].userId+">"+data[index].firstName+" "+data[index].lastName+"</p>")
            		$('#message_inner_div_'+rowIndex).append(" <span class=msg_time id=message_date_time_"+rowIndex+"_"+data[index].userId+">"+data[index].messageDateTime+"</span> ")
            		$('#message_inner_div_'+rowIndex).append(" <span class=text_message id=message_"+rowIndex+"_"+data[index].userId+">"+data[index].message+"</span>")
            		$('#message_inner_div_'+rowIndex).append(" </div>")
            		$('#message_div_'+rowIndex).append("</div>")
            		
            		rowIndex++;
            	}
            	else{ 
            		$('#rowIndex').val(rowIndex);
            		$('#chat_div').append(" <div class=d-content-end-flex id=message_div_"+rowIndex+" >")
            		$('#message_div_'+rowIndex).append("<div class=msg_cotainer_send id=message_sender_div_"+rowIndex+">")
            		
            		
            		$('#message_sender_div_'+rowIndex).append(" <span class=msg_time_send id=message_date_time_"+rowIndex+"_"+data[index].userId+">"+data[index].messageDateTime+"</span> ")
            		$('#message_sender_div_'+rowIndex).append(" <p id=username_"+rowIndex+"_"+data[index].userId+">"+data[index].firstName+" "+data[index].lastName+"</p>")
            		$('#message_sender_div_'+rowIndex).append(" <span class=text_message_sender id=message_"+rowIndex+"_"+data[index].userId+">"+data[index].message+"</span>")
            		
            		$('#message_sender_div_'+rowIndex).append(" </div>")
            		$('#message_div_'+rowIndex).append("</div>")
            		
            		rowIndex++;
            	}
            	
            });
         //scrolling to bottom only whenever data is updated because remaining time if user reading the middle of the page messages means,he should saty in that position only.
            if(currentDataSize!=oldDataSize){  
            	scrollBottom();
            	$('#message').focus();
            }
            oldDataSize=data.length
           
        });
      
       
     }
     
     
      setInterval(function(){ 
      		getMessages();
        
        }, 1000);
       
      getMessages();
        
       
        </script>
        <style>
        	.mn_msg_center_fixed .navbar{
        		position:fixed;
        		width:100%;
        		background:#000;
        		z-index:99;
        	}
        	.mn_edit{
        		margin-top:120px
        	}
        </style>
   </head>
   
    <body>
<div class="he_login mn_msg_center_fixed">

    
    <!-- Page Header -->
		<%@ include file="/jsp/common/adminHeader.jsp"%>
	
	<!--  END of Page Header -->
    
    <div class="mn_edit">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mn_edit_profile">
                        <h2>Message Center</h2>
                        
                        <div class="card scrollbar" >
                          <input type="hidden" id="userId" value="${userId}">
                          <div  id="chat_div" class="card-body msg_card_body">
                         
                          
			                           
		                        
                          
                          </div>
                           
                          <div class="card-footer">
                          
                            <div class="input-group">
                              <div class="input-group-append">
                                <span class="input-group-text attach_btn"><i class="fa fa-paperclip"></i></span>
                              </div>
                              <textarea name="" id="message" class="form-control type_msg" placeholder="Type your message..."></textarea>
                              <div class="input-group-append" >
                                <span class="input-group-text send_btn" ><i class="fa fa-location-arrow"  onclick="saveMessage();"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

</body>
</html>