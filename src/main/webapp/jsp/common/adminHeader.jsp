

<nav class="navbar navbar-expand-xl navbar-light nav">
		<input  type="hidden" id="userId" value="${userId}">
        <a href="" class="navbar-brand"><img src="${pageContext.request.contextPath}/images/logo-.png" alt=""></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav topBotomBordersOut mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/dashboard.do">Dashboard</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              ScoreBoard Management
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <li><a class="dropdown-item" href="${pageContext.request.contextPath}/admin/dailyscoreboard.do">Daily ScoreBoard</a></li>
                <li><a class="dropdown-item" href="${pageContext.request.contextPath}/user/closerboard.do">Closer ScoreBoard</a></li>
                <li><a class="dropdown-item" href="${pageContext.request.contextPath}/user/fronterboard.do">Fronter ScoreBoard</a></li>
                <li><a class="dropdown-item" href="${pageContext.request.contextPath}/user/viewmessages.do">Message Center</a></li>
            </ul>
          </li>
          <c:if test="${userType eq 'Admin' }">
	          <li class="nav-item dropdown">
	            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	              User Management
	            </a>
	            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
	                <li><a class="dropdown-item" href="${pageContext.request.contextPath}/admin/userregister.do">Create User</a></li>
	                <li><a class="dropdown-item" href="${pageContext.request.contextPath}/admin/viewusers.do">Manage Users</a></li>
	            </ul>
	          </li>
          </c:if>
          <c:if test="${userType eq 'Admin' }">
	          <li class="nav-item dropdown">
	            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	              Profile Management
	            </a>
	            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
	                <li><a class="dropdown-item" href="${pageContext.request.contextPath}/admin/editprofile.do">Edit Profile</a></li>
	                <li><a class="dropdown-item" href="${pageContext.request.contextPath}/admin/adminresetpassword.do">Reset Password</a></li>
	            </ul>
	          </li>
          </c:if>
           <c:if test="${userType != 'Admin' }">
          <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/user/dealclose.do">Deal Close</a>
          </li>
          </c:if>
        </ul>

        <ul class="navbar-nav mn_right">
            <li class="nav-item"><a href="#" class="nav-link" title="Profile"><i class="fa fa-user"></i>&nbsp;&nbsp; Hi ${username}</a></li>
            <li class="nav-item dropdown nopad-left"><a href="#"  class="nav-link dropdown-toggle" id="swich" title="Settings"><i class="fa fa-cog"></i></a>
                <ul class="dropdown-menu mn_r" id="switch_on">
                    <li><a class="dropdown-item" href="#">
                      <div class="form-group" data-key="disabled">
                          <label class="control-label" for="input-model">Score Notifications</label>
                          <div class="input">
                              <div class="toggle">         
                                  <input type="checkbox" class="toggle-checkbox" id="disabled" onchange="notificationConfiguration()">   
                                  <label class="toggle-label" for="disabled">    
                                      <span class="toggle-inner"></span>
                                      <span class="toggle-switch"></span>
                                  </label>
                              </div>
                          </div> 
                      </div>
                      </a></li>
                      <li><a class="dropdown-item" href="#">
                      <div class="form-group" data-key="disabled">
                          <label class="control-label" for="input-model">Message Notifications</label>
                          <div class="input">
                              <div class="toggle">         
                                  <input type="checkbox" class="toggle-checkbox" id="disabled1" onchange="notificationConfiguration()">   
                                  <label class="toggle-label" for="disabled1">    
                                      <span class="toggle-inner"></span>
                                      <span class="toggle-switch"></span>
                                  </label>
                              </div>
                          </div> 
                      </div>
                      </a></li>
                  </ul>
              </li>
              <li class="nav-item"><a href="${pageContext.request.contextPath}/logoutsuccess.do" class="nav-link" title="Logout"><i class="fa fa-power-off"></i></a></li>
        </ul>
      </div>
      
            	<div class="icon-badge-container">
        			<a href="${pageContext.request.contextPath}/user/viewmessages.do"><i class="fa fa-bell notification-bell" id="bell" ></i></a>
        			<div  id="notificationCount" class="icon-badge"></div>
    			</div>
    </nav>
    
    <script type="text/javascript">
 
  var userId = $('#userId').val();
  /*  function to get notification configuration when the page loads*/
  $.ajax({
		url:"/hoveytracker/user/getnotificationconfiguration.do",
		type:'GET',
		async:false,
		data:{
			userId:userId
			
		},
		success:function(data){
			
			//Returning data to controller in json object format 
			
			var obj = JSON.parse(data);
			
			if(obj.scoreNotification=="false"){
				$('#disabled').attr('checked', false);
			}
			else{
				$('#disabled').attr('checked', true);
			}
			
			if(obj.messageNotification=="false"){
				$('#disabled1').attr('checked', false);
			}
			else{
				$('#disabled1').attr('checked', true);
			}
				
								
		},
	complete:{
	}
	});		
	$.ajaxSetup({async: true});
  /**
  * Timer function - for to fetch the notification count by every second
  * Added by bhagya on JUne 22nd, 2021
  */
	 setInterval(function(){ 
		 getUserNotificationCount();
     	
     }, 1000); // fetch the notification count value by every second
    
   getUserNotificationCount(); // calling the function on page load
   
   
/**
 * Created By BHagya on JUne 22nd, 2021
 * function for to get the user notification count
 */
   function getUserNotificationCount(){
 	$.ajax({
 		url:"/hoveytracker/user/getnotificationcount.do",
 		type:'GET',
 		async:false,
 		data:{
 			userId:userId
 			
 		},
 		success:function(data){
 			
 			var obj = JSON.parse(data);
 			//alert(" count "+obj.notificationCount)
 			$('#notificationCount').html('');
 			$('#notificationCount').append(obj.notificationCount)
 			
 								
 		},
 	complete:{
 	}
 	});		
 	$.ajaxSetup({async: true});
   }
   
   
/* Function to save or update the notification configuration */
  function notificationConfiguration(){
	  var userId = $('#userId').val();
	  var scoreNotification = jQuery('#disabled').is(':checked')?true:false;
	  var messageNotification = jQuery('#disabled1').is(':checked')?true:false;
	
	  //Ajax function to save the notification configuration data from controller
  	$.ajax({
		url:"/hoveytracker/user/savenotificationconfiguration.do",
		type:'GET',
		async:false,
		data:{
			userId:userId,
			scoreNotification:scoreNotification,
			messageNotification:messageNotification
		},
		success:function(data){
			if(data==="success"){
				//alert(" Notfication Configuration Updated Successfully");
			}		
								
		},
	complete:{
	}
	});		
	$.ajaxSetup({async: true});
  }
  //function ends here
  
</script>
<script type="text/javascript">
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
  if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
  }
  var $subMenu = $(this).next(".dropdown-menu");
  $subMenu.toggleClass('show');


  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-submenu .show').removeClass("show");
  });


  return false;
});
    
    $(document).ready(function() {
        $("#swich").click(function(e) {
            $("#switch_on").toggle();
            e.stopPropagation();
        });
        $(document).click(function(e) {
            if (!$(e.target).is('#switch_on, #switch_on *')) {
                $("#switch_on").hide();
            }
        });
    });
</script>
