<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Hovey Energy -::</title>
<%@ include file="/jsp/common/common.jsp"%>
</head>
<body>
<div class="he_login">

   
    <div class="mn_edit">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-5">
                <c:if test="${ not empty status}">
				     <div id="login_head_text" class="status">
							${status}
					 </div> 
		  		</c:if>
				<c:if test="${empty status && not empty error}">
					<div id="login_head_text" class="status" style="color:#FF0000;">
						${error}
					</div>
		  		</c:if>	
                <form class="login100-form" action="${pageContext.request.contextPath}/admin/forgotpassword.do" method = "POST" >
                    <div class="mn_edit_profile">
                        <h2>Forgot your Password?</h2>
                        <p>Enter your email address and we will send you instructions to reset your password</p>
                        <div class="card-body row">
                            <div class="input-group no-border input-lg col-md-12">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-lock"></i>
                                </span>
                              </div>
                              <input type="text" class="form-control" name="emailId" placeholder="Email Address...">
                            </div>
                          </div>
                          <div class="card-footer">
                            <div class="row justify-content-center align-items-center">
                                <button class="btn btn-primary mb-1 mr-2 btn-round btn-lg">Forgot Password</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>

</body>
</html>