<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Reset/Change Password -::</title>
<%@ include file="/jsp/common/common.jsp"%>
<script src="${pageContext.request.contextPath}/js/admin/admin.js"></script>

</head>
<body>
<div class="he_login">

	<!-- Page Header -->
		<%@ include file="/jsp/common/adminHeader.jsp"%>
	
	<!--  END of Page Header -->

    <div class="mn_edit">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-5">
                <c:if test="${ not empty status}">
                    	<div class="text-center p-t-136 success_color">
                        <p>${status}</p>
                        </div>
                    </c:if>
                    <c:if test="${empty status && not empty error}">
                    	<div class="text-center p-t-136 success_color">
                        <p>${error}</p>
                        </div>
                    </c:if>
				<form class="login100-form " action="${pageContext.request.contextPath}/admin/adminresetpassword.do" method = "POST" onsubmit="return validateResetPassword();">
                    <div class="mn_edit_profile">
                        <h2>Reset Password</h2>
                        <div class="card-body row">
                            <div class="input-group no-border input-lg col-md-12">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-lock"></i>
                                </span>
                              </div>
                              <input type="password" id="password" class="form-control"  name="password" placeholder="Password...">
                               <span id="password_error" class="error_msg"></span>
                            </div>
                            <div class="input-group no-border input-lg col-md-12">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-lock"></i>
                                </span>
                              </div>
                              <input type="password" id="cpassword" class="form-control" name="confirmPassword" placeholder="Confirm Password...">
                               <span id="cpassword_error" class="error_msg"></span>
                            </div>
                          </div>
                          <input type="hidden" name="userId" value="${userId}" />
                          <div class="card-footer">
                            <div class="row justify-content-center align-items-center">
                                <button class="btn btn-primary mb-1 mr-2 btn-round btn-lg">Reset Password</button>
                               
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>

</body>
</html>