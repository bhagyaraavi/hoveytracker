<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Reset/Change Password -::</title>
<%@ include file="/jsp/common/common.jsp"%>

</head>
<body>
<div class="he_login">

	<!-- Page Header -->
		<%-- <%@ include file="/jsp/common/adminHeader.jsp"%> --%>
	
	<!--  END of Page Header -->

    <div class="mn_edit">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-5">
                <c:if test="${ not empty status}">
					     <div id="login_head_text" class="status">
								${status}
						 </div>
					  </c:if>
					<c:if test="${empty status && not empty error}">
					<div id="login_head_text" class="status" style="color:#FF0000;">
						${error}
					</div>
				  </c:if>
				<form class="login100-form " action="${pageContext.request.contextPath}/admin/resetpassword.do" method = "POST" >
                    <div class="mn_edit_profile">
                        <h2>Reset Password</h2>
                        <div class="card-body row">
                            <div class="input-group no-border input-lg col-md-12">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-lock"></i>
                                </span>
                              </div>
                              <input type="password" class="form-control"  name="password" placeholder="Password...">
                            </div>
                            <div class="input-group no-border input-lg col-md-12">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-lock"></i>
                                </span>
                              </div>
                              <input type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password...">
                            </div>
                          </div>
                          <input type="hidden" name="userId" value="${userId}" />
                          <div class="card-footer">
                            <div class="row justify-content-center align-items-center">
                                <button class="btn btn-primary mb-1 mr-2 btn-round btn-lg">Reset Password</button>
                                <a href="${pageContext.request.contextPath}/login.do" class="btn btn-danger mr_3 mb-1 btn-round btn-lg">Cancel</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>

</body>
</html>