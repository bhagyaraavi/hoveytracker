<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Daily Scoreboard -::</title>
<%@ include file="/jsp/common/common.jsp"%>
<script src="${pageContext.request.contextPath}/js/admin/admin.js"></script>

</head>
<body>
<div class="he_login">
	<!-- Page Header -->
		<%@ include file="/jsp/common/adminHeader.jsp"%>
	
	<!--  END of Page Header -->
   
    <div class="mn_edit">
        <div class="container">
            <div class="row">
            <div class="col-md-12">
            
                      <h2>Daily Scoreboard</h2>
                      </div>
                <div class="col-md-12">
                  <div class="mn_edit_profile">
                      <input type="hidden" id="userId" value="${userId}"></input>
                      <input type="hidden" id="userType" value="${userType}"></input>
                      <div class="row justify-content-center align-items-center">
                        <div class="col-lg-4 col-md-6">
                          <div class="mn_time_left">
                            <h1>Time Left</h1>
                            <div id="countdown">
                              <div id='tiles'></div>
                              <!-- <div class="labels">
                                <li></li>
                                <li></li>
                              </div> -->
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row justify-content-center align-items-center">
                        <div class="col-md-4">
                          <div class="mn_time_left">
                            <h1>DEALS</h1>
                            <div class="mn_deals">
                            <c:choose>
                            	<c:when test="${userType eq 'Admin' }">
                            		<h2><input type="" name="" id="dealsCount" value="${dealsCount}" class="count" onchange="editDailyScoreBoard();"></h2>
                            	</c:when>
                            	<c:otherwise>
                            		<h2><input type="" name="" id="dealsCount" value="${dealsCount}" class="count"  readonly="readonly"></h2>
                            	</c:otherwise>
                            </c:choose>
          						
                            </div>
                            <div class="qty mt-2">
                            <c:choose>
                            <c:when test="${userType eq 'Admin' }">
                        		<span class="minus bg-dark">-</span>
                        		 <p>Add/Remove Deal</p>
                        		 <span class="plus bg-dark">+</span>
                        	</c:when>
                        	<c:otherwise>
                        		<c:if test="${userType eq 'Closer' }">
		                        	<p>Add Deal</p>
		                        	<span class="plus bg-dark">+</span>
	                        	</c:if>
                        	</c:otherwise>
                        	</c:choose>
                        	
                    	</div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="mn_time_left">
                            <h1>Quarter</h1>
                            <div class="mn_deals">
                              <h2><input type="" name="" id="quarter" value="00" readonly="readonly"></h2>
                            </div>
                            <div class="mt-25_m"></div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="mn_time_left">
                            <h1>XFERS</h1>
                            <div class="mn_deals">
                            <c:choose>
                            	<c:when test="${userType eq 'Admin' }">
                            		 <h2><input type="" name="" id="xfersCount" value="${xfersCount}" class="xfercount" onchange="editDailyScoreBoard();"></h2>
                            	</c:when>
                            	<c:otherwise>
                            		 <h2><input type="" name=""  id="xfersCount" value="${xfersCount}"  class="xfercount" readonly="readonly"></h2>
                            	</c:otherwise>
                            </c:choose>
                            
                            </div>
                            <div class="qty mt-2">
                            <c:choose>
                            <c:when test="${userType eq 'Admin' }">
                        		<span class="xferminus bg-dark">-</span>
                        		<p>Add/Remove Xfer</p>
                        		 <span class="xferplus bg-dark">+</span>
                        	</c:when>
                        	<c:otherwise>
                        		<c:if test="${userType eq 'Fronter' }">
	                        		<p>Add Xfer</p>
	                        		 <span class="xferplus bg-dark">+</span>
                        		 </c:if>
                        	</c:otherwise>
                        	</c:choose>
                       
                    	</div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div> 
</div>

<script type="text/javascript">
	
	var countdown = document.getElementById("tiles"); // get tag element
	
	getCountdown();	
	
	setInterval(function () { getCountdown(); }, 1000);
	
  function getCountdown(){
	  $('#quarter').val('00');

		var startDateTime=new Date();
		//startDateTime.setHours(08);
		//startDateTime.setMinutes(00);
		//startDateTime.setSeconds(00);
		
		//Development Server - Maintaining IST time to see in as per CST time (appending +5:30 to IST time)
		// Development server is kns server - the time is in IST ,so we are converting
		// Production server no need to maintain..because the CST timezone is already available in server
		startDateTime.setHours(18);
		startDateTime.setMinutes(30);
		startDateTime.setSeconds(00);
	

		
		//var endDateTime=new Date();
		//endDateTime.setHours(16);
		//endDateTime.setMinutes(00);
		//endDateTime.setSeconds(00);
		
		//Development Server - Maintaining IST time to see in as per CST time (appending +5:30 to IST time)
		var today=new Date();
		var endDateTime=new Date(today);
		endDateTime.setDate(endDateTime.getDate()+1) //tomorrow Date
		endDateTime.setHours(2);
		endDateTime.setMinutes(30);
		endDateTime.setSeconds(00);
		
		
		var startTime=startDateTime.getTime();
		var endTime = endDateTime.getTime();
		// Get today's date and time
	  	var currentTime = new Date().getTime();
	 
		  if(startTime<=currentTime){
			 
		  	 // Find the distance between now and the end time
				  var distance = endTime - currentTime;
				
				  // Time calculations for days, hours, minutes and seconds
				  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				  
				  if(hours<10){
				  	hours="0"+hours;
				  }
				  if(minutes<10){
				  	minutes="0"+minutes;
				  }
				  // These quarters consideration based on client requiremnt
				  if(hours==08 || hours>06){
				  	 $('#quarter').val('01');
				  }
				  else if(hours<=06 || hours>04){
				  	 $('#quarter').val('02');
				  }
				  else if(hours<=04 || hours>02){
				  	$('#quarter').val('03');
				  }
				  else if(hours<=02){
				  	 $('#quarter').val('04');
				  }
				  
				
				  // Display the result in the element with id="demo"
				 // format countdown string + set tag value
		  			countdown.innerHTML = "<span>" + hours + "</span><span>" + minutes + "</span>"; 
				
				  // If the count down is finished, write some text
				  if (distance < 0) {
				    countdown.innerHTML = "<span>" + "00" + "</span><span>" + "00" + "</span>";
				    $('#quarter').val('00');
				  }
		  }
		  else{
		  	countdown.innerHTML = "<span>" + "00" + "</span><span>" + "00" + "</span>";
		  	$('#quarter').val('00');
		  }
	} //function
	
	

</script>
<script>
$(document).ready(function(){
	var count=0;
	var t = null;
	var userType=$('#userType').val();
    $('.count').prop('disabled', true);
		$(document).on('click','.plus',function(){
		$('.count').val(parseInt($('.count').val()) + 1 );
		count=count+1;
		editDailyScoreBoard();
		
		processDeal();
	});
	$(document).on('click','.minus',function(){
		$('.count').val(parseInt($('.count').val()) - 1 );
			if ($('.count').val() == 0) {
				$('.count').val(1);
			}
			editDailyScoreBoard();
    	});
	
			$('.xfercount').prop('disabled', true);
			$(document).on('click','.xferplus',function(){
			$('.xfercount').val(parseInt($('.xfercount').val()) + 1 );
			count=count+1;
			editDailyScoreBoard();
			processDeal();
		});
		$(document).on('click','.xferminus',function(){
			$('.xfercount').val(parseInt($('.xfercount').val()) - 1 );
				if ($('.xfercount').val() == 0) {
					$('.xfercount').val(1);
				}
				editDailyScoreBoard();
			});
		
		/**
		 Created By Bhagya on 18th may 2021
		 function for to edit the dailyscoreboard
		*/
		function editDailyScoreBoard(){
		var dealsCount=$('#dealsCount').val();
		var xfersCount=$('#xfersCount').val();
		var userId=$('#userId').val();
		
		$.ajax({
			    url: "/hoveytracker/admin/editdailyscoreboard.do",
			    type: 'GET',
			    data:
			    	{
				    	userId:userId,
				    	dealsCount:dealsCount,
						xfersCount:xfersCount
			    	},
			   	success: function(data) {
			   		//alert("DailyScoreBoard Updated Successfully");
			   		//window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Updating DailyScoreBoard");
			    }
			});
		 }
		
		
		function processDeal(){
			if(userType!="Admin"){
		    	if (t !== null) { window.clearTimeout(t); }
	
		        t = window.setTimeout(function() {
		        	 alert("Number Of Deals Closed: "+count);
		       	 	window.location.href = '${pageContext.request.contextPath}/user/processdealclose.do?count='+count;
		        }, 3000);
			}
	    }
	});
</script>

<script>

</script>
</body>
</html>