<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Register User -::</title>

<%@ include file="/jsp/common/common.jsp"%>
<script src="${pageContext.request.contextPath}/js/admin/admin.js"></script>
</head>
<body>
<div class="he_login">
	
	<!-- Page Header -->
		<%@ include file="/jsp/common/adminHeader.jsp"%>
	
	<!--  END of Page Header -->

    
    <div class="mn_edit">
        <div class="container">
            <div class="row">
            <form:form method="post" id="register_user" name="registerUser"  action="${pageContext.request.contextPath}/admin/userregister.do" commandName="userDto"  autocomplete="off" onsubmit="return validateRegisterUser();"
			enctype="multipart/form-data">
                <div class="col-md-12">
                        <h2>Create User</h2>
                    <div class="mn_edit_profile">
                        <div class="card-body row">
                            <div class="input-group no-border input-lg col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-user"></i>
                                </span>
                              </div>
                              <form:input id="username" type="text" class="form-control" path="username" placeholder="User Name..."></form:input>
                              <span id="username_error" class="error_msg"></span>
                            </div>
                            <div class="input-group no-border input-lg  col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-lock"></i>
                                </span>
                              </div>
                              <form:input id="password" type="password" path="password" placeholder="Password..." class="form-control"></form:input>
                               <span id="password_error" class="error_msg"></span>
                            </div>
                            <div class="input-group no-border input-lg col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-lock"></i>
                                </span>
                              </div>
                              <form:input id="cpassword" type="password" path="" class="form-control" placeholder="Confirm Password..."></form:input>
                               <span id="cpassword_error" class="error_msg"></span>
                            </div>
                            <div class="input-group no-border input-lg  col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-user"></i>
                                </span>
                              </div>
                              <form:input id="firstName" type="text" placeholder="First Name..." path="firstName" class="form-control"></form:input>
                               <span id="firstName_error" class="error_msg"></span>
                            </div>
                            <div class="input-group no-border input-lg col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-user"></i>
                                </span>
                              </div>
                              <form:input id="lastName" type="text" class="form-control" placeholder="Last Name..." path="lastName"></form:input>
                               <span id="lastName_error" class="error_msg"></span>
                            </div>
                            <div class="input-group no-border input-lg  col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-map-marker"></i>
                                </span>
                              </div>
                              <form:input id="city"  type="text" placeholder="city..." class="form-control" path="city"></form:input>
                            </div>
                            <div class="input-group no-border input-lg  col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-map-marker"></i>
                                </span>
                              </div>
                              <form:select id="state" class="form-control"  path="state">
                              	<option value="Select State">Select State</option>
                              	<c:forEach var="state" items="${states}">
                              		<option value="${state}">${state}</option>
                              	</c:forEach>
                                    
                              </form:select>
                             
                            </div>
                            <div class="input-group no-border input-lg  col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-code-fork"></i>
                                </span>
                              </div>
                              <form:input id="zipCode" type="text" placeholder="Zip Code..." class="form-control" path="zipcode"></form:input>
                            </div>
                            <div class="input-group no-border input-lg  col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-envelope-o"></i>
                                </span>
                              </div>
                              <form:input id="emailId" type="text" placeholder="Email..." class="form-control" path="emailId"></form:input>
                               <span id="emailId_error" class="error_msg"></span>
                            </div>
                            
                            <div class="input-group no-border input-lg  col-md-6">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="fa fa-user"></i>
                                </span>
                              </div>
                              <form:select id="userType" class="form-control" path="userType">
                              	<option value="Select UserType">Select UserType</option>
                                <option value="Admin">Admin</option>
                                 <option value="Fronter">Fronter</option>
                                 <option value="Closer">Closer</option>
                              </form:select>
                               <span id="userType_error" class="error_msg"></span>
                            </div>
                          </div>
                          <div class="card-footer  text-center">
                            <div class="row mn_re">
                            <div class="col-md-12">
                                <button class="btn btn-primary float-right mb-3 col-md-2 btn-round btn-lg">Register</button>
                            </div></div>
                        </div>
                        
                    </div>
                </div>
                
                </form:form>
            </div>
        </div>
    </div> 
</div>

</body>
</html>