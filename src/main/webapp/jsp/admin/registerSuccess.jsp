<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Register Success -::</title>
<%@ include file="/jsp/common/common.jsp"%>
</head>
<body>
<div class="he_login">

   <!-- Page Header -->
		<%@ include file="/jsp/common/adminHeader.jsp"%>
	
	<!--  END of Page Header -->
    <div class="mn_edit">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mn_success">
                        <i class="fa fa-check-circle"></i>
                        <h2>Registered Successfully, </h2>
                        <p>A Mail has been sent to your email: <a href="">${email}</a>,<br/> Please click on the activation link to activate your account</p>
                        <!-- <p>A Mail has been sent to your email: <a href="">bhagya@knstek.com</a>,<br/> Please click on the activation link to activate your account</p> -->
                        <p>Successfully Registered to Hovey Tracker, Please check your mail to activate the account</p>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

</body>
</html>