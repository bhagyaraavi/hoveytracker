<!DOCTYPE html>
<html lang="en-US">
<head>

<title>::- Manage Users -::</title>
<%@ include file="/jsp/common/common.jsp"%>
<script src="${pageContext.request.contextPath}/js/admin/admin.js"></script>
</head>
<script>
$(document).ready(function(){
	var sortBy=$('#sortBy').val();
	var sortDirection=$('#sortDirection').val();
	if(sortDirection=='true'){
		jQuery("#user_name,#first_name,#last_name,#email_id,#user_type").removeClass('sorting').addClass('sorting_asc');
		
	}
	else{
		jQuery("#user_name,#first_name,#last_name,#email_id,#user_type").removeClass('sorting').addClass('sorting_desc');
	}
	
});
</script>
<body>
<div class="he_login">
	<!-- Page Header -->
		<%@ include file="/jsp/common/adminHeader.jsp"%>
	
	<!--  END of Page Header -->
	
    
    <div class="mn_edit">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	
               <form:form commandName="displayListBean" method="POST" class="form-horizontal" action="${pageContext.request.contextPath}/admin/viewusers.do" role="form">
								<form:hidden path="sortBy" value="${displayListBean.sortBy}" id="sortBy" />
								<form:hidden path="searchBy" />
								<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection" />
								<form:hidden path="pagerDto.range" />
								<form:hidden path="pagerDto.pageNo" />
               
                	
                  	  <h2>Manage Users</h2>
                  <div class="mn_edit_profile">
                 	 <c:set var="range" value="${displayListBean.pagerDto.range}" />
								<!-- SELECT  PAGE RANGE -->
								
								<div class="row">
									<div class="col-xl-6 col-lg-5 col-md-4 col-sm-12">
										<div class="dataTables_length" id="dataTableId_length" 	style="margin-left: 10px;">
											<label style="font-weight: 900;">Show 
											<select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
													<option value="10" ${range=='10' ? 'selected' : '' }>10</option>
													<option value="25" ${range=='25' ? 'selected' : '' }>25</option>
													<option value="50" ${range=='50' ? 'selected' : '' }>50</option>
													<option value="100" ${range=='100' ? 'selected' : '' }>100</option>
											</select> entries
											</label>
										</div>
									</div>
									<!-- END OF SELECT PAGE RANGE -->
									<!-- SEARCH DIVISION -->
									<div class="col-xl-6 col-lg-7 col-md-8 col-sm-12 mn_padding">
										<div class="row">
											<div class="col-lg-2 col-md-2 col-sm-12 no_pad_left">
											<label>SearchBy:</label>
											</div>
											<div class="col-lg-5 col-md-4 col-sm-6 no_pad_left">
												<input type="text" id="searchText" name="searchText" placeholder="Search for users "	value="${displayListBean.searchBy}">
											</div>
											
									<div class="col-lg-5 col-md-5 col-sm-6 no_pad_left">
									
											<div class="col-6 no_pad_left pull-right">
												<button class="btn mn_none" type="button"
													onclick="window.location='${pageContext.request.contextPath}/admin/viewusers.do'">Reset</button>
											</div>
											<div class="col-6 no_pad_left pull-right">
												<button class="btn mn_none1" type="button" value="Search"
													onclick="searchResults();">Search</button>
											</div>
									</div>
										</div>
									</div>
								</div>
								<!-- END OF Search divison -->
								 <c:choose>
                				<c:when test="${empty message }">
								<div class="mn_fronter">
                      <table id="dataTableId" class="table dataTable table-striped"	role="grid" aria-describedby="dataTableId_info"	 cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th class="sorting" id="user_name" onclick="sortResults('username')">Username</th>
                          <th  class="sorting" id="first_name" onclick="sortResults('firstName')">FirstName</th>
                          <th class="sorting" id="last_name" onclick="sortResults('LastName')">LastName</th>
                          <th class="sorting" id="email_id" onclick="sortResults('emailId')">Email</th>
                          <th class="sorting" id="user_type" onclick="sortResults('userType')">UserType</th>
                          <th>Status</th>
                          <th>EditUser</th>
                          <th>ResetPassword</th>
                        </tr>
                      </thead>

                      <tbody>
                      <c:forEach var="userDto" items="${userDtos}">
                        <tr>
                          <td>${userDto.username}</td>
                          <td>${userDto.firstName}</td>
                          <td>${userDto.lastName}</td>
                          <td>${userDto.emailId}</td>
                          <td>${userDto.userType}</td>
                          
                          <td>
                			<input type="hidden" id="accountStatus${userDto.userId}" value="${userDto.isActive}">
                			<c:choose>
                				<c:when test="${userDto.isActive==true}">
                					<c:set var="userStatus" value="active"></c:set>
                				</c:when>
                				<c:otherwise>
                					<c:set var="userStatus" value="inactive"></c:set>
                				</c:otherwise>
                			</c:choose>
						                      					
						     <input type="hidden" id="userStatus${userDto.userId}" value="${userStatus}">
							 <select id="status_${userDto.userId}"  onchange="changeStatus(this,${userDto.userId});">
							 	<option value="active" ${userStatus=='active' ? 'selected' : '' }>Active</option>
								<option value="inactive" ${userStatus=='inactive' ? 'selected' : '' }>Inactive</option>				                   
							</select>
						</td>
                          <td><a href="${pageContext.request.contextPath}/admin/editprofile.do?userId=${userDto.userId}"><i class="fa fa-user-plus"></i></a></td>
                          <td><a href="${pageContext.request.contextPath}/admin/adminresetpassword.do?userId=${userDto.userId}" title="Reset Password"><i class="fa fa-lock"></i></a></td>
                        </tr>
                        </c:forEach>
                        
                      </tbody>
                    </table>
                	<div class="clearfix"></div>
												
												<c:set var="first" value="0" />
												<c:set var="end"
													value="${displayListBean.pagerDto.pagesNeeded }" />
												<c:set var="page" value="${displayListBean.pagerDto.pageNo}" />
												<c:set var="total"
													value="${displayListBean.pagerDto.totalItems}" />
												<c:set var="firstResult"
													value="${displayListBean.pagerDto.firstResult}" />
											<c:set var="lastResult"
													value="${displayListBean.pagerDto.lastResult}" />
												<div class="dataTables_info" id="dataTableId_info"
													role="status" aria-live="polite">Showing
													${firstResult} to ${lastResult} of ${total} entries</div>
												<div class="dataTables_paginate paging_simple_numbers"
													id="dataTableId_paginate">
	
													<%@ include file="/jsp/common/pager.jsp"%>
	
												</div>
                    </div>
                    </c:when>
                	<c:otherwise>
                		<h2>${message}</h2>
                	</c:otherwise>
                  </c:choose>
                  </div>
                  </form:form>
                  
                    
                </div>
                
            </div>
        </div>
    </div> 
</div>

</body>
</html>