<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html>
<head manifest="tetris.manifest">     
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black" /> 
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="screen, mobile" title="main" charset="utf-8">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <title>Error</title>
   
</head>		
<body>
	<div class="login-page">
	<div id="login_upper">
		<a href="/hoveytracker/login.do"><h1><img src="/hoveytracker/images/logo.png"/></h1> </a>
		<h2 align="center">Something went Wrong</h2>
	</div>
	<br />
	<br />
	<h4 align="center">Please &nbsp; <a href="${pageContext.request.contextPath}/login.do"> Click </a> &nbsp; here for Login Page</h4>
	</div>
</body>
</html>

