/**
		Created By Bhagya on 21th may 2021
		function for to edit the fronterboard by admin
	 */
	function editFronterBoard(fronterId,dayName,count){
		var adminUserId=$('#adminUserId').val();
		
		$.ajax({
			    url: "/hoveytracker/admin/editfronterboard.do",
			    type: 'GET',
			    data:
			    	{
						fronterId:fronterId,
						adminUserId:adminUserId,
				    	count:count,
						dayName:dayName
			    	},
			   	success: function(data) {
			   		//alert("Fronter Board Updated Successfully");
			   		window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Updating Fronter Board");
			    }
			});
	}
	/**
		Created By Bhagya on may 22nd, 2021
		FUnction for to update fronter board data
	 */
	
	function updateFrontersBoardData(){	 
    	var lastWeek=$('#lastWeek').val();
		var weekNum=$('#weekNum').val();
		var weekGoal=$('#weekGoal').val();
		var adminUserId=$('#adminUserId').val();
		
		var numberFilter = /^[0-9]+$/;
		var a,b,c=true;
		if(lastWeek.length>0 && !numberFilter.test(lastWeek)){
			a=false;
			$('#lastWeek_error').html('*Please enter numeric characters only');
			
		}
		else{
			a=true;
			$('#lastWeek_error').html('');
		}
		if(weekNum.length>0 && !numberFilter.test(weekNum)){
			b=false;
			$('#weekNum_error').html('*Please enter numeric characters only');
			
		}
		else{
			b=true;
			$('#weekNum_error').html('');
		}
		if(weekGoal.length>0 && !numberFilter.test(weekGoal)){
			c=false;
			$('#weekGoal_error').html('*Please enter numeric characters only');
			
		}
		else{
			c=true;
			$('#weekGoal_error').html('');
		}
		if(a==true&&b==true&&c==true){
		$.ajax({
			    url: "/hoveytracker/admin/updatefrontersboarddata.do",
			    type: 'GET',
			    data:
			    	{
						adminUserId:adminUserId,
						lastWeekCount:lastWeek,
						weekNum:weekNum,
						weekGoal:weekGoal
				    	
			    	},
			   	success: function(data) {
			   		//alert("Fronter Board Updated Successfully");
			   		window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Updating Fronter Board");
			    }
			});
      }
      
		
	}
	
	
	/**
		Created By Bhagya on 26th may 2021
		function for to edit the closerboard by admin
	 */
	function editCloserBoard(closerId,dayName,count){
		var adminUserId=$('#adminUserId').val();
		
		$.ajax({
			    url: "/hoveytracker/admin/editcloserboard.do",
			    type: 'GET',
			    data:
			    	{
						closerId:closerId,
						adminUserId:adminUserId,
				    	count:count,
						dayName:dayName
			    	},
			   	success: function(data) {
			   		//alert("Closer Board Updated Successfully");
			   		window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Updating Closer Board");
			    }
			});
	}
	/**
		Created By Bhagya on may 26th, 2021
		FUnction for to update closer board data
	 */
	
	function updateClosersBoardData(){
		var lastWeek=$('#lastWeek').val();
		var weekNum=$('#weekNum').val();
		var weekGoal=$('#weekGoal').val();
		var adminUserId=$('#adminUserId').val();
		
		var numberFilter = /^[0-9]+$/;
		var a,b,c=true;
		if(lastWeek.length>0 && !numberFilter.test(lastWeek)){
			a=false;
			$('#lastWeek_error').html('*Please enter numeric characters only');
			
		}
		else{
			a=true;
			$('#lastWeek_error').html('');
		}
		if(weekNum.length>0 && !numberFilter.test(weekNum)){
			b=false;
			$('#weekNum_error').html('*Please enter numeric characters only');
			
		}
		else{
			b=true;
			$('#weekNum_error').html('');
		}
		if(weekGoal.length>0 && !numberFilter.test(weekGoal)){
			c=false;
			$('#weekGoal_error').html('*Please enter numeric characters only');
			
		}
		else{
			c=true;
			$('#weekGoal_error').html('');
		}
		if(a==true&&b==true&&c==true){
			$.ajax({
				    url: "/hoveytracker/admin/updateclosersboarddata.do",
				    type: 'GET',
				    data:
				    	{
							adminUserId:adminUserId,
							lastWeek:lastWeek,
							weekNum:weekNum,
							weekGoal:weekGoal
					    	
				    	},
				   	success: function(data) {
				   		//alert("Closer Board Updated Successfully");
				   		window.location.reload();
				    },
				    error:function(data,status,er) {
				    	alert("Error While Updating Closer Board");
				    }
				});
			}
	}
	
	

	/*Created By Harshitha on May 27th, 2021
	Function for to delete the fronter*/
	
	function deleteFronter(sel,id){
		var status=sel.value;
		if(confirm("Are you Sure to Delete Fronter")==true){
			$.ajax({
			    url: "/hoveytracker/admin/deletefronter.do",
			    type: 'GET',
			    data:
			    	{
			    		fronterId:id
			    	},
			   	success: function(data) {
			        alert("Fronter Deleted Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Deleting Fronter");
			    }
			});
		}
	}
	
	
	/*Created By Harshitha on May 27th, 2021
	Function for to delete the closer*/
	
	function deleteCloser(sel,id){
		var status=sel.value;
		if(confirm("Are you Sure to Delete Closer")==true){
			$.ajax({
			    url: "/hoveytracker/admin/deletecloser.do",
			    type: 'GET',
			    data:
			    	{
			    		closerId:id
			    	},
			   	success: function(data) {
			        alert("Closer Deleted Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Deleting Closer");
			    }
			});
		}
	}
	
	/**
	Created By Bhagya on June 11th, 2021
	function for to save the message
	 */
	function saveMessage(){
		var userId=$('#userId').val();
		var message=$('#message').val();
		$.ajax({
			    url: "/hoveytracker/user/savemessage.do",
			    type: 'GET',
			    data:
			    	{
			    		userId:userId,
						message:message
			    	},
			   	success: function(data) {
				$('#message').val("");
				//window.location.href = '/hoveytracker/user/viewmessages.do';
				
			    },
			    error:function(data,status,er) {
			        alert("Error While Sending Message");
			    }
			});
		
	}
	
	
	