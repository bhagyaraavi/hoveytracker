		var isUserNameExist=false;
		var isUserEmailExist=false;
		
/**
		 * Created By Bhagya On May 12th, 2021
		 * Function for to validate the USer registration form
		 */
		function validateRegisterUser(){
			var result=false;
			var a,b,c,d,e,f,g=true;
			var nameFilter=/^(?=.*[a-z])/; //matches only letters
			var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			var username=$('#username').val();
			var password=$('#password').val();
			var cpassword=$('#cpassword').val();
			var firstName=$('#firstName').val();
			var lastName=$('#lastName').val();
			var emailId=$('#emailId').val();
			var userType=$('#userType').val();
			
			if(username=="" || username==null){
				a=false;
				$('#username_error').html('*Required');
			}
			else if(!nameFilter.test(username)){
				a=false;
				$('#username_error').html('*Should have atleast one Alphabet');
			}
			else{
				a=true;
				$('#username_error').html('');
				checkUserNameExistance(username);
			}
									
			if(password.trim().length<1){
				$('#password_error').html('*Required');
				b=false;
			}
			else if(password.trim().length<8 || password.trim().lenght>20){
				$('#password_error').html('* Should have minimum 8 characters and maximum 20 characters');
				b=false;
			}
			else{
				$('#password_error').html('');
				b=true;
			}
			
			if(cpassword.trim().length<1){
				$('#cpassword_error').html('*Required');
				c=false;
			}
			else if(password!=cpassword){
				$('#cpassword_error').html('* Password  and Confirm Password both should be same');
				c=false;
			}
			else{
				$('#cpassword_error').html('');
				c=true;
			}
			if(firstName=="" || firstName==null){
				d=false;
				$('#firstName_error').html('*Required');
			}
			else{
				d=true;
				$('#firstName_error').html('');
			}
			if(lastName=="" || lastName==null){
				e=false;
				$('#lastName_error').html('*Required');
			}
			else{
				e=true;
				$('#lastName_error').html('');
			}
			
			if(emailId=="" || emailId==null){
				f=false;
				$('#emailId_error').html('*Required');
			}
			else if(emailId!="" && !emailFilter.test(emailId)){
				f=false;
				$('#emailId_error').html('*Email Address is not Valid');
			}
			
			else{
				f=true;
				$('#emailId_error').html('');
				checkUserEmailExistance(emailId);
				
			}
			if(userType=="" || userType==null){
				g=false;
				$('#userType_error').html('*Required');
			}
			else if(userType=='Select UserType'){
				g=false;
				$('#userType_error').html('*Required');
			}
			else{
				g=true;
				$('#userType_error').html('');
			}
			
			
			if(a==true && b==true && c==true && d==true && e==true &&f==true && g==true){
				result=true;
			}
			else{
				result=false;
			}		
		
			 if(isUserNameExist==true || isUserEmailExist==true ){
				 result=false;
			 }
								 
			return result;
			
		}
		
		
		/**
		 * Created By bHagya On May 12th, 2021
		 * @param user
		 * 
		 * Function for to check the user Existance by username
		 */
		function checkUserNameExistance(user){
			var a=false;
			$.ajax({
				url:"/hoveytracker/admin/checkuserexistance.do",
				type:'GET',
				async:false,
				data:{
					user:user
				},
				success:function(data){
					if(data==="success"){
						isUserNameExist=false;	
						$('#username_error').html('');
					}		
					else{
						isUserNameExist=true;		
						$('#username_error').html('*User Name Already Exist');
					}	
										
				},
			complete:{
			}
			});		
			$.ajaxSetup({async: true});
		}
		
		/**
		 * Created By Bhagya on May 12th, 2021
		 * @param user
		 * 
		 * Function for to check the user email existance
		 */


		function checkUserEmailExistance(user){
			var result=false;
			var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			$.ajax({
				url:"/hoveytracker/admin/checkuserexistance.do",
				type:'GET',
				async:false,
				data:{
					user:user
				},
				success:function(data){
					if(data==="success"){
						if(user!="" && !emailFilter.test(user)){
							$('#emailId_error').html('*Email Address is not Valid');
						}else{
							isUserEmailExist=false;		
							$('#emailId_error').html('');
						}
					}		
					else{
						isUserEmailExist=true;		
						$('#emailId_error').html('*Email Already Exist');
					}							
				},
			complete:{
				
			}
			});		
			$.ajaxSetup({async: true});
			
		}
		
		
		
		/**
		 * Created By Bhagya On May 12th, 2021
		 * Function for to validate the edit registered user/profile
		 */
		function validateEditRegisterUser(){
			var result=false;
			var a,b,c,d,e,f,g=true;
			var nameFilter=/^(?=.*[a-z])/; //matches only letters
			var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			var username=$('#username').val();
			var firstName=$('#firstName').val();
			var lastName=$('#lastName').val();
			var emailId=$('#emailId').val();
			var userType=$('#userType').val();
			var existedUsername=$('#existedUsername').val();
			var existedEmailId=$('#existedEmailId').val();
		
			if(username=="" || username==null){
				a=false;
				$('#username_error').html('*Required');
			}
			else if(!nameFilter.test(username)){
				a=false;
				$('#username_error').html('*Should have atleast one Alphabet');
			}
			else{
				a=true;
				$('#username_error').html('');
				if(username!=existedUsername){
					checkUserNameExistance(username);
				}
			}
									
			
			if(firstName=="" || firstName==null){
				b=false;
				$('#firstName_error').html('*Required');
			}
			else{
				b=true;
				$('#firstName_error').html('');
			}
			if(lastName=="" || lastName==null){
				c=false;
				$('#lastName_error').html('*Required');
			}
			else{
				c=true;
				$('#lastName_error').html('');
			}
			
			if(emailId=="" || emailId==null){
				d=false;
				$('#emailId_error').html('*Required');
			}
			else if(emailId!="" && !emailFilter.test(emailId)){
				d=false;
				$('#emailId_error').html('* Email Address is not Valid');
			}
			
			else{
				d=true;
				$('#emailId').html('');
				if(emailId!=existedEmailId){
					checkUserEmailExistance(emailId);
				}
				
			}
			if(userType=="" || userType==null){
				e=false;
				$('#userType_error').html('*Required');
			}
			else if(userType=='Select UserType'){
				e=false;
				$('#userType_error').html('*Required');
			}
			else{
				e=true;
				$('#userType_error').html('');
			}
			
			
			if(a==true && b==true && c==true && d==true && e==true){
				result=true;
			}
			else{
				result=false;
			}		
		
			 if(isUserNameExist==true || isUserEmailExist==true ){
				 result=false;
			 }
								 
			return result;
		}
		/**
		Created By Bhagya On MAy 13th, 2021
		Function for to validate the reset password functionality for admin
		 */
		function validateResetPassword(){
			var password=$('#password').val();
			var cpassword=$('#cpassword').val();
			
			var a,b=true;
			
			if(password.trim().length<1){
				$('#password_error').html('*Required');
				a=false;
			}
			else if(password.trim().length<8 || password.trim().lenght>20){
				$('#password_error').html('* Password Should have minimum 8 characters and maximum 20 characters');
				a=false;
			}
			else{
				$('#password_error').html('');
				a=true;
			}
			
			if(cpassword.trim().length<1){
				$('#cpassword_error').html('*Required');
				b=false;
			}
			else if(password!=cpassword){
				$('#cpassword_error').html('* Password  and Confirm Password both should be same');
				b=false;
			}
			else{
				$('#cpassword_error').html('');
				b=true;
			}
			
			if(a==true && b==true){
				result=true;
			}
			else{
				result=false;
			}
			return result;	
		}
		
		
		/**
		Created By Bhagya on May 13th, 2021
		FUnction for to change the status of user by admin
		 */
		function changeStatus(sel,id){
		var status=sel.value;
		if(status=="active"){
			$.ajax({
			    url: "/hoveytracker/admin/changeuserstatus.do",
			    type: 'GET',
			    data:
			    	{
			    		userId:id,
			    		isActive:true
			    	},
			   	success: function(data) {
			        alert("User Activated Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Activating User");
			    }
			});
		}
		else if(status=="inactive"){
			$.ajax({
			    url: "/hoveytracker/admin/changeuserstatus.do",
			    type: 'GET',
			    data:
			    	{
				    	userId:id,
				    	isActive:false
			    	},
			   	success: function(data) {
			   		alert("User Inactivated Successfully");
			   		window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Inactivating User");
			    }
			});
		}
		
	}
	
	/**
		Created By Bhagya on 18th may 2021
		function for to edit the dailyscoreboard
	 */
	function editDailyScoreBoard(){
		var dealsCount=$('#dealsCount').val();
		var xfersCount=$('#xfersCount').val();
		var userId=$('#userId').val();
		
		$.ajax({
			    url: "/hoveytracker/admin/editdailyscoreboard.do",
			    type: 'GET',
			    data:
			    	{
				    	userId:userId,
				    	dealsCount:dealsCount,
						xfersCount:xfersCount
			    	},
			   	success: function(data) {
			   		//alert("DailyScoreBoard Updated Successfully");
			   		window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Updating DailyScoreBoard");
			    }
			});
	}