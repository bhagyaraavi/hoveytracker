package com.hoveytracker.common.exceptions;

/***
 * Created By Harshitha On June 18th ,2021
 *	This Exception Handles the condition when the ScoreMessage notification data not found
 */
public class ScoreMessageNotificationNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "ScoreMessage Notification data Not Found";
	}
}