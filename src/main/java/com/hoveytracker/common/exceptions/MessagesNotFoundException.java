package com.hoveytracker.common.exceptions;

/***
 * Created By Harshitha On June 7th ,2020
 *	This Exception Handles the condition when the messages not found
 */
public class MessagesNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "messages Not Found";
	}
}