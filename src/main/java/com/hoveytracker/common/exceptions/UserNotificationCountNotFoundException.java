package com.hoveytracker.common.exceptions;

/***
 * Created By harshitha On June 16th ,2021
 *	This Exception Handles the condition User Notification Count is not found
 */
public class UserNotificationCountNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Notification Count is not found";
	}
}