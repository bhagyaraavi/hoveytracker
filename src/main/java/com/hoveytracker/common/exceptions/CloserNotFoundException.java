package com.hoveytracker.common.exceptions;

/***
 * Created By harshitha On May 19th ,2021
 *	This Exception Handles the condition when the closer is not found
 */
public class CloserNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Closer Not Found";
	}
}