package com.hoveytracker.common.exceptions;

/***
 * Created By Bhagya On May 22nd ,2021
 *	This Exception Handles the condition when the fronter grand total is not found
 */
public class FrontersGrandTotalNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Fronter Grand Total Not Found";
	}
}