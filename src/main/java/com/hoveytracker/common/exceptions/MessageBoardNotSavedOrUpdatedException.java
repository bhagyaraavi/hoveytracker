package com.hoveytracker.common.exceptions;


/***
 * Created By harshitha On June 7th ,2021
 *	This Exception Handles the condition when the Message Board data is not saved or updated
 */
public class MessageBoardNotSavedOrUpdatedException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Message Board details are Not Saved or Updated";
	}
}