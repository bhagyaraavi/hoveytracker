package com.hoveytracker.common.exceptions;

/***
 * Created By harshitha On June 17th ,2021
 *	This Exception Handles the condition when Notification Count is not updated
 */
public class NotificationCountNotUpdatedException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Notification Count is not Updated";
	}
}