package com.hoveytracker.common.exceptions;

/***
 * Created By Bhagya On May 22nd ,2021
 *	This Exception Handles the condition when the fronters board data not found
 */
public class FrontersBoardDataNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Fronters board data Not Found";
	}
}