package com.hoveytracker.common.exceptions;

/***
 * Created By Harshitha On May 24th ,2021
 *	This Exception Handles the condition when the closer grand total is not found
 */
public class ClosersGrandTotalNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Closer Grand Total Not Found";
	}
}