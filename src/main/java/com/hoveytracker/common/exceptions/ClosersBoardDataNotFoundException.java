package com.hoveytracker.common.exceptions;


/***
 * Created By Harshitha On May 25th ,2021
 *	This Exception Handles the condition when the closers board data not found
 */
public class ClosersBoardDataNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Closer's board data Not Found";
	}
}