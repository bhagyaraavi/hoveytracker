package com.hoveytracker.common.exceptions;

/***
 * Created By Bhagya On May 07th ,2021
 *	This Exception Handles the condition when the admin/fronter/closer user not found
 */
public class UserNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Not Found";
	}
}