package com.hoveytracker.common.exceptions;


/***
 * Created By Harshitha On June 3rd ,2021
 *	This Exception Handles the condition when the notification configuration data not found
 */
public class NotificationConfigurationNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Notification configuration data Not Found";
	}
}