package com.hoveytracker.common.exceptions;
/**
 * 
 * Created By Bhagya On may07th, 2021
 * Exception class for UserNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when user not saved or updated
 */
public class UserNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Not Saved Or Updated ";
	}
}