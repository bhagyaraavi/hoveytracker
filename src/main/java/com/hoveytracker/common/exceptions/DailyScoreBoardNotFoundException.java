package com.hoveytracker.common.exceptions;

/***
 * Created By Bhagya On May 17th ,2021
 *	This Exception Handles the condition when the  dailyscoreboard not found
 */
public class DailyScoreBoardNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "DailyScoreBoard Not Found";
	}
}