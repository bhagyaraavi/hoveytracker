package com.hoveytracker.common.exceptions;

/***
 * Created By harshitha On May 17th ,2021
 *	This Exception Handles the condition when the fronter's grand total data is not saved or updated
 */
public class FronterGrandTotalNotSavedOrUpdatedException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Fronter's Grand Total data is Not Saved or Updated";
	}
}