package com.hoveytracker.common.exceptions;
/**
 * Created By Bhagya On may 10th,2021
 * This Exception handles if the mail was not sent to user
 */
public class MailNotSentException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Mail Not Sent Exception";
	}
}