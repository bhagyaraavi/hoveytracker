package com.hoveytracker.common.exceptions;

/***
 * Created By Bhagya On May 12th ,2020
 *	This Exception Handles the condition when the state not found in db
 */
public class StateNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "State Not Found";
	}
}