package com.hoveytracker.common.exceptions;

/***
 * Created By harshitha On May 17th ,2021
 *	This Exception Handles the condition when the closer's board data is not saved or updated
 */
public class CloserBoardDataNotSavedOrUpdatedException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Closer's Board data is Not Saved or Updated";
	}
}