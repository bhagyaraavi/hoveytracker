package com.hoveytracker.common.exceptions;


/***
 * Created By harshitha On June 3rd ,2021
 *	This Exception Handles the condition when Notification Configuration is not saved or updated
 */
public class NotificationConfigurationNotSavedOrUpdatedException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Notification Configuration is not saved or updated";
	}
}