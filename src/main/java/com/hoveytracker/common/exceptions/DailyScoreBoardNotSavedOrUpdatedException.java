package com.hoveytracker.common.exceptions;
/**
 * 
 * Created By Bhagya On may20th, 2021
 * Exception class for DailyScoreBoardNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when dailyScoreboard not saved or updated
 */
public class DailyScoreBoardNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "DailyScoreBoard Not Saved Or Updated ";
	}
}