package com.hoveytracker.common.security.service;
/**
 * Created By Bhagya On May 07th,2021
 * Implementation class for core interface of UserDeatailsService 
 * 		which loads user specific data
 */
import java.util.ArrayList;




import java.util.Collection;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.hoveytracker.frontend.user.dto.UserDto;
import com.hoveytracker.frontend.user.service.UserService;

/**
 * Created on may 07th,2021
 * @author Bhagya
 * Service for UserDeatils authentication handling
 */

@Service(value="userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{
	
	@Resource(name="userService")
	private UserService userService;
	
	private static Logger log=Logger.getLogger(UserDetailsServiceImpl.class);
	

	@Override
	public UserDetails loadUserByUsername(String emailId)
			throws UsernameNotFoundException, DataAccessException {
		log.info("inside loadUserByUsername ");
		UserDto userDto=null;
		Boolean accountNonLocked=true;
		try{
			System.out.println(" Inside try user details "+emailId);
			
			userDto=this.userService.getUserByUsernameOrEmailId(emailId);
		
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("inside user service "+e.toString());
		}
		
		if(userDto ==null){
			
			throw new UsernameNotFoundException("User Not Found");
		}
		 
		
		if(null!=userDto.getIsActive() && userDto.getIsActive()==true){
			accountNonLocked =true;
		}
		else{
			
			accountNonLocked =false;
		}
		//String email=userDto.getEmailId();
		String username=userDto.getUsername();
		String password=userDto.getPassword();
		
		System.out.println(" password "+password +" username "+username +" usertype "+userDto.getUserType().toString());
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(userDto.getUserType().toString()));
		Boolean enabled =true;
		Boolean accountNonExpired =true;
		Boolean credentialsNonExpired =true;
		//User user=new User(email, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		User user=new User(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		return (UserDetails)user;
		 
	}

}