/**
 *  Created by    : Bhagya


 * Created Date	  : May 13th, 2020
 * file Name	  : CustomAuthenticationFailureHandler.java
 * Purpose		  : Handling Autyhentication Faiulure mechanism and redirecting to User according to the Login Failure Pages..
 * Type			  : Support Utility/Service
 */


package com.hoveytracker.common.security.service;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.hoveytracker.backend.user.model.User;
import com.hoveytracker.frontend.user.dto.UserDto;
import com.hoveytracker.frontend.user.service.UserService;


/**
 * Created By Bhagya on May 07th,2021
 * Custom Authentication Failure Adapter,
 * Handles Authentication Failure Conditions With Detailed Messages for Each Failure....
 */
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	@Autowired
	private org.springframework.security.crypto.password.PasswordEncoder bCryptEncoder;
	@Resource(name="userService")
	private UserService userService;

	/* pre defined */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		
		super.onAuthenticationFailure(request, response, exception);
		String message;
		Boolean isPasswordMatch=true;
		String password = request.getParameter("password");
		String username= request.getParameter("username");
		
		System.out.println("EXCEPTION "+exception.getMessage()+" "+exception.getClass());
		/**
		 * Added the try block functionality by bhagya on june 14th, 2021
		 * To check the Password empty condition for inactive users
		 */
		try{
			
			UserDto userDto=this.userService.getUserByUsernameOrEmailId(username);
			
			if(!bCryptEncoder.matches(password, userDto.getPassword())) {
				isPasswordMatch=false;
			}
		
		}
		catch(Exception e){
			
		}
	
		if(exception.getClass().isAssignableFrom(BadCredentialsException.class)){
			message="Invalid Login Credentials..Try Again";
		}
		else if(exception.getClass().isAssignableFrom(CredentialsExpiredException.class)){
			message="Your Account is Expired, Please Contact the Admin for further Details";
		}
		else if(exception.getClass().isAssignableFrom(LockedException.class) && isPasswordMatch==false){
			message="Invalid Login Credentials..Try Again";
		}
		
		else if(exception.getClass().isAssignableFrom(LockedException.class)){
			message="Your Account is InActive, Please Contact the Admin for further Details";
		}
		
		else if(exception.getClass().isAssignableFrom(DisabledException.class)){
			message="Your Account is not yet Activated, Please click on the Activation link on your Mail";			
		}
		else{
			message="Something went wrong..please contact us";
		}
		
		
		
		
        request.getSession().setAttribute("error", message );      
		
	}
	
	
}
