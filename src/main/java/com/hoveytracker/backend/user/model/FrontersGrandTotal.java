package com.hoveytracker.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Created By Harshitha on May 17th,2021
 * @author user
 * Model class for fronter Grand Total
 *
 */
@Entity
@Table(name="fronter_grand_total")
public class FrontersGrandTotal implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="fronter_grand_total_id")
	private Integer fronterGrandTotalId;
	
	@Column(name="mon_total")
	private Integer monTotal;
	
	@Column(name="tues_total")
	private Integer tuesTotal;
	
	@Column(name="wed_total")
	private Integer wedTotal;
	
	@Column(name="thurs_total")
	private Integer thursTotal;
	
	@Column(name="fri_total")
	private Integer friTotal;
	
	
	@Column(name="grand_total")
	private Integer grandTotal;
	

	public Integer getFronterGrandTotalId() {
		return fronterGrandTotalId;
	}

	public void setFronterGrandTotalId(Integer fronterGrandTotalId) {
		this.fronterGrandTotalId = fronterGrandTotalId;
	}

	public Integer getMonTotal() {
		return monTotal;
	}

	public void setMonTotal(Integer monTotal) {
		this.monTotal = monTotal;
	}

	public Integer getTuesTotal() {
		return tuesTotal;
	}

	public void setTuesTotal(Integer tuesTotal) {
		this.tuesTotal = tuesTotal;
	}

	public Integer getWedTotal() {
		return wedTotal;
	}

	public void setWedTotal(Integer wedTotal) {
		this.wedTotal = wedTotal;
	}

	public Integer getThursTotal() {
		return thursTotal;
	}

	public void setThursTotal(Integer thursTotal) {
		this.thursTotal = thursTotal;
	}

	public Integer getFriTotal() {
		return friTotal;
	}

	public void setFriTotal(Integer friTotal) {
		this.friTotal = friTotal;
	}

	

	public Integer getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}


}
	