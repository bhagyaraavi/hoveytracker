package com.hoveytracker.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Created By Harshitha on May 17th,2021
 * @author user
 * Model class for closers Grand Total
 *
 */
@Entity
@Table(name="closer_grand_total")
public class ClosersGrandTotal implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="closer_grand_total_id")
	private Integer closerGrandTotalId;
	
	@Column(name="mon_total")
	private Integer monTotal;
	
	@Column(name="tues_total")
	private Integer tuesTotal;
	
	@Column(name="wed_total")
	private Integer wedTotal;
	
	@Column(name="thurs_total")
	private Integer thursTotal;
	
	@Column(name="fri_total")
	private Integer friTotal;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="creation_date")
	private Date creationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updation_date")
	private Date updationDate;
	
	
	@Column(name="grand_total")
	private Integer grandTotal;
	

	
	public Integer getCloserGrandTotalId() {
		return closerGrandTotalId;
	}
	public void setCloserGrandTotalId(Integer closerGrandTotalId) {
		this.closerGrandTotalId = closerGrandTotalId;
	}
	
	public Integer getMonTotal() {
		return monTotal;
	}
	public void setMonTotal(Integer monTotal) {
		this.monTotal = monTotal;
	}
	public Integer getTuesTotal() {
		return tuesTotal;
	}
	public void setTuesTotal(Integer tuesTotal) {
		this.tuesTotal = tuesTotal;
	}
	public Integer getWedTotal() {
		return wedTotal;
	}
	public void setWedTotal(Integer wedTotal) {
		this.wedTotal = wedTotal;
	}
	public Integer getThursTotal() {
		return thursTotal;
	}
	public void setThursTotal(Integer thursTotal) {
		this.thursTotal = thursTotal;
	}
	public Integer getFriTotal() {
		return friTotal;
	}
	public void setFriTotal(Integer friTotal) {
		this.friTotal = friTotal;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	public Integer getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}

	
}
	