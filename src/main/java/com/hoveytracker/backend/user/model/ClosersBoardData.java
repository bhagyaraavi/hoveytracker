package com.hoveytracker.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Created By Harshitha on May 17th,2021
 * @author user
 * Model class for closers Board data
 *
 */
@Entity
@Table(name="closer_board_data")
public class ClosersBoardData implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="closer_Board_data_id")
	private Integer closerBoardDataId;
	
	@Column(name="last_week")
	private Integer lastWeek;
	
	@Column(name="week_num")
	private Integer weekNum;
	
	@Column(name="week_goal")
	private Integer weekGoal;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="creation_date")
	private Date creationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updation_date")
	private Date updationDate;
	

	public Integer getCloserBoardDataId() {
		return closerBoardDataId;
	}
	public void setCloserBoardDataId(Integer closerBoardDataId) {
		this.closerBoardDataId = closerBoardDataId;
	}

	public Integer getLastWeek() {
		return lastWeek;
	}
	public void setLastWeek(Integer lastWeek) {
		this.lastWeek = lastWeek;
	}

	public Integer getWeekNum() {
		return weekNum;
	}
	public void setWeekNum(Integer weekNum) {
		this.weekNum = weekNum;
	}

	public Integer getWeekGoal() {
		return weekGoal;
	}
	public void setWeekGoal(Integer weekGoal) {
		this.weekGoal = weekGoal;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	public void setUser(User adminUser) {
		// TODO Auto-generated method stub
		
	}
	
	
}