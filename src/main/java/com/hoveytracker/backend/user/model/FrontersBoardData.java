package com.hoveytracker.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Created By Harshitha on May 17th,2021
 * @author user
 * Model class for fronters Board data
 *
 */
@Entity
@Table(name="fronters_board_data")
public class FrontersBoardData implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="fronter_Board_data_id")
	private Integer fronterBoardDataId;
	
	@Column(name="last_week")
	private Integer lastWeek;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="creation_date")
	private Date creationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updation_date")
	private Date updationDate;
	
	
	@ManyToOne(targetEntity=User.class)
	@JoinColumn(name="user_id")
	private User user;
	
	/* As per the Client (Robert's) needed added the week# and goal
	 * on 24th june 2021*/
	@Column(name="week_num")
	private Integer weekNum;
	
	@Column(name="week_goal")
	private Integer weekGoal;
	
	public Integer getFronterBoardDataId() {
		return fronterBoardDataId;
	}

	public void setFronterBoardDataId(Integer fronterBoardDataId) {
		this.fronterBoardDataId = fronterBoardDataId;
	}

	public Integer getLastWeek() {
		return lastWeek;
	}

	public void setLastWeek(Integer lastWeek) {
		this.lastWeek = lastWeek;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdationDate() {
		return updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getWeekNum() {
		return weekNum;
	}

	public void setWeekNum(Integer weekNum) {
		this.weekNum = weekNum;
	}

	public Integer getWeekGoal() {
		return weekGoal;
	}

	public void setWeekGoal(Integer weekGoal) {
		this.weekGoal = weekGoal;
	}
	
	
}