package com.hoveytracker.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Created By Harshitha on May 17th,2021
 * @author user
 * Model class for fronter
 *
 */
@Entity
@Table(name="fronter")
public class Fronter implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="fronter_id")
	private Integer fronterId;
	
	@ManyToOne(targetEntity=User.class)
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="mon")
	private Integer mon;
	
	@Column(name="tues")
	private Integer tues;
	
	@Column(name="wed")
	private Integer wed;
	
	@Column(name="thurs")
	private Integer thurs;
	
	@Column(name="fri")
	private Integer fri;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="creation_date")
	private Date creationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updation_date")
	private Date updationDate;
	
	@Column(name="total")
	private Integer total;
	
	@Column(name="IsUpdateByAdmin")
	private Boolean IsUpdateByAdmin;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="admin_updation_date")
	private Date adminUpdationDate;
	
	@ManyToOne(targetEntity=User.class)
	@JoinColumn(name="admin_user_id")
	private User adminUser;
	
	@Transient
	private Integer totalFronters;

	public Integer getFronterId() {
		return fronterId;
	}

	public void setFronterId(Integer fronterId) {
		this.fronterId = fronterId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getMon() {
		return mon;
	}

	public void setMon(Integer mon) {
		this.mon = mon;
	}

	public Integer getTues() {
		return tues;
	}

	public void setTues(Integer tues) {
		this.tues = tues;
	}

	public Integer getWed() {
		return wed;
	}

	public void setWed(Integer wed) {
		this.wed = wed;
	}

	public Integer getThurs() {
		return thurs;
	}

	public void setThurs(Integer thurs) {
		this.thurs = thurs;
	}

	public Integer getFri() {
		return fri;
	}

	public void setFri(Integer fri) {
		this.fri = fri;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdationDate() {
		return updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Boolean getIsUpdateByAdmin() {
		return IsUpdateByAdmin;
	}

	public void setIsUpdateByAdmin(Boolean isUpdateByAdmin) {
		IsUpdateByAdmin = isUpdateByAdmin;
	}

	public Date getAdminUpdationDate() {
		return adminUpdationDate;
	}

	public void setAdminUpdationDate(Date adminUpdationDate) {
		this.adminUpdationDate = adminUpdationDate;
	}

	public User getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(User adminUser) {
		this.adminUser = adminUser;
	}

	public Integer getTotalFronters() {
		return totalFronters;
	}

	public void setTotalFronters(Integer totalFronters) {
		this.totalFronters = totalFronters;
	}
	
	
}