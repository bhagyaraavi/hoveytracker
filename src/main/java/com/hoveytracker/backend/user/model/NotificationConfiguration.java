package com.hoveytracker.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Created By Harshitha on June 3rd,2021
 * @author user
 * Model class for Notification Configuration
 *
 */
@Entity
@Table(name="notification_configuration")
public class NotificationConfiguration implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="config_id")
	private Integer configId;
	
	@ManyToOne(targetEntity=User.class)
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="score_notification")
	private Boolean scoreNotification;
	
	@Column(name="message_notification")
	private Boolean messageNotification;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="creation_date")
	private Date creationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updation_date")
	private Date updationDate;

	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getScoreNotification() {
		return scoreNotification;
	}

	public void setScoreNotification(Boolean scoreNotification) {
		this.scoreNotification = scoreNotification;
	}

	public Boolean getMessageNotification() {
		return messageNotification;
	}

	public void setMessageNotification(Boolean messageNotification) {
		this.messageNotification = messageNotification;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdationDate() {
		return updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	
	
}