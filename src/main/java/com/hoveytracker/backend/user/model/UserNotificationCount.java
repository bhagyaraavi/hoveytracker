package com.hoveytracker.backend.user.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created By Harshitha on June 16th,2021
 * @author user
 * Model class for User Notification Count
 *
 */
@Entity
@Table(name="user_notification_count")
public class UserNotificationCount implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="count_id")
	private Integer countId;
	
	
	@JoinColumn(name="user_id")
	private Integer userId;
	
	@Column(name="notification_count")
	private Integer notificationCount;

	public Integer getCountId() {
		return countId;
	}

	public void setCountId(Integer countId) {
		this.countId = countId;
	}

	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getNotificationCount() {
		return notificationCount;
	}

	public void setNotificationCount(Integer notificationCount) {
		this.notificationCount = notificationCount;
	}

	public void getScoreMessageNotification(boolean b) {
		// TODO Auto-generated method stub
		
	}

	
	
	
}