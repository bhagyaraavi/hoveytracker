package com.hoveytracker.backend.user.dao;

import java.util.ArrayList;


import com.hoveytracker.backend.user.model.Closer;

import com.hoveytracker.backend.user.model.ClosersBoardData;
import com.hoveytracker.backend.user.model.ClosersGrandTotal;
import com.hoveytracker.backend.user.model.Fronter;
import com.hoveytracker.backend.user.model.FrontersBoardData;
import com.hoveytracker.backend.user.model.FrontersGrandTotal;
import com.hoveytracker.backend.user.model.MessageBoard;
import com.hoveytracker.backend.user.model.NotificationConfiguration;
import com.hoveytracker.backend.user.model.User;
import com.hoveytracker.backend.user.model.UserNotificationCount;
import com.hoveytracker.common.exceptions.CloserBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.CloserGrandTotalNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.CloserNotFoundException;
import com.hoveytracker.common.exceptions.CloserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.ClosersBoardDataNotFoundException;
import com.hoveytracker.common.exceptions.ClosersGrandTotalNotFoundException;
import com.hoveytracker.common.exceptions.FronterBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterGrandTotalNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterNotFoundException;
import com.hoveytracker.common.exceptions.FronterNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FrontersBoardDataNotFoundException;
import com.hoveytracker.common.exceptions.FrontersGrandTotalNotFoundException;
import com.hoveytracker.common.exceptions.MessageBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.MessagesNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.NotificationCountNotUpdatedException;
import com.hoveytracker.common.exceptions.ScoreMessageNotificationNotFoundException;
import com.hoveytracker.common.exceptions.UserNotFoundException;
import com.hoveytracker.common.exceptions.UserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotFoundException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotSavedOrUpdatedException;
import com.hoveytracker.frontend.user.dto.ClosersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.FrontersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.MessageBoardDto;

/**
 * 
 * @author Bhagya
 * Created On May 07th,2021
 * Interface for UserDao
 *
 */
public interface UserDao{
	public Integer saveOrUpdateUser(User user) throws UserNotSavedOrUpdatedException;
	public User getUserforAuthentication(String username)throws UserNotFoundException;
	public User getUserByUserId(Integer userId) throws UserNotFoundException;
	
	
	/**
	 * @author Harshitha
	 * Created On May 17th,2021
	 * Interface for FrontersDao and ClosersDao
	 */
	public Integer saveOrUpdateFronter(Fronter fronter) throws FronterNotSavedOrUpdatedException;
	
	public Integer saveOrUpdateFronterGrandTotal(FrontersGrandTotal fronterGrandTotal) throws FronterGrandTotalNotSavedOrUpdatedException;

	public Integer saveOrUpdateFronterBoarddata(FrontersBoardData frontersBoardData) throws FronterBoardDataNotSavedOrUpdatedException;
	
	public Integer saveOrUpdateClosers(Closer closer) throws CloserNotSavedOrUpdatedException;
	
	public Integer saveOrUpdateCloserGrandTotal(ClosersGrandTotal closersGrandTotal) throws CloserGrandTotalNotSavedOrUpdatedException;
	
	public Integer saveOrUpdateClosersBoarddata(ClosersBoardData closersBoardData) throws CloserBoardDataNotSavedOrUpdatedException;
	
	/**
	 * @author Harshitha
	 * Created On May 19th,2021
	 * Interface to get Fronter and Closer based on their Id's
	 */
	public Fronter getFronterByFronterId(Integer fronterId) throws FronterNotFoundException;
	
	public Closer getCloserByCloserId(Integer closerId) throws CloserNotFoundException;
	
	/* Added by bhagya on may 21st, 2021*/
	public Fronter getFronterByUser(User user) throws FronterNotFoundException;
	public Closer getCloserByUser(User user) throws CloserNotFoundException;
	public ArrayList<Fronter> getFrontersData(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws FronterNotFoundException;
	public FrontersBoardData getFrontersBoardData() throws FrontersBoardDataNotFoundException;
	public FrontersGrandTotalDto getFrontersGrandTotal();
	public FrontersGrandTotal getFrontersGrandTotalData() throws FrontersGrandTotalNotFoundException;
	
	/* Added by Harshitha on may 24th, 2021*/
	public ClosersGrandTotal getClosersGrandTotalData() throws ClosersGrandTotalNotFoundException;
	public ClosersGrandTotalDto getClosersGrandTotal();
	
	/* Added by Harshitha on may 25th, 2021*/
	public ClosersBoardData getClosersBoardData() throws ClosersBoardDataNotFoundException;
	public ArrayList<Closer> getClosersData(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws CloserNotFoundException;

	/* Added by Harshitha on June 3rd, 2021*/
	public Integer saveOrUpdateNotificationConfiguration(NotificationConfiguration notificationConfiguration) throws NotificationConfigurationNotSavedOrUpdatedException;
	public NotificationConfiguration getNotificationConfigurationByUser(User user) throws NotificationConfigurationNotFoundException;
	
	/* Added by Harshitha on June 7th, 2021*/
	public Integer saveOrUpdateMessage(MessageBoard messageBoard) throws MessageBoardNotSavedOrUpdatedException;
	public ArrayList<MessageBoard> getMessages() throws MessagesNotFoundException;
	
	/* Added by Harshitha on June 16th, 2021*/
	public Integer saveOrUpdateNotificationCount(UserNotificationCount userNotificationCount) throws UserNotificationCountNotSavedOrUpdatedException;
	public UserNotificationCount getUserNotificationCountByUserId(Integer userId) throws UserNotificationCountNotFoundException;
	public ArrayList<UserNotificationCount> getUserNotificationCountForAllUser() throws UserNotificationCountNotFoundException;

	/* Added by Harshitha on June 17th, 2021*/
    public void updateNotificationCount(Integer userId,Boolean scoreMessage) throws NotificationCountNotUpdatedException;
    
}