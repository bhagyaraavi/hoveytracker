package com.hoveytracker.backend.user.dao;

import java.util.ArrayList;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hoveytracker.backend.user.model.Closer;
import com.hoveytracker.backend.user.model.ClosersBoardData;
import com.hoveytracker.backend.user.model.ClosersGrandTotal;
import com.hoveytracker.backend.user.model.Fronter;
import com.hoveytracker.backend.user.model.FrontersBoardData;
import com.hoveytracker.backend.user.model.FrontersGrandTotal;
import com.hoveytracker.backend.user.model.MessageBoard;
import com.hoveytracker.backend.user.model.NotificationConfiguration;
import com.hoveytracker.backend.user.model.User;
import com.hoveytracker.backend.user.model.UserNotificationCount;
import com.hoveytracker.common.exceptions.CloserBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.CloserGrandTotalNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.CloserNotFoundException;
import com.hoveytracker.common.exceptions.CloserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.ClosersBoardDataNotFoundException;
import com.hoveytracker.common.exceptions.ClosersGrandTotalNotFoundException;
import com.hoveytracker.common.exceptions.FronterBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterGrandTotalNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterNotFoundException;
import com.hoveytracker.common.exceptions.FronterNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FrontersBoardDataNotFoundException;
import com.hoveytracker.common.exceptions.FrontersGrandTotalNotFoundException;
import com.hoveytracker.common.exceptions.MessageBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.MessagesNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.NotificationCountNotUpdatedException;
import com.hoveytracker.common.exceptions.ScoreMessageNotificationNotFoundException;
import com.hoveytracker.common.exceptions.UserNotFoundException;
import com.hoveytracker.common.exceptions.UserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotFoundException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotSavedOrUpdatedException;
import com.hoveytracker.frontend.user.dto.ClosersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.FrontersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.MessageBoardDto;
import com.mysql.cj.Query;
import com.mysql.cj.Session;

/**
 * 
 * @author Bhagya
 * Created on May 07th,2021
 * Implementation class for User Dao
 *
 */
@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao{
private static Logger log=Logger.getLogger(UserDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Created By Bhagya on May 07th, 2021
	 * @param user
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Service for save or update user
	 */
	public Integer saveOrUpdateUser(User user) throws UserNotSavedOrUpdatedException{
		log.info("UserDaoImpl-> saveOrUpdateAdminUser()");
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		sessionFactory.getCurrentSession().flush();
		return user.getUserId();
	}
	/**
	 * Created By Bhagya on May 07th,2021
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to get the user by authentication
	 */
	@SuppressWarnings("unchecked")
	public User getUserforAuthentication(String username)throws UserNotFoundException{
		log.info("UserDaoImpl-> getUserforAuthentication() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.or(Restrictions.eqOrIsNull("emailId", username),Restrictions.eq("username", username)));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya on May 07th, 2021
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to get the user by userId
	 */
	@SuppressWarnings("unchecked")
	public User getUserByUserId(Integer userId) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getUserByUserId() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("userId", userId));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
		
	}
	
	
	/**
	 * Created By Harshitha on May 17h, 2021
	 * @param fronter
	 * @return
	 *  @throws FronterNotSavedOrUpdatedException
	 *  
	 * Service for save or update Fronter
	 */
	public Integer saveOrUpdateFronter(Fronter fronter) throws FronterNotSavedOrUpdatedException
	{	
	log.info("UserDaoImpl-> saveOrUpdateFronter()");
		sessionFactory.getCurrentSession().saveOrUpdate(fronter);
		sessionFactory.getCurrentSession().flush();
		return fronter.getFronterId();
	}  
	
	
	/**
	 * Created By Harshitha on May 17h, 2021
	 * @param fronterGrandTotal
	 * @return
	 *  @throws FronterGrandTotalNotSavedOrUpdatedException
	 *  
	 * Service for save or update Fronter Grand Total
	 */
	public Integer saveOrUpdateFronterGrandTotal(FrontersGrandTotal fronterGrandTotal) throws FronterGrandTotalNotSavedOrUpdatedException
	{
	log.info("UserDaoImpl-> saveOrUpdateFronterGrandTotal()");
		sessionFactory.getCurrentSession().saveOrUpdate(fronterGrandTotal);
		sessionFactory.getCurrentSession().flush();
		return fronterGrandTotal.getFronterGrandTotalId();
	} 
	
	/**
	 * Created By Harshitha on May 17h, 2021
	 * @param frontersBoardData
	 * @return
	 *  @throws FronterGrandTotalNotSavedOrUpdatedException
	 *  
	 * Service for save or update Fronter Board Data
	 */
	public Integer saveOrUpdateFronterBoarddata(FrontersBoardData frontersBoardData) throws FronterBoardDataNotSavedOrUpdatedException
	{
	log.info("UserDaoImpl-> saveOrUpdateFronterBoarddata()");
		sessionFactory.getCurrentSession().saveOrUpdate(frontersBoardData);
		sessionFactory.getCurrentSession().flush();
		return frontersBoardData.getFronterBoardDataId();
	}
	
	
	/**
	 * Created By Harshitha on May 17h, 2021
	 * @param closer
	 * @return
	 *  @throws FronterGrandTotalNotSavedOrUpdatedException
	 *  
	 * Service for save or update closer details
	 */
	public Integer saveOrUpdateClosers(Closer closer) throws CloserNotSavedOrUpdatedException
	{
	log.info("UserDaoImpl-> saveOrUpdateClosers()");
		sessionFactory.getCurrentSession().saveOrUpdate(closer);
		sessionFactory.getCurrentSession().flush();
		return closer.getCloserId();
	}
	
	
	/**
	 * Created By Harshitha on May 17h, 2021
	 * @param closersGrandTotal
	 * @return
	 *  @throws FronterGrandTotalNotSavedOrUpdatedException
	 *  
	 * Service for save or update closer Grand Total
	 */
	public Integer saveOrUpdateCloserGrandTotal(ClosersGrandTotal closersGrandTotal) throws CloserGrandTotalNotSavedOrUpdatedException
	{
	log.info("UserDaoImpl-> saveOrUpdateCloserGrandTotal()");
		sessionFactory.getCurrentSession().saveOrUpdate(closersGrandTotal);
		sessionFactory.getCurrentSession().flush();
		return closersGrandTotal.getCloserGrandTotalId();
	}
	
	/**
	 * Created By Harshitha on May 17h, 2021
	 * @param closersBoardData
	 * @return
	 *  @throws FronterGrandTotalNotSavedOrUpdatedException
	 *  
	 * Service for save or update closer board data
	 */
	public Integer saveOrUpdateClosersBoarddata(ClosersBoardData closersBoardData) throws CloserBoardDataNotSavedOrUpdatedException
	{
	log.info("UserDaoImpl-> saveOrUpdateClosersBoarddata()");
		sessionFactory.getCurrentSession().saveOrUpdate(closersBoardData);
		sessionFactory.getCurrentSession().flush();
		return closersBoardData.getCloserBoardDataId();
	}
     
	/**
	 * Created By Harshitha on May 19th, 2021
	 * @param fronterId
	 * @return
	 * @throws FronterNotFoundException
	 * 
	 * Service for to get the fronter by fronterId
	 */
	@SuppressWarnings("unchecked")
	public Fronter getFronterByFronterId(Integer fronterId) throws FronterNotFoundException{
		log.info("Inside UserDaoImpl -> getFronterByFronterId() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Fronter.class);
		criteria.add(Restrictions.eq("fronterId", fronterId));
		ArrayList<Fronter> fronter = (ArrayList<Fronter>) criteria.list();
		if(!fronter.isEmpty()){
			return fronter.get(0);
		}
		else{
			throw new FronterNotFoundException();
		}
		
	}
	
	/**
	 * Created By Harshitha on May 19th, 2021
	 * @param closerId
	 * @return
	 * @throws CloserNotFoundException
	 * 
	 * Service for to get the closer by closerId
	 */
	@SuppressWarnings("unchecked")
	public Closer getCloserByCloserId(Integer closerId) throws CloserNotFoundException{
		log.info("Inside UserDaoImpl -> getCloserByCloserId() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Closer.class);
		criteria.add(Restrictions.eq("closerId", closerId));
		ArrayList<Closer> closer = (ArrayList<Closer>) criteria.list();
		if(!closer.isEmpty()){
			return closer.get(0);
		}
		else{
			throw new CloserNotFoundException();
		}
		
	}
	/**
	 * Created By BHagya on May 21st, 2021
	 * @param user
	 * @return
	 * @throws FronterNotFoundException
	 * 
	 * Service for to get fronter by user
	 */
	@SuppressWarnings("unchecked")
	public Fronter getFronterByUser(User user) throws FronterNotFoundException {
		log.info("Inside UserDaoImpl -> getFronterByUser() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Fronter.class);
		criteria.add(Restrictions.eq("user", user));
		ArrayList<Fronter> fronters = (ArrayList<Fronter>) criteria.list();
		if(!fronters.isEmpty()){
			return fronters.get(0);
		}
		else{
			throw new FronterNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya on mAy 21st, 2021
	 * @param user
	 * @return
	 * @throws CloserNotFoundException
	 * 
	 * Service for to get closer by user
	 */
	@SuppressWarnings("unchecked")
	public Closer getCloserByUser(User user) throws CloserNotFoundException {
		log.info("Inside UserDaoImpl -> getCloserByUser() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Closer.class);
		criteria.add(Restrictions.eq("user", user));
		ArrayList<Closer> closers = (ArrayList<Closer>) criteria.list();
		if(!closers.isEmpty()){
			return closers.get(0);
		}
		else{
			throw new CloserNotFoundException();
		}
		
	}
	/**
	 * Created By BHagya on Mya 21st 2021
	 * @return
	 * @throws FronterNotFoundException
	 * 
	 * Service for to get fronters list and that user should be active
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Fronter> getFrontersData(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws FronterNotFoundException{
		log.info("Inside UserDaoImpl -> getFrontersData() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Fronter.class);
		Criteria userCriteria = criteria.createCriteria("user",JoinType.INNER_JOIN);
        userCriteria.add(Restrictions.eq("isActive", true));
        
        if (searchBy != null && !searchBy.isEmpty()) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("username", searchBy,MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("firstName", searchBy,MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("LastName", searchBy,MatchMode.ANYWHERE));
			userCriteria.add(disjunction);
		}
		
		if(null!=sortBy){
			if(sortBy.equalsIgnoreCase("username")) {
				if(ascending){
					userCriteria.addOrder(Order.asc(sortBy));			
				}
				else{
					
					userCriteria.addOrder(Order.desc(sortBy));
					
				}
			}
			else {
				if(ascending){
					criteria.addOrder(Order.asc(sortBy));			
				}
				else{
					
					criteria.addOrder(Order.desc(sortBy));
					
				}	
			}
		}
		
		Integer totalFronters=criteria.list().size();
		
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		
		ArrayList<Fronter> fronters = (ArrayList<Fronter>) criteria.list();
		if(!fronters.isEmpty()){
			fronters.get(0).setTotalFronters(totalFronters);
			return fronters;
			
		}
		else{
			throw new FronterNotFoundException();
		}
	}
	/**
	 * Created By Bhagya on May 22nd, 2021
	 * @return
	 * @throws FrontersBoardDataNotFoundException
	 * 
	 * Service for to get fronters board data
	 */
	@SuppressWarnings("unchecked")
	public FrontersBoardData getFrontersBoardData() throws FrontersBoardDataNotFoundException {
		log.info("Inside UserDaoImpl -> getFrontersBoardData() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(FrontersBoardData.class);
		ArrayList<FrontersBoardData> frontersBoardData = (ArrayList<FrontersBoardData>) criteria.list();
		if(!frontersBoardData.isEmpty()){
			return frontersBoardData.get(0);
		}
		else{
			throw new FrontersBoardDataNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya On MAy 22nd, 2021
	 * Service for to calculate fonters grand total
	 */
	
	public FrontersGrandTotalDto getFrontersGrandTotal() {
		log.info("Inside UserDaoImpl -> getFrontersGrandTotal() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Fronter.class);
		Projection projection = Projections.sum("mon"); 
		Projection projection2 = Projections.sum("tues"); 
		Projection projection3 = Projections.sum("wed"); 
		Projection projection4 = Projections.sum("thurs");
		Projection projection5 = Projections.sum("fri");
		Projection projection6 = Projections.sum("total");
		ProjectionList pList = Projections.projectionList(); 
		pList.add(projection); 
		pList.add(projection2); 
		pList.add(projection3);
		pList.add(projection4);
		pList.add(projection5);
		pList.add(projection6);
		criteria.setProjection(pList);
		List list = criteria.list();
		Object[] object=(Object[]) list.get(0);
		
		FrontersGrandTotalDto frontersGrandTotalDto=new FrontersGrandTotalDto();
		Long mon=(Long)object[0];
		Long tues=(Long)object[1];
		Long wed=(Long)object[2];
		Long thurs=(Long)object[3];
		Long fri=(Long)object[4];
		Long total=(Long)object[5];
		try {
		frontersGrandTotalDto.setMonTotal(mon.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		frontersGrandTotalDto.setTuesTotal(tues.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		frontersGrandTotalDto.setWedTotal(wed.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		frontersGrandTotalDto.setThursTotal(thurs.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		frontersGrandTotalDto.setFriTotal(fri.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		frontersGrandTotalDto.setGrandTotal(total.intValue());
		
		}
		catch(NullPointerException e) {
			
		}
		
		return frontersGrandTotalDto;
	}
	/**
	 * Created By Bhagya on MAy 22nd, 2021
	 * @return
	 * @throws FrontersGrandTotalNotFoundException
	 * 
	 * Service for to get the fronters grand total
	 */
	@SuppressWarnings("unchecked")
	public FrontersGrandTotal getFrontersGrandTotalData() throws FrontersGrandTotalNotFoundException {
		log.info("Inside UserDaoImpl -> getFrontersGrandTotalData() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(FrontersGrandTotal.class);
		ArrayList<FrontersGrandTotal> frontersGrandTotal = (ArrayList<FrontersGrandTotal>) criteria.list();
		if(!frontersGrandTotal.isEmpty()){
			return frontersGrandTotal.get(0);
		}
		else{
			throw new FrontersGrandTotalNotFoundException();
		}
		
	}
	
	/**
	 * Created By Harshitha on May 24th, 2021
	 * @return
	 * @throws ClosersGrandTotalNotFoundException
	 * 
	 * Service for to get the closers grand total
	 */
	@SuppressWarnings("unchecked")
	public ClosersGrandTotal getClosersGrandTotalData() throws ClosersGrandTotalNotFoundException {
		log.info("Inside UserDaoImpl -> getClosersGrandTotalData() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ClosersGrandTotal.class);
		ArrayList<ClosersGrandTotal> closersGrandTotal = (ArrayList<ClosersGrandTotal>) criteria.list();
		if(!closersGrandTotal.isEmpty()){
			return closersGrandTotal.get(0);
		}
		else{
			throw new ClosersGrandTotalNotFoundException();
		}
		
	}
	
	/**
	 * Created By Harshitha On MAy 24th, 2021
	 * Service for to calculate closers grand total
	 */
	
	public ClosersGrandTotalDto getClosersGrandTotal() {
		log.info("Inside UserDaoImpl -> getClosersGrandTotal() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Closer.class);
		Projection projection = Projections.sum("mon"); 
		Projection projection2 = Projections.sum("tues"); 
		Projection projection3 = Projections.sum("wed"); 
		Projection projection4 = Projections.sum("thurs");
		Projection projection5 = Projections.sum("fri");
		Projection projection6 = Projections.sum("total");
		ProjectionList pList = Projections.projectionList(); 
		pList.add(projection); 
		pList.add(projection2); 
		pList.add(projection3);
		pList.add(projection4);
		pList.add(projection5);
		pList.add(projection6);
		criteria.setProjection(pList);
		List list = criteria.list();
		Object[] object=(Object[]) list.get(0);
		
		ClosersGrandTotalDto closerGrandTotalDto=new ClosersGrandTotalDto();
		Long mon=(Long)object[0];
		Long tues=(Long)object[1];
		Long wed=(Long)object[2];
		Long thurs=(Long)object[3];
		Long fri=(Long)object[4];
		Long total=(Long)object[5];
		try {
		closerGrandTotalDto.setMonTotal(mon.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		closerGrandTotalDto.setTuesTotal(tues.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		closerGrandTotalDto.setWedTotal(wed.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		closerGrandTotalDto.setThursTotal(thurs.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		closerGrandTotalDto.setFriTotal(fri.intValue());
		}
		catch(NullPointerException e) {
			
		}
		try {
		closerGrandTotalDto.setGrandTotal(total.intValue());
		}
		catch(NullPointerException e) {
			
		}
		return closerGrandTotalDto;
	}
	
	/**
	 * Created By Harshitha on May 25th, 2021
	 * @return
	 * @throws ClosersBoardDataNotFoundException
	 * 
	 * Service for to get closers board data
	 */
	@SuppressWarnings("unchecked")
	public ClosersBoardData getClosersBoardData() throws ClosersBoardDataNotFoundException {
		log.info("Inside UserDaoImpl -> getClosersBoardData() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ClosersBoardData.class);
		ArrayList<ClosersBoardData> closersBoardData = (ArrayList<ClosersBoardData>) criteria.list();
		if(!closersBoardData.isEmpty()){
			return closersBoardData.get(0);
		}
		else{
			throw new ClosersBoardDataNotFoundException();
		}
		
	}
	
	/**
	 * Created By Harshitha on May 25th 2021
	 * @return
	 * @throws CloserNotFoundException
	 * 
	 * Service for to get closers list
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Closer> getClosersData(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws CloserNotFoundException{
		log.info("Inside UserDaoImpl -> getClosersData() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Closer.class);
		Criteria userCriteria = criteria.createCriteria("user",JoinType.INNER_JOIN);
        userCriteria.add(Restrictions.eq("isActive", true));
        
        if (searchBy != null && !searchBy.isEmpty()) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("username", searchBy,MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("firstName", searchBy,MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("LastName", searchBy,MatchMode.ANYWHERE));
			userCriteria.add(disjunction);
		}
		
		if(null!=sortBy){
			if(sortBy.equalsIgnoreCase("username")) {
				if(ascending){
					userCriteria.addOrder(Order.asc(sortBy));			
				}
				else{
					
					userCriteria.addOrder(Order.desc(sortBy));
					
				}
			}
			else {
				if(ascending){
					criteria.addOrder(Order.asc(sortBy));			
				}
				else{
					
					criteria.addOrder(Order.desc(sortBy));
					
				}	
			}
		}
		
		Integer totalClosers=criteria.list().size();
		
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<Closer> closers = (ArrayList<Closer>) criteria.list();
		if(!closers.isEmpty()){
			closers.get(0).setTotalClosers(totalClosers);
			return closers;
		}
		else{
			throw new CloserNotFoundException();
		}
	}
	
	/**
	 * created By Harshitha on June 3rd, 2021
	 * @return
	 * @throws NotificationConfigurationNotSavedOrUpdatedException
	 * 
	 * Service for to save or update Notification Configuration
	 */
	
	public Integer saveOrUpdateNotificationConfiguration(NotificationConfiguration notificationConfiguration) throws NotificationConfigurationNotSavedOrUpdatedException
	{	
	log.info("UserDaoImpl-> saveOrUpdateNotificationConfiguration");
		sessionFactory.getCurrentSession().saveOrUpdate(notificationConfiguration);
		sessionFactory.getCurrentSession().flush();
		return notificationConfiguration.getConfigId();
	}

	/**
	 * created by Harshitha on june 3rd, 2021
	 * @param user
	 * @return
	 * @throws NotificationConfigurationNotFoundException
	 * 
	 * service for to get the Notification configuration by
	 * user
	 */
	
	@SuppressWarnings("unchecked")
	public NotificationConfiguration getNotificationConfigurationByUser(User user) throws NotificationConfigurationNotFoundException
	{
	log.info("Inside UserDaoImpl -> getNotificationConfigurationByUser ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(NotificationConfiguration.class);
		criteria.add(Restrictions.eq("user", user));
		ArrayList<NotificationConfiguration> notificationConfiguration = (ArrayList<NotificationConfiguration>) criteria.list();
		if(!notificationConfiguration.isEmpty()){
			return notificationConfiguration.get(0);
		}
		else{
			throw new NotificationConfigurationNotFoundException();
		}
		
	}
	
	/**
	 * created By Harshitha on June 7th, 2021
	 * @return
	 * @throws MessageBoardNotSavedOrUpdatedException
	 * 
	 * Service for to save or update MessageBoard
	 */
	
	public Integer saveOrUpdateMessage(MessageBoard messageBoard) throws MessageBoardNotSavedOrUpdatedException
	{	
	log.info("UserDaoImpl-> saveOrUpdateMessageBoard");
		sessionFactory.getCurrentSession().saveOrUpdate(messageBoard);
		sessionFactory.getCurrentSession().flush();
		return messageBoard.getMessageId();
	}
	
	/**
	 * Created By Harshitha on June 7th, 2021
	 * @return
	 * 
	 * Service for to get the messages
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<MessageBoard> getMessages() throws MessagesNotFoundException{
		log.info("Inside UserDaoImpl -> getMessages() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(MessageBoard.class);
		ArrayList<MessageBoard> messageBoard = (ArrayList<MessageBoard>) criteria.list();
		if(!messageBoard.isEmpty()){
			return messageBoard;
		}
		else{
			throw new MessagesNotFoundException();
		}
		
	}
	
	/**
	 * created By Harshitha on June 16th, 2021
	 * @return
	 * @throws UserNotificationCountNotSavedOrUpdatedException
	 * 
	 * Service for to save or update Notification Count
	 */
	
	public Integer saveOrUpdateNotificationCount(UserNotificationCount userNotificationCount) throws UserNotificationCountNotSavedOrUpdatedException
	{	
	log.info("UserDaoImpl-> saveOrUpdateNotificationCount");
		sessionFactory.getCurrentSession().saveOrUpdate(userNotificationCount);
		sessionFactory.getCurrentSession().flush();
		return userNotificationCount.getCountId();
	}
	
	/**
	 * created by Harshitha on june 16th, 2021
	 * @param user
	 * @return
	 * @throws UserNotificationCountNotFoundException
	 * 
	 * service for to get the User Notification Count by
	 * user
	 */
	
	@SuppressWarnings("unchecked")
	public UserNotificationCount getUserNotificationCountByUserId(Integer userId) throws UserNotificationCountNotFoundException
	{
	log.info("Inside UserDaoImpl -> getUserNotificationCountByUserId ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserNotificationCount.class);
		criteria.add(Restrictions.eq("userId", userId));
		ArrayList<UserNotificationCount> userNotificationCount = (ArrayList<UserNotificationCount>) criteria.list();
		if(!userNotificationCount.isEmpty()){
			return userNotificationCount.get(0);
		}
		else{
			throw new UserNotificationCountNotFoundException();
		}
		
	}
	
	/**
	 * Created By Harshitha on June 16th, 2021
	 * @return
	 * 
	 * UserNotificationCountNotFoundException
	 * Service for to get User Notification Count By All User
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserNotificationCount> getUserNotificationCountForAllUser() throws UserNotificationCountNotFoundException{
		log.info("Inside UserDaoImpl -> getMessages() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserNotificationCount.class);
		ArrayList<UserNotificationCount> userNotificationCount = (ArrayList<UserNotificationCount>) criteria.list();
		if(!userNotificationCount.isEmpty()){
			return userNotificationCount;
		}
		else{
			throw new UserNotificationCountNotFoundException();
		}
		
	}
	
	/**
	 * Created By Harshitha on June 17th, 2021
	 * @return
	 * 
	 * NotificationCountNotUpdatedException
	 * Service for to get User Notification Count By UserId
	 */
	@SuppressWarnings("unchecked")
	public void updateNotificationCount(Integer userId,Boolean scoreMessage) throws NotificationCountNotUpdatedException {
		log.info("Inside UserDaoImpl -> updateNotificationCount() ");
		/*
		 * code to update notification Count except who has 
		 * 1. disabled Score/message notification. 
		 * 2. sent the message(loggedIn user). 
		 * 3. for inactive users
		 * 
		 */
		ArrayList<NotificationConfiguration> disabledConfigurationUsers = null;
		if(scoreMessage==true) {
			try {
				disabledConfigurationUsers=this.getNotficationConfigurationOfDisabledUsers(true);
			} catch (NotificationConfigurationNotFoundException e) {
				
			}	
		}
		else {
			try {
				disabledConfigurationUsers=this.getNotficationConfigurationOfDisabledUsers(false);
			} catch (NotificationConfigurationNotFoundException e) {
				
			}
		}
		ArrayList<Integer> usersList=new ArrayList<Integer>();
		usersList.add(userId); // loggedIN
		StringBuffer listBuffer=new StringBuffer();
		listBuffer.append(userId);
		 if(null!=disabledConfigurationUsers && !disabledConfigurationUsers.isEmpty()){
			  for(NotificationConfiguration notificationConfiguration:disabledConfigurationUsers) {
			  usersList.add(notificationConfiguration.getUser().getUserId()); 
			  listBuffer.append(","+notificationConfiguration.getUser().getUserId());
			  
			  }
		  }
		
		System.out.println(" usersLists "+usersList.toString());
		
		System.out.println(" list "+listBuffer);
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery("UPDATE user_notification_count set notification_count = notification_count+1 where userId not in("+listBuffer+")");
	    query.executeUpdate();
		}
		
	


		/**
		 * created by Harshitha on june 18th, 2021
		 * @param scoreNotification
		 * @return
		 * @throws ScoreMessageNotificationNotFoundException
		 * 
		 * service for to get Score Message Notification
		 * 
		 */
		
		@SuppressWarnings("unchecked")
		public ArrayList<NotificationConfiguration> getNotficationConfigurationOfDisabledUsers(Boolean scoreNotification) throws NotificationConfigurationNotFoundException
		{
		log.info("Inside UserDaoImpl -> getNotficationConfigurationOfDisabledUsers ");
			Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(NotificationConfiguration.class);
			if (scoreNotification==true) {
				criteria.add(Restrictions.eq("scoreNotification", false));
			}
			else {
				criteria.add(Restrictions.eq("messageNotification", false));
			}
			ArrayList<NotificationConfiguration> notificationConfigurations = (ArrayList<NotificationConfiguration>) criteria.list();
			if(!notificationConfigurations.isEmpty()){
				return notificationConfigurations;
			}
			else {
				throw new NotificationConfigurationNotFoundException();
			}
		 }


}

