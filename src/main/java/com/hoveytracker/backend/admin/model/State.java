package com.hoveytracker.backend.admin.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="states")
public class State implements Serializable{
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="state_id")
	private Integer stateId;
	
	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name="state_name")
	private String stateName;
	
	
	
}