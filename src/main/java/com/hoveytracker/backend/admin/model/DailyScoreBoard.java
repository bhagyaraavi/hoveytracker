package com.hoveytracker.backend.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.hoveytracker.backend.user.model.User;

/**
 * 
 * @author Bhagya
 * Created on may 14th, 2021
 * 
 * Model class for Daily Scoreboard
 *
 */
@Entity
@Table(name="daily_scoreboard")
public class DailyScoreBoard implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="score_id")
	private Integer scoreId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;
	
	@ManyToOne(targetEntity=User.class)
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="deals_count")
	private String dealsCount;
	
	@Column(name="xfers_count")
	private String xfersCount;

	public Integer getScoreId() {
		return scoreId;
	}

	public void setScoreId(Integer scoreId) {
		this.scoreId = scoreId;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDealsCount() {
		return dealsCount;
	}

	public void setDealsCount(String dealsCount) {
		this.dealsCount = dealsCount;
	}

	public String getXfersCount() {
		return xfersCount;
	}

	public void setXfersCount(String xfersCount) {
		this.xfersCount = xfersCount;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	
}

