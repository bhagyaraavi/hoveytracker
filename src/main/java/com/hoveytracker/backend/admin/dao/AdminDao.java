package com.hoveytracker.backend.admin.dao;

import java.util.ArrayList;

import com.hoveytracker.backend.admin.model.DailyScoreBoard;
import com.hoveytracker.backend.admin.model.State;
import com.hoveytracker.backend.user.model.Closer;
import com.hoveytracker.backend.user.model.Fronter;
import com.hoveytracker.backend.user.model.User;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotFoundException;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterNotFoundException;
import com.hoveytracker.common.exceptions.StateNotFoundException;
import com.hoveytracker.common.exceptions.UserNotFoundException;

public interface AdminDao{
	
	public User getUserByPasswordResetToken(String token) throws UserNotFoundException;
	public User getUserByAccountToken(String token)throws UserNotFoundException;
	public ArrayList<State> getStatesListFromDB() throws StateNotFoundException;
	public ArrayList<User>  getListOfUsers(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws UserNotFoundException;
	public Integer saveOrUpdateDailyScoreBoard(DailyScoreBoard dailyScoreBoard) throws DailyScoreBoardNotSavedOrUpdatedException;
	public DailyScoreBoard getDailyScoreBoard() throws DailyScoreBoardNotFoundException;

	/**
	 * Created By Harshitha on May 27th, 2021
	 */
	public Integer deleteFronter(Fronter fronter);
	public Integer deleteCloser(Closer closer);
}



