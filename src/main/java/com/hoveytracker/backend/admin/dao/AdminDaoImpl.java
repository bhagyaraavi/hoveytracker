package com.hoveytracker.backend.admin.dao;

import java.util.ArrayList;


import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hoveytracker.backend.admin.model.DailyScoreBoard;
import com.hoveytracker.backend.admin.model.State;
import com.hoveytracker.backend.user.model.Closer;
import com.hoveytracker.backend.user.model.Fronter;
import com.hoveytracker.backend.user.model.User;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotFoundException;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.StateNotFoundException;
import com.hoveytracker.common.exceptions.UserNotFoundException;
import com.hoveytracker.common.exceptions.UserNotSavedOrUpdatedException;
import com.hoveytracker.frontend.admin.dto.DailyScoreBoardDto;


@Repository("adminDao")
@Transactional
public  class AdminDaoImpl implements AdminDao{
	
private static Logger log=Logger.getLogger(AdminDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**Created By Bhagya On may 07th, 2021
	 * Returns the user's  one who has given Password reset token
	 * password reset token sent to the user
	 * 
	 * @param token
	 * @throws UserNotFoundException 
	 * @throws Exception
	 *             if there is no user with given token OR any kind of exception
	 *             while interacting with db
	 */
	@SuppressWarnings("unchecked")
	public User getUserByPasswordResetToken(String token) throws UserNotFoundException {
		log.info("AdminDaoImpl-> getUserByPasswordResetToken()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("passwordToken", token));
		ArrayList<User> users=(ArrayList<User>) criteria.list();
		if (!users.isEmpty()) {
			return users.get(0);
		} else {
			throw new UserNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya on May 07th, 2021
	 * Method to Retrieve User based on accountToken..
	 */
     @SuppressWarnings("unchecked")
	public User getUserByAccountToken(String token)throws UserNotFoundException{
    	 log.info("AdminDaoImpl-> getUserByAccountToken()");
    	 ArrayList<User> user=(ArrayList<User>) sessionFactory.getCurrentSession().createCriteria(User.class)
    			 .add(Restrictions.eq("accountActivationToken",token))
    			 .list();
    	 
    	 if(!user.isEmpty()){
    		 return user.get(0);
    	 }
    	 else{
    		 throw new UserNotFoundException();
    	 }    	 
     }
     /**
      * Created By BHagya on mAy 12th, 2021
      * @return
      * @throws StateNotFoundException
      * 
      * Service for to get the list of states
      */
     @SuppressWarnings("unchecked")
    public ArrayList<State> getStatesListFromDB() throws StateNotFoundException{
    	 log.info("AdminDaoImpl -> getStatesListFromDB()");
    	 ArrayList<State> states=(ArrayList<State>) sessionFactory.getCurrentSession().createCriteria(State.class).list();
    	 if(!states.isEmpty()){
    		 return states;
    	 }
    	 else{
    		 throw new StateNotFoundException();
    	 }    
     }
     
     /**
      * Created By Bhagya on may 13th, 2021
      * Service for to get the list of users
      */
 	
 	@SuppressWarnings("unchecked")
 	public ArrayList<User>  getListOfUsers(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws UserNotFoundException{
 		log.info("AdminDaoImpl ->  getListOfUsers()");
 		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
 		
 		 if (searchBy != null && !searchBy.isEmpty()) {
 			Disjunction disjunction = Restrictions.disjunction();
 			disjunction.add(Restrictions.ilike("username", searchBy,MatchMode.ANYWHERE));
 			disjunction.add(Restrictions.ilike("firstName", searchBy,MatchMode.ANYWHERE));
 			disjunction.add(Restrictions.ilike("LastName", searchBy,MatchMode.ANYWHERE));
 			disjunction.add(Restrictions.ilike("emailId", searchBy,MatchMode.ANYWHERE));
 			disjunction.add(Restrictions.ilike("userType", searchBy,MatchMode.ANYWHERE));
 			criteria.add(disjunction);
 		}
 		
 		if(null!=sortBy){
 				if(ascending){
 					criteria.addOrder(Order.asc(sortBy));			
 				}
 				else{
 					
 					criteria.addOrder(Order.desc(sortBy));
 					
 				}	
 		}
 		
 		Integer totalUsers=criteria.list().size();
 		
 		if(null!=pageNo){
 			criteria.setFirstResult(pageNo*pageSize);
 			criteria.setMaxResults(pageSize);
 		}	
 		ArrayList<User> users=(ArrayList<User>) criteria.list();
 		if(!users.isEmpty()){
 			users.get(0).setTotalUsers(totalUsers);
 			return users;
 		}
 		else{
 			 throw new UserNotFoundException();
 		}
 		
 	}
 	/**
 	 * Created By Bhagya on May 17th, 2021
 	 * @param dailyScoreBoard
 	 * @return
 	 * @throws DailyScoreBoardNotSavedOrUpdatedException
 	 * 
 	 * Service for to save or update dailyscoreboard data
 	 */
 	public Integer saveOrUpdateDailyScoreBoard(DailyScoreBoard dailyScoreBoard) throws DailyScoreBoardNotSavedOrUpdatedException{
		log.info("AdminDaoImpl-> saveOrUpdateDailyScoreBoard()");
		sessionFactory.getCurrentSession().saveOrUpdate(dailyScoreBoard);
		sessionFactory.getCurrentSession().flush();
		return dailyScoreBoard.getScoreId();
	}
 	/**
 	 * Created By Bhagya on may 17th, 2021
 	 * @return
 	 * @throws DailyScoreBoardNotFoundException
 	 * 
 	 * Service for to get the daily scoreboard
 	 */
 	
 	@SuppressWarnings("unchecked")
 	public DailyScoreBoard getDailyScoreBoard() throws DailyScoreBoardNotFoundException {
 		log.info("AdminDaoImpl ->  getDailyScoreBoard()");
 		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(DailyScoreBoard.class);
 		ArrayList<DailyScoreBoard> dailyScoreBoards=(ArrayList<DailyScoreBoard>) criteria.list();
 		if(!dailyScoreBoards.isEmpty()){
 			return dailyScoreBoards.get(0);
 		}
 		else{
 			 throw new DailyScoreBoardNotFoundException();
 		}
 	}
 	
 	/**
	 * Created By Harshitha On May 27th, 2021
	 * @param fronter
	 * @return
	 * Service for to delete the fronter
	 */
	public Integer deleteFronter(Fronter fronter) {
		log.info("AdminDaoImpl -> deleteFronter()");
		sessionFactory.getCurrentSession().delete(fronter);
		sessionFactory.getCurrentSession().flush();
		return fronter.getFronterId();
	}
	
	/**
	 * Created By Harshitha On May 27th, 2021
	 * @param closer
	 * @return
	 * Service for to delete the closer
	 */
	public Integer deleteCloser(Closer closer) {
		log.info("AdminDaoImpl -> deleteCloser()");
		sessionFactory.getCurrentSession().delete(closer);
		sessionFactory.getCurrentSession().flush();
		return closer.getCloserId();
	}
}