package com.hoveytracker.frontend.user.controller;

import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hoveytracker.frontend.user.dto.UserDto;
import com.hoveytracker.frontend.user.dto.UserNotificationCountDto;
import com.hoveytracker.frontend.user.service.UserService;



/**
 * 
 * @author Bhagya
 * Created on may 10th, 2021
 * Controller class for Login functionality
 *
 */

@Controller("loginController")
public class LoginController{
	
private static Logger log=Logger.getLogger(LoginController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	
	/**Created BY BHagya on May 10th, 2021
	 * Init method for to get the Login page
	 * @return
	 */
	@RequestMapping(value="/login.do",method=RequestMethod.GET)
	public String login(Map<String,Object> map){
		log.info("LoginController ->  login");
		
		try{		
			
			 return "login";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while initiate login "+e.getMessage());
			map.put("message", "Error while initiate login");
			return "error";
		}
	}
	
	/**
	 * Created by bhagya on may 10th,2021
	 * @param request
	 * @param map
	 * @return
	 * 
	 * Method for handling the process after login success
	 */
	
	
	@RequestMapping(value="/loginsuccess.do",method=RequestMethod.GET)
	public String redirectToLoginSuccess(HttpServletRequest request,Map<String, Object> map){
		log.info("LoginController -> redirectToLoginSuccess");
		HttpSession session=request.getSession(true);
		try{
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();
			String user=auth.getName();
			UserDto userDto=this.userService.getUserByUsernameOrEmailId(user);
			
			session.setAttribute("username", userDto.getUsername());
			session.setAttribute("userId", userDto.getUserId());
			session.setAttribute("userType", userDto.getUserType());
			
			String error="Problem While Login, Please Try Again Later";
			map.put("message",error);
			return "common/dashboard";
			//return "error";
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while login "+e.getMessage());
			String error="Problem While Login, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		
	}
	
	
	/**
	 * Created by Bhagya on May 10th, 2021
	 * Method to handle Logout COndition
	 * Removes all cache and send to logout page
	 * 
	 */
	@RequestMapping(value="/logoutsuccess.do", method=RequestMethod.GET)
	public String logoutHandler(Map<String, Object>map,HttpServletRequest request,HttpServletResponse response){
		log.info("LoginController -> logoutHandler()");
		try{
			
			HttpSession session=request.getSession(false);
			if(null!=session)
				session.invalidate();
			response.reset();
			   response.setHeader("Cache-Control", "no-cache");
			   response.setHeader("Pragma", "no-cache");
			    response.setHeader("Cache-Control", "no-store");
			   response.setHeader("Cache-Control", "must-revalidate");
			   response.setDateHeader("Expires", 0); 
			   String message="Successfully Logged Out";
			   map.put("message", message);			  
			  return "login";
		}
		catch(Exception e){
			log.info("User Logged Out"+ e.toString() +" "+e.getMessage());
			e.printStackTrace();
			String message="Error while logging out the User";
			   map.put("message", message);
			return "error";
		}
	}
	
}