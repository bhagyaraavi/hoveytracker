package com.hoveytracker.frontend.user.controller;


import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hoveytracker.common.exceptions.CloserNotFoundException;
import com.hoveytracker.common.exceptions.FronterNotFoundException;
import com.hoveytracker.common.exceptions.MessageBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.MessagesNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.NotificationCountNotUpdatedException;
import com.hoveytracker.frontend.user.dto.CloserDto;
import com.hoveytracker.frontend.user.dto.ClosersBoardDataDto;
import com.hoveytracker.frontend.user.dto.ClosersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.FronterDto;
import com.hoveytracker.frontend.user.dto.FrontersBoardDataDto;
import com.hoveytracker.frontend.user.dto.FrontersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.MessageBoardDto;
import com.hoveytracker.frontend.user.dto.NotificationConfigurationDto;
import com.hoveytracker.frontend.user.dto.UserDto;
import com.hoveytracker.frontend.user.dto.UserNotificationCountDto;
import com.hoveytracker.frontend.user.service.UserService;
import com.hoveytracker.frontend.utility.dto.DisplayListBeanDto;

/**
 * Created On May 07th,2021
 * @author Bhagya
 * 
 * Controller class User Functionality
 *
 */
@Controller("userController")
@RequestMapping("/user")
public class UserController{
	
private static Logger log=Logger.getLogger(UserController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	/**
	 * Created By BHagya on may 21st 2021
	 * @param map
	 * @return
	 * 
	 * Service for intializing the deal close
	 */
	
	@RequestMapping(value="/dealclose.do",method=RequestMethod.GET)
	public String initDealClose(Map<String,Object> map,HttpServletRequest request){
		log.info( " UserController -> initDealClose()");

		try{
			HttpSession session=request.getSession(true);
			Integer userId=(Integer) session.getAttribute("userId");
			map.put("userId", userId);
			return "common/dealClose";
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating deal close "+e.getMessage());
			map.put("message", "Error while initiating deal close ");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya on may 21st, 2021
	 * @param map
	 * @param request
	 * @return
	 * 
	 * Service for to process the deal - both fronter and closer user type
	 */
	
	@RequestMapping(value="/processdealclose.do",method=RequestMethod.GET)
	public String processDealClose(Map<String,Object> map,HttpServletRequest request,@RequestParam("count") Integer count){
		log.info( " UserController -> processDealClose()");

		try{
			HttpSession session=request.getSession(true);
			Integer userId=(Integer) session.getAttribute("userId");
			String userType=(String) session.getAttribute("userType");
			
			this.userService.processDealClose(userId, userType,count);
			map.put("userId", userId);
			return "common/congratulations";
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating deal close "+e.getMessage());
			map.put("message", "Error while initiating deal close ");
			return "error";
		}
		
	}
	
	/**
	 * Created By Bhagya on May 21st, 2021
	 * @param map
	 * @return
	 * 
	 * Service for to view the fronter board
	 */
	@RequestMapping(value="/fronterboard.do")
	public String initFronterBoard(Map<String,Object> map,HttpServletRequest request,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info( " UserController -> initFronterBoard()");
		Integer totalResults;
		try{
			HttpSession session=request.getSession(true);
			Integer userId=(Integer) session.getAttribute("userId");
			FrontersBoardDataDto frontersBoardDataDto=this.userService.getFrontersBoardData();
			FrontersGrandTotalDto frontersGrandTotalDto=this.userService.getFrontersGrandTotalData();
			if(null!=frontersBoardDataDto) {
				map.put("frontersBoardDataDto",frontersBoardDataDto);
			}
			if(null!=frontersGrandTotalDto) {
				map.put("frontersGrandTotalDto", frontersGrandTotalDto);
			}
			//Default sortBy Monday -> every monday client want to sort the data by total
			String dayName=LocalDate.now().getDayOfWeek().name();
			Calendar now = Calendar.getInstance();
	        Integer currentHour=now.get(Calendar.HOUR_OF_DAY);
	        //have the data sorted and complete in the program anytime between Friday night and / before start of business at 8am CST Monday morning. 
			if((dayName.equalsIgnoreCase("FRIDAY") && currentHour>20) || dayName.equalsIgnoreCase("SATURDAY")|| dayName.equalsIgnoreCase("SUNDAY") ||(dayName.equalsIgnoreCase("MONDAY") && currentHour<8) ) {
				listBeanDto.setSortBy("total");
				listBeanDto.setSortDirection(false);
			}
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("fronterId");
			}
			
			
			ArrayList<FronterDto> fronterDtos=this.userService.getFrontersData(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSearchBy(),listBeanDto.getSortBy(),listBeanDto.getSortDirection());
			totalResults=fronterDtos.get(0).getTotalFronters();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("fronterDtos",fronterDtos);
			map.put("userId",userId);
			return "common/fronterBoard";
			
		}
		catch(FronterNotFoundException e) {
			String error="No fronter was found";
			map.put("message",error);
			return "common/fronterBoard";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating fronter board "+e.getMessage());
			map.put("message", "Error while initiating fronter board");
			return "error";
		}
		
	}
	
	/**
	 * Created By Harshitha on May 25th, 2021
	 * @param map
	 * @return
	 * 
	 * Service for to view the closer board
	 */
	@RequestMapping(value="/closerboard.do")
	public String initCloserBoard(Map<String,Object> map,HttpServletRequest request,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info( " UserController -> initCloserBoard()");
		Integer totalResults;
		try{
			HttpSession session=request.getSession(true);
			Integer userId=(Integer) session.getAttribute("userId");
			ClosersBoardDataDto closersBoardDataDto=this.userService.getClosersBoardData();
			ClosersGrandTotalDto closerGrandTotalDto=this.userService.getClosersGrandTotalData();
			if(null!=closersBoardDataDto) {
			    map.put("closersBoardDataDto",closersBoardDataDto);
			}
			if(null!=closerGrandTotalDto) {
				map.put("closersGrandTotalDto", closerGrandTotalDto);
			}
			//Default sortBy Monday -> every monday client want to sort the data by total
			String dayName=LocalDate.now().getDayOfWeek().name();
	        Calendar now = Calendar.getInstance();
	        Integer currentHour=now.get(Calendar.HOUR_OF_DAY);
	        //have the data sorted and complete in the program anytime between Friday night and / before start of business at 8am CST Monday morning. 
			if((dayName.equalsIgnoreCase("FRIDAY") && currentHour>20) || dayName.equalsIgnoreCase("SATURDAY")|| dayName.equalsIgnoreCase("SUNDAY") ||(dayName.equalsIgnoreCase("MONDAY") && currentHour<8) ) {
				listBeanDto.setSortBy("total");
				listBeanDto.setSortDirection(false); //descending order - highest to lowest
			}
			
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("closerId");
			}
			
			ArrayList<CloserDto> closerDtos=this.userService.getClosersData(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSearchBy(),listBeanDto.getSortBy(),listBeanDto.getSortDirection());
			totalResults=closerDtos.get(0).getTotalClosers();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("closerDtos",closerDtos);
			map.put("userId",userId);
			return "common/closerBoard";
			
		}
		catch(CloserNotFoundException e) {
			String error="No closer was found";
			map.put("message",error);
			return "common/closerBoard";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating closer board "+e.getMessage());
			map.put("message", "Error while initiating closer board");
			return "error";
		}
		
	}
	
	/**
	 * Created By harshitha on june 4th, 2021
	 * @param request
	 * @param map
	 * @param NotificationConfigurationDto
	 * @return
	 * 
	 * Service for to save or update NotificationConfiguration
	 */
	@RequestMapping(value="/savenotificationconfiguration.do", method=RequestMethod.GET)
	@ResponseBody
	public String saveOrUpdateNotificationConfiguration(Map<String,Object> map,@RequestParam("userId")Integer userId, @RequestParam("scoreNotification")String scoreNotification,@RequestParam("messageNotification")String messageNotification){
		log.info( " UserController -> saveOrUpdateNotificationConfiguration()");
		try{
			NotificationConfigurationDto notificationConfigurationDto=new NotificationConfigurationDto();
			notificationConfigurationDto.setScoreNotification(Boolean.valueOf(scoreNotification));
			notificationConfigurationDto.setMessageNotification(Boolean.valueOf(messageNotification));
			Integer result=this.userService.saveOrUpdateNotificationConfiguration(notificationConfigurationDto, userId);
			if(result>0){
				return "success";
			}
			else{
				throw new NotificationConfigurationNotSavedOrUpdatedException();
			}
			
		}
		catch(NotificationConfigurationNotSavedOrUpdatedException e){
			String error="Notification Configuration Not Saved Or Updated Exception";
			map.put("message",error);
			return "fail";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while saving or updating Notification Configuration "+e.getMessage());
			map.put("message", "Error while saving or updating Notification Configuration ");
			return "fail";
		}
		
	}
	
	/**
	 * Created By Harshitha on June 4th, 2021
	 * @param request
	 * @param map
	 * @return
	 * 
	 * Ajax service for to get the notification configuration data
	 */
	@RequestMapping(value="/getnotificationconfiguration.do", method=RequestMethod.GET)
	@ResponseBody
	public String getNotificationConfiguration(@RequestParam("userId")Integer userId){
		log.info( " UserController -> getNotificationConfiguration()");
		Boolean scoreNotification = false; 
		Boolean messageNotification = false; 
		
		try{
			
			NotificationConfigurationDto notificationConfigurationDto=this.userService.getNotificationConfigurationByUserId(userId);
			if(null!=notificationConfigurationDto){
				scoreNotification=notificationConfigurationDto.getScoreNotification();
				messageNotification=notificationConfigurationDto.getMessageNotification();
			}
			
			// converting requested data into json object format 
			JSONObject responseObject=new JSONObject();
			responseObject.append("scoreNotification", scoreNotification);
			responseObject.append("messageNotification",messageNotification);
			
			// returning json object in the string format
			return responseObject.toString();
			
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while getting notification configuration "+e.getMessage());
			
			return "fail";
		}
		
	}
	/**
	 * Created By Bhagya on 11th june 2021
	 * @param map
	 * @param request
	 * @return
	 * 
	 * Service for to get the messages 
	 */
	
	@RequestMapping(value="/viewmessages.do")
	public String initMessageCenter(Map<String,Object> map,HttpServletRequest request){
		log.info( " UserController -> initMessageCenter()");
	
		try{
			HttpSession session=request.getSession(true);
			Integer userId=(Integer) session.getAttribute("userId");
			ArrayList<MessageBoardDto> messageBoardDtos=this.userService.getMessages(userId);
			
			map.put("messageBoardDtos",messageBoardDtos);
			map.put("userId",userId);
			return "common/messageCenter";
			
		}
		catch(MessagesNotFoundException e) {
			String error="No Messages availabe";
			map.put("message",error);
			return "common/messageCenter";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating Message Center "+e.getMessage());
			map.put("message", "Error while initiating Message Center");
			return "error";
		}
		
	}
	@ResponseBody
	@RequestMapping(value="/getmessages.do")
	public String messageCenter(Map<String,Object> map,HttpServletRequest request){
		log.info( " UserController -> initMessageCenter()");
	
		try{
			HttpSession session=request.getSession(true);
			Integer userId=(Integer) session.getAttribute("userId");
			ArrayList<MessageBoardDto> messageBoardDtos=this.userService.getMessages(userId);
			
			map.put("messageBoardDtos",messageBoardDtos);
			map.put("userId",userId);
			ArrayList<JSONObject> messageBoardJsons=new ArrayList<JSONObject>();
			for(MessageBoardDto messageBoardDto:messageBoardDtos) {
				JSONObject messageBoardJson=new JSONObject();
				messageBoardJson.accumulate("userId",  messageBoardDto.getUserDto().getUserId());
				if(messageBoardDto.getIsScoreMessage()) {
					messageBoardJson.accumulate("firstName", "Score");
					messageBoardJson.accumulate("lastName", "Notification");
				}
				else {
					messageBoardJson.accumulate("firstName", messageBoardDto.getUserDto().getFirstName());
					messageBoardJson.accumulate("lastName", messageBoardDto.getUserDto().getLastName());
				}
				SimpleDateFormat dateFormat=new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss");
				String formattedDate=dateFormat.format(messageBoardDto.getMessageDateTime());
				messageBoardJson.accumulate("messageDateTime",formattedDate);
				messageBoardJson.accumulate("message", messageBoardDto.getMessage());
				messageBoardJsons.add(messageBoardJson);
			}
			return messageBoardJsons.toString();
			
		}
		catch(MessagesNotFoundException e) {
			String error="No Messages availabe";
			map.put("message",error);
			return "common/messageCenter";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating Message Center "+e.getMessage());
			map.put("message", "Error while initiating Message Center");
			return "error";
		}
		
	}
	/**
	 * Created By BHagya on june 11th, 2021
	 * @param map
	 * @param userId
	 * @param message
	 * @return
	 * 
	 * Service for to save message.
	 */
	@RequestMapping(value="/savemessage.do", method=RequestMethod.GET)
	@ResponseBody
	public String saveMessage(Map<String,Object> map,@RequestParam("userId")Integer userId, @RequestParam("message")String message){
		log.info( " UserController -> saveMessage()");
		try{
			MessageBoardDto messageBoardDto=new MessageBoardDto();
			messageBoardDto.setMessage(message);
			Integer result=this.userService.saveMessages(messageBoardDto, userId,false);
			if(result>0){
				return "success";
			}
			else{
				throw new MessageBoardNotSavedOrUpdatedException();
			}
			
		}
		catch(MessageBoardNotSavedOrUpdatedException e){
			String error="Message Not Saved Or Updated Exception";
			map.put("message",error);
			return "fail";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while saving message "+e.getMessage());
			map.put("message", "Error while saving message ");
			return "fail";
		}
		
	}
	/**
	 * Created By Bhagya On June 22nd , 2021
	 * @param userId
	 * @return
	 * 
	 * Service for to get the user notification count 
	 */
	@RequestMapping(value="/getnotificationcount.do", method=RequestMethod.GET)
	@ResponseBody
	public String getNotificationCount(@RequestParam("userId")Integer userId){
		log.info( " UserController -> getNotificationCount()");
		Integer notificationCount=0;
		
		try{
			
			UserNotificationCountDto notificationCountDto=this.userService.getUserNotificationCountByUserId(userId);
			if(null!=notificationCountDto){
				notificationCount=notificationCountDto.getNotificationCount();
				
			}
			
			// converting requested data into json object format 
			JSONObject responseObject=new JSONObject();
			responseObject.append("notificationCount", notificationCount);
		
			// returning json object in the string format
			return responseObject.toString();
			
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while getting notification Count "+e.getMessage());
			
			return "fail";
		}
		
	}
}