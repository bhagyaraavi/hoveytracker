package com.hoveytracker.frontend.user.service;

import java.util.ArrayList;

import com.hoveytracker.backend.user.model.User;
import com.hoveytracker.backend.user.model.UserNotificationCount;
import com.hoveytracker.common.exceptions.CloserNotFoundException;
import com.hoveytracker.common.exceptions.FronterNotFoundException;
import com.hoveytracker.common.exceptions.MessageBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.MessagesNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.UserNotFoundException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotFoundException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotSavedOrUpdatedException;
import com.hoveytracker.frontend.user.dto.CloserDto;
import com.hoveytracker.frontend.user.dto.ClosersBoardDataDto;
import com.hoveytracker.frontend.user.dto.ClosersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.FronterDto;
import com.hoveytracker.frontend.user.dto.FrontersBoardDataDto;
import com.hoveytracker.frontend.user.dto.FrontersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.MessageBoardDto;
import com.hoveytracker.frontend.user.dto.NotificationConfigurationDto;
import com.hoveytracker.frontend.user.dto.UserDto;
import com.hoveytracker.frontend.user.dto.UserNotificationCountDto;

/**
 * Created on May 07th, 2021
 * @author Bhagya
 * 
 * Interface for User Service
 *
 */
public interface UserService{
	public UserDto getUserByUsernameOrEmailId(String emailId);
	public UserDto getUserByUserId(Integer userId) throws UserNotFoundException;
	public void processDealClose(Integer userId,String userType,Integer count);
	public ArrayList<FronterDto> getFrontersData(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws FronterNotFoundException;
	public FrontersBoardDataDto getFrontersBoardData();
	public FrontersGrandTotalDto getFrontersGrandTotalData();
	public void saveOrUpdateFrontersGrandTotal();
	
	/**
	 * Created on May 25th, 2021 by Harshitha
	 */	
	public ClosersBoardDataDto getClosersBoardData();
	public ClosersGrandTotalDto getClosersGrandTotalData();
	public ArrayList<CloserDto> getClosersData(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws CloserNotFoundException;
	public void saveOrUpdateCloserGrandTotal() ;
	
	/** Created on June 3rd, 2021 by Harshitha */
	public Integer saveOrUpdateNotificationConfiguration(NotificationConfigurationDto notificationConfigurationDto, Integer userId) throws NotificationConfigurationNotSavedOrUpdatedException;
	public NotificationConfigurationDto getNotificationConfigurationByUserId(Integer userId) throws NotificationConfigurationNotFoundException;
	
	/** Created on June 7th, 2021 by Harshitha */
	public Integer saveMessages(MessageBoardDto messageBoardDto, Integer userId,Boolean scoreMessage) throws MessageBoardNotSavedOrUpdatedException;
	public ArrayList<MessageBoardDto> getMessages(Integer userId) throws MessagesNotFoundException;
	
	/** Created on June 17th, 2021 by Harshitha */
	public Integer saveOrUpdateUserNotificationCount(UserNotificationCountDto notificationConfigurationDto, Integer userId) throws UserNotificationCountNotSavedOrUpdatedException;
	
	/** Created on June 18th, 2021 by Harshitha */
	public UserNotificationCountDto getUserNotificationCountByUserId(Integer userId); 
}