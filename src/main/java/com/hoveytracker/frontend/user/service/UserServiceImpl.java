package com.hoveytracker.frontend.user.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;


import javax.annotation.Resource;

import org.apache.log4j.Logger;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hoveytracker.backend.user.dao.UserDao;
import com.hoveytracker.backend.user.model.Closer;

import com.hoveytracker.backend.user.model.ClosersBoardData;
import com.hoveytracker.backend.user.model.ClosersGrandTotal;
import com.hoveytracker.backend.user.model.Fronter;
import com.hoveytracker.backend.user.model.FrontersBoardData;
import com.hoveytracker.backend.user.model.FrontersGrandTotal;
import com.hoveytracker.backend.user.model.MessageBoard;
import com.hoveytracker.backend.user.model.NotificationConfiguration;
import com.hoveytracker.backend.user.model.User;
import com.hoveytracker.backend.user.model.UserNotificationCount;
import com.hoveytracker.common.exceptions.CloserGrandTotalNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.CloserNotFoundException;
import com.hoveytracker.common.exceptions.CloserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.ClosersBoardDataNotFoundException;
import com.hoveytracker.common.exceptions.ClosersGrandTotalNotFoundException;
import com.hoveytracker.common.exceptions.FronterGrandTotalNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterNotFoundException;
import com.hoveytracker.common.exceptions.FronterNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FrontersBoardDataNotFoundException;
import com.hoveytracker.common.exceptions.FrontersGrandTotalNotFoundException;
import com.hoveytracker.common.exceptions.MessageBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.MessagesNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotFoundException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.NotificationCountNotUpdatedException;
import com.hoveytracker.common.exceptions.UserNotFoundException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotFoundException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotSavedOrUpdatedException;
import com.hoveytracker.frontend.user.dto.CloserDto;

import com.hoveytracker.frontend.user.dto.ClosersBoardDataDto;
import com.hoveytracker.frontend.user.dto.ClosersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.FronterDto;
import com.hoveytracker.frontend.user.dto.FrontersBoardDataDto;
import com.hoveytracker.frontend.user.dto.FrontersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.MessageBoardDto;
import com.hoveytracker.frontend.user.dto.NotificationConfigurationDto;
import com.hoveytracker.frontend.user.dto.UserDto;
import com.hoveytracker.frontend.user.dto.UserNotificationCountDto;


/**
 * Created on May 07th,2021
 * @author Bhagya
 *
 *Service implementation class for User
 */
@Transactional
@Service("userService")
public class UserServiceImpl implements UserService{
	
	private static Logger log = Logger.getLogger(UserServiceImpl.class);

	@Resource(name = "userDao")
	private UserDao userDao;
	
	/**
	 * Created by bhagya on may 07th, 2021
	 * Service for get the user based on username or email
	 */
	public UserDto getUserByUsernameOrEmailId(String emailId) {
		
		log.info("UserServiceImpl-> getUserByUsernameOrEmailId();");
		try {
			
			User user = this.userDao.getUserforAuthentication(emailId);
			if (user != null) {
				 UserDto userDto = UserDto.populateUserDto(user);
				return userDto;
			} else {
				return null;
			}
		} 
		catch(UserNotFoundException e){
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service " + e.toString());
			return null;
		}
	}
	/**
	 * Created By Bhagya on may 10th, 2021
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to get the user by userId
	 */
	public UserDto getUserByUserId(Integer userId) throws UserNotFoundException {
		log.info("UserServiceImpl -> getUserByUserId()");
			User user=this.userDao.getUserByUserId(userId);
			UserDto userDto=UserDto.populateUserDto(user);
			return userDto;
		
	}
	
	
	/**
	 * Created By bhagya on may 24th, 2021
	 * Service for to process the deal close
	 */
	public void processDealClose(Integer userId,String userType,Integer count) {
		log.info("UserServiceImpl -> processDealClose()");
		User user = null;
		try {
			
			user=this.userDao.getUserByUserId(userId);
			/**
			 * Created By harshitha on june 16th, 2021
			 *  to get the messages in message center once after the deal close
			 */
			MessageBoardDto messageBoardDto = new MessageBoardDto();
			if(count>1) {
				messageBoardDto.setMessage("Congratulations! " + user.getFirstName() + " " + user.getLastName() +" for closing "+count+" deals. Great Work!!");
			}
			else {
				messageBoardDto.setMessage("Congratulations! " + user.getFirstName() + " " + user.getLastName() +" for closing "+count+" deal. Great Work!!");
			}
			this.saveMessages(messageBoardDto, userId,true);
            //End of the implmentation by harshitha
			String dayName=LocalDate.now().getDayOfWeek().name();
			
			if(userType.equalsIgnoreCase("fronter")) {
				try { 	
					this.saveOrUpdateFronterData(user,dayName,count);
				} catch (FronterNotSavedOrUpdatedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				try {
					this.saveOrUpdateCloserData(user,dayName,count);
				} catch (CloserNotSavedOrUpdatedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		catch(UserNotFoundException e) {
			
		} catch (MessageBoardNotSavedOrUpdatedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	/**
	 * Craeted By Bhagya on may 21st, 2021
	 * @param user
	 * @param dayName
	 * @throws FronterNotSavedOrUpdatedException
	 * 
	 * Service for to save or update fronter board data
	 */
	public void saveOrUpdateFronterData(User user,String dayName,Integer count) throws FronterNotSavedOrUpdatedException {
		log.info("UserServiceImpl -> saveOrUpdateFronterData()");
		Fronter fronter=null;
		Integer totalCount=0;
		try {
			fronter=this.userDao.getFronterByUser(user);
			fronter.setUpdationDate(new Date());
		}
		catch(FronterNotFoundException e) {
			fronter=new Fronter();
			fronter.setUser(user);
			fronter.setCreationDate(new Date());
		}
		
		// monday
		if(dayName.equalsIgnoreCase("MONDAY")) {
			Integer monCount=0;
			try {
				monCount=fronter.getMon()+count;
			}
			catch(NullPointerException e) {
				monCount=count;
			}
			fronter.setMon(monCount);
		}
		// tuesday
		if(dayName.equalsIgnoreCase("TUESDAY")) {
			Integer tuesCount=0;
			try {
				tuesCount=fronter.getTues()+count;
			}
			catch(NullPointerException e) {
				tuesCount=count;
			}
			fronter.setTues(tuesCount);
		}
		// wednesday
		if(dayName.equalsIgnoreCase("WEDNESDAY")) {
			Integer wedCount=0;
			try {
				wedCount=fronter.getWed()+count;
			}
			catch(NullPointerException e) {
				wedCount=count;
			}
			fronter.setWed(wedCount);
		}
		// thursday
		if(dayName.equalsIgnoreCase("THURSDAY")) {
			Integer thursCount=0;
			try {
				thursCount=fronter.getThurs()+count;
			}
			catch(NullPointerException e) {
				thursCount=count;
			}
			fronter.setThurs(thursCount);
		}

		// Friday
		if(dayName.equalsIgnoreCase("FRIDAY")) {
			Integer friCount=0;
			try {
				friCount=fronter.getFri()+count;
			}
			catch(NullPointerException e) {
				friCount=count;
			}
			fronter.setFri(friCount);
		}
		// Total
		if(!dayName.equalsIgnoreCase("SATURDAY")&&!dayName.equalsIgnoreCase("SUNDAY")) {
			try {
				totalCount=fronter.getTotal()+count;
			}
			catch(NullPointerException e) {
				totalCount=count;
			}
			fronter.setTotal(totalCount);
			
			
		}
		
		Integer savedResult=this.userDao.saveOrUpdateFronter(fronter);
		if(savedResult>0) {
			this.saveOrUpdateFrontersGrandTotal();
		}
	}
	/**
	 * Created By Bhagya on may 21st, 2021
	 * @return
	 * @throws FronterNotFoundException
	 * 
	 * Service for to get list of fronters data
	 */
	public ArrayList<FronterDto> getFrontersData(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws FronterNotFoundException{
		log.info("UserServiceImpl -> getFrontersData() ");
		ArrayList<FronterDto> fronterDtos=new ArrayList<FronterDto>();
		ArrayList<Fronter> fronters=this.userDao.getFrontersData(pageNo,pageSize,searchBy,sortBy,ascending);
		for(Fronter fronter:fronters) {
			FronterDto fronterDto=FronterDto.populateFronterDto(fronter);
			fronterDtos.add(fronterDto);
		}
		fronterDtos.get(0).setTotalFronters(fronters.get(0).getTotalFronters());
		return fronterDtos;
		
	}
	
	/**
	 * Created By Bhagya on May 22nd, 2021
	 * @return
	 * 
	 * Service for to get fronters board data
	 */
	public FrontersBoardDataDto getFrontersBoardData() {
		log.info("UserServiceImpl -> getFrontersBoardData() ");
		
		FrontersBoardDataDto frontersBoardDataDto=null;
		
		try {
			FrontersBoardData frontersBoardData = this.userDao.getFrontersBoardData();
			frontersBoardDataDto=FrontersBoardDataDto.populateFrontersBoardDataDto(frontersBoardData);
		} catch (FrontersBoardDataNotFoundException e) {
			
		}
		return frontersBoardDataDto;
	}
	/**
	 * Created By Bhagya on may 22nd,2021
	 * @return
	 * 
	 * Service for to get fronters grand total data
	 */
	public FrontersGrandTotalDto getFrontersGrandTotalData() {
		log.info("UserServiceImpl -> getFrontersGrandTotalData() ");
		FrontersGrandTotalDto frontersGrandTotalDto=null;
		try {
			FrontersGrandTotal frontersGrandTotal = this.userDao.getFrontersGrandTotalData();
			frontersGrandTotalDto=FrontersGrandTotalDto.populateFrontersGrandTotalDto(frontersGrandTotal);
		} catch (FrontersGrandTotalNotFoundException e) {
			
		}
		return frontersGrandTotalDto;
	}
	/**
	 * Created By Bhagya on May 22nd, 2021
	 * Service for to save or update fronters grand total
	 */
	public void saveOrUpdateFrontersGrandTotal() {
		log.info("UserServiceImpl -> saveOrUpdateFrontersGrandTotal() ");
		// Grand Total
		FrontersGrandTotal frontersGrandTotal=null;
		try {
			frontersGrandTotal = this.userDao.getFrontersGrandTotalData();
			
		} catch (FrontersGrandTotalNotFoundException e1) {
			// TODO Auto-generated catch block
			frontersGrandTotal=new FrontersGrandTotal();
			
		}
		FrontersGrandTotalDto frontersGrandTotalDto=this.userDao.getFrontersGrandTotal();
		frontersGrandTotal.setMonTotal(frontersGrandTotalDto.getMonTotal());
		frontersGrandTotal.setTuesTotal(frontersGrandTotalDto.getTuesTotal());
		frontersGrandTotal.setWedTotal(frontersGrandTotalDto.getWedTotal());
		frontersGrandTotal.setThursTotal(frontersGrandTotalDto.getThursTotal());
		frontersGrandTotal.setFriTotal(frontersGrandTotalDto.getFriTotal());
		frontersGrandTotal.setGrandTotal(frontersGrandTotalDto.getGrandTotal());
		try {
			this.userDao.saveOrUpdateFronterGrandTotal(frontersGrandTotal);
		} catch (FronterGrandTotalNotSavedOrUpdatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	

	/**
	 * Craeted By Harshitha on may 24st, 2021
	 * @param user
	 * @param dayName
	 * @throws CloserNotSavedOrUpdatedException
	 * 
	 * Service for to save or update closer board data
	 */
	public void saveOrUpdateCloserData(User user,String dayName,Integer count) throws CloserNotSavedOrUpdatedException {
		log.info("UserServiceImpl -> saveOrUpdateCloserData()");
		Closer closer=null;
		Integer totalCount=0;
		try {
			closer=this.userDao.getCloserByUser(user);
			closer.setUpdationDate(new Date());
		}
		catch(CloserNotFoundException e) {
			closer=new Closer();
			closer.setUser(user);
			closer.setCreationDate(new Date());
		}
		
		// monday
		if(dayName.equalsIgnoreCase("MONDAY")) {
			Integer monCount=0;
			try {
				monCount=closer.getMon()+count;
			}
			catch(NullPointerException e) {
				monCount=count;
			}
			closer.setMon(monCount);
		}
		// tuesday
		if(dayName.equalsIgnoreCase("TUESDAY")) {
			Integer tuesCount=0;
			try {
				tuesCount=closer.getTues()+count;
			}
			catch(NullPointerException e) {
				tuesCount=count;
			}
			closer.setTues(tuesCount);
		}
		// wednesday
		if(dayName.equalsIgnoreCase("WEDNESDAY")) {
			Integer wedCount=0;
			try {
				wedCount=closer.getWed()+count;
			}
			catch(NullPointerException e) {
				wedCount=count;
			}
			closer.setWed(wedCount);
		}
		// thursday
		if(dayName.equalsIgnoreCase("THURSDAY")) {
			Integer thursCount=0;
			try {
				thursCount=closer.getThurs()+count;
			}
			catch(NullPointerException e) {
				thursCount=count;
			}
			closer.setThurs(thursCount);
		}

		// Friday
		if(dayName.equalsIgnoreCase("FRIDAY")) {
			Integer friCount=0;
			try {
				friCount=closer.getFri()+count;
			}
			catch(NullPointerException e) {
				friCount=count;
			}
			closer.setFri(friCount);
		}
		// Total
		if(!dayName.equalsIgnoreCase("SATURDAY")&&!dayName.equalsIgnoreCase("SUNDAY")) {
			try {
				totalCount=closer.getTotal()+count;
			}
			catch(NullPointerException e) {
				totalCount=count;
			}
			closer.setTotal(totalCount);
			
			
		}
		
		Integer savedResult=this.userDao.saveOrUpdateClosers(closer);
		if(savedResult>0) {
			this.saveOrUpdateCloserGrandTotal();
		}
	}
	
	/**
	 * Created By Harshitha on May 24th, 2021
	 * Service for to save or update closers grand total
	 */
	public void saveOrUpdateCloserGrandTotal() {
		log.info("UserServiceImpl -> saveOrUpdateCloserGrandTotal() ");
		// Grand Total
		ClosersGrandTotal closerGrandTotal=null;
		try {
			closerGrandTotal = this.userDao.getClosersGrandTotalData();
			
		} catch (ClosersGrandTotalNotFoundException e1) {
			// TODO Auto-generated catch block
			closerGrandTotal=new ClosersGrandTotal();
			
		}
		ClosersGrandTotalDto closersGrandTotalDto=this.userDao.getClosersGrandTotal();
		closerGrandTotal.setMonTotal(closersGrandTotalDto.getMonTotal());
		closerGrandTotal.setTuesTotal(closersGrandTotalDto.getTuesTotal());
		closerGrandTotal.setWedTotal(closersGrandTotalDto.getWedTotal());
		closerGrandTotal.setThursTotal(closersGrandTotalDto.getThursTotal());
		closerGrandTotal.setFriTotal(closersGrandTotalDto.getFriTotal());
		closerGrandTotal.setGrandTotal(closersGrandTotalDto.getGrandTotal());
		try {
			this.userDao.saveOrUpdateCloserGrandTotal(closerGrandTotal);
		} catch (CloserGrandTotalNotSavedOrUpdatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Created By Harshitha on May 25th, 2021
	 * @return
	 * 
	 * Service for to get closers board data
	 */
	public ClosersBoardDataDto getClosersBoardData() {
		log.info("UserServiceImpl -> getClosersBoardData() ");
		
		ClosersBoardDataDto closersBoardDataDto=null;
		
		try {
			ClosersBoardData closersBoardData = this.userDao.getClosersBoardData();
			closersBoardDataDto=ClosersBoardDataDto.populateClosersBoardDataDto(closersBoardData);
		} catch (ClosersBoardDataNotFoundException e) {
			
		}
		return closersBoardDataDto;
	}
	
	/**
	 * Created By Harshitha on may 25th,2021
	 * @return
	 * 
	 * Service for to get closers grand total data
	 */
	public ClosersGrandTotalDto getClosersGrandTotalData() {
		log.info("UserServiceImpl -> getClosersGrandTotalData() ");
		ClosersGrandTotalDto closerGrandTotalDto=null;
		try {
			ClosersGrandTotal closersGrandTotal = this.userDao.getClosersGrandTotalData();
			closerGrandTotalDto=ClosersGrandTotalDto.populateCloserGrandTotalDto(closersGrandTotal);
		} catch (ClosersGrandTotalNotFoundException e) {
			
		}
		return closerGrandTotalDto;
	}
	
	/**
	 * Created By Harshitha on may 25th, 2021
	 * @return
	 * @throws CloserNotFoundException
	 * 
	 * Service for to get list of closers data
	 */
	public ArrayList<CloserDto> getClosersData(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws CloserNotFoundException{
		log.info("UserServiceImpl -> getClosersData() ");
		ArrayList<CloserDto> closerDtos=new ArrayList<CloserDto>();
		ArrayList<Closer> closers=this.userDao.getClosersData(pageNo,pageSize,searchBy,sortBy,ascending);
		for(Closer closer:closers) {
			CloserDto closerDto=CloserDto.populateCloserDto(closer);
			closerDtos.add(closerDto);
		}
		closerDtos.get(0).setTotalClosers(closers.get(0).getTotalClosers());
		return closerDtos;
	}
	
	/**
	 * Created By Harshitha on June 3rd, 2021
	 * @param notificationConfigurationDto
	 * @return
	 * 
	 * Service for to save or update notification configuration
	 * @throws NotificationConfigurationNotSavedOrUpdatedException 
	 */
	
	public Integer saveOrUpdateNotificationConfiguration(NotificationConfigurationDto notificationConfigurationDto, Integer userId) throws NotificationConfigurationNotSavedOrUpdatedException
	{
		log.info("UserServiceImpl -> saveOrUpdateNotificationConfiguration()");
		NotificationConfiguration notificationConfiguration=null;
		User user=null;
		
		try {
			user = this.userDao.getUserByUserId(userId);
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			notificationConfiguration=this.userDao.getNotificationConfigurationByUser(user);
			notificationConfiguration.setUpdationDate(new Date());
		}
		catch(NotificationConfigurationNotFoundException e){
			notificationConfiguration=new NotificationConfiguration();
			notificationConfiguration.setCreationDate(new Date());
		}
		notificationConfiguration.setUser(user);
		notificationConfiguration.setScoreNotification(notificationConfigurationDto.getScoreNotification());
		notificationConfiguration.setMessageNotification(notificationConfigurationDto.getMessageNotification());
		
		Integer savedResult=this.userDao.saveOrUpdateNotificationConfiguration(notificationConfiguration);
		return savedResult;
	}
	
	/**
	 * created By Harshitha on june 3rd 2021
	 * @param userId
	 * @return
	 * 
	 * Service for to get the Notification Configuration by userId
	 */
	public NotificationConfigurationDto getNotificationConfigurationByUserId(Integer userId) throws NotificationConfigurationNotFoundException
	{	
		log.info("UserServiceImpl -> getNotificationConfigurationByUserId ");
		NotificationConfigurationDto notificationConfigurationDto=null;
		try{
			
			User user=this.userDao.getUserByUserId(userId);
			NotificationConfiguration notificationConfiguration=this.userDao.getNotificationConfigurationByUser(user);
			notificationConfigurationDto=NotificationConfigurationDto.populateNotificationConfigurationDto(notificationConfiguration);
		}
		catch(UserNotFoundException e){
			//e.printStackTrace();
			log.info("User Not Found");
		}
		return notificationConfigurationDto;
	}
	
	/**
	 * Created By Harshitha on June 7th, 2021
	 * @param messageBoardDto
	 * @return
	 * 
	 * Service for to save Messages
	 * @throws MessageBoardNotSavedOrUpdatedException 
	 */
	
	public Integer saveMessages(MessageBoardDto messageBoardDto, Integer userId,Boolean scoreMessage) throws MessageBoardNotSavedOrUpdatedException
	{
		log.info("UserServiceImpl -> saveOrUpdateMessages()");
		Integer savedResult = 0;
	try{
		MessageBoard messageBoard=new MessageBoard();
		User user=this.userDao.getUserByUserId(userId);

		messageBoard.setUser(user);
		messageBoard.setMessageDateTime(new Date());
		messageBoard.setMessage(messageBoardDto.getMessage());
		if(scoreMessage==true) {
			messageBoard.setIsScoreMessage(true);
		}
		else {
			messageBoard.setIsScoreMessage(false);
		}
		
		savedResult=this.userDao.saveOrUpdateMessage(messageBoard);
		}
		catch(UserNotFoundException e){
			//e.printStackTrace();
			log.info("User Not Found");
	      }
	/**
	 * Created By harshitha on june 17th, 2021
	 *  to update notification count for users except loggedIn userId
	 */
	try {
		this.userDao.updateNotificationCount(userId,scoreMessage);
	} catch (NotificationCountNotUpdatedException e) {
		
	}
	
	return savedResult;
	}
	
	/**
	 * Created By Harshitha on June 7th, 2021
	 * @return
	 * 
	 * Service for to get messages
	 * @throws MessagesNotFoundException 
	 */
	public ArrayList<MessageBoardDto> getMessages(Integer userId) throws MessagesNotFoundException {
		log.info("UserServiceImpl -> getMessages() ");
		ArrayList<MessageBoardDto> messageBoardDtos=new ArrayList<MessageBoardDto>();
			ArrayList<MessageBoard> messageBoards = this.userDao.getMessages();
			for(MessageBoard messageBoard:messageBoards) {
				MessageBoardDto messageBoardDto=MessageBoardDto.populateMessageBoardDto(messageBoard);
				messageBoardDtos.add(messageBoardDto);
			}
		// Update the UsernotificationCount to 0 based on UserId
			 /**
			 * Created By harshitha on june 17th, 2021
			 *  to get the user notification count
			 */
		  UserNotificationCountDto userNotificationCountDto = new UserNotificationCountDto();
		  userNotificationCountDto.setNotificationCount(0);
		  
		  try {
			this.saveOrUpdateUserNotificationCount(userNotificationCountDto, userId);
		} catch (UserNotificationCountNotSavedOrUpdatedException e) {
			
		}
		return messageBoardDtos;
	}
	
	/**
	 * Created By Harshitha on June 17th, 2021
	 * @param notificationConfigurationDto
	 * @return
	 * 
	 * Service for to save or update User notification count
	 * @throws UserNotificationCountNotSavedOrUpdatedException 
	 */
	
	public Integer saveOrUpdateUserNotificationCount(UserNotificationCountDto userNotificationCountDto, Integer userId) throws UserNotificationCountNotSavedOrUpdatedException{
	{
		log.info("UserServiceImpl -> saveOrUpdateUserNotificationCount()");
		UserNotificationCount userNotificationCount=null;
		
		try{
			userNotificationCount=this.userDao.getUserNotificationCountByUserId(userId);
		}
		
		catch(UserNotificationCountNotFoundException e){
			userNotificationCount = new UserNotificationCount();
		}
		
		userNotificationCount.setUserId(userId);
		userNotificationCount.setNotificationCount(userNotificationCountDto.getNotificationCount());
		
		Integer savedResult=this.userDao.saveOrUpdateNotificationCount(userNotificationCount);
		return savedResult;
	}

 }
	
	/**
	 * created By Harshitha on june 18th 2021
	 * @param user
	 * @return
	 * UserNotificationCountNotFoundException
	 * Service for to get the User Notification Count by userId
	 */
	public UserNotificationCountDto getUserNotificationCountByUserId(Integer userId){
	{	
		log.info("UserServiceImpl -> getUserNotificationCountByUserId ");
		UserNotificationCountDto userNotificationCountDto=null;
		try{
			UserNotificationCount userNotificationCount=this.userDao.getUserNotificationCountByUserId(userId);
			userNotificationCountDto=UserNotificationCountDto.populateUserNotificationCountDto(userNotificationCount);
		}
		catch(UserNotificationCountNotFoundException e){
			
		}
		return userNotificationCountDto;
	}

 }
}
