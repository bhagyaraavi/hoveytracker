package com.hoveytracker.frontend.user.dto;

import java.util.Date;

import com.hoveytracker.backend.user.model.Fronter;
import com.hoveytracker.backend.user.model.MessageBoard;
import com.hoveytracker.backend.user.model.NotificationConfiguration;
import com.hoveytracker.backend.user.model.User;

/**
 * 
 * @author harshitha
 * Created on June 7th,2021
 * Dto class for Message  Board
 *
 */
public class MessageBoardDto{
	
	private Integer messageId;
	private UserDto userDto;
	private Date messageDateTime;
	private String message;
	private Boolean isScoreMessage;
	
	public Integer getMessageId() {
		return messageId;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public Date getMessageDateTime() {
		return messageDateTime;
	}
	public void setMessageDateTime(Date messageDateTime) {
		this.messageDateTime = messageDateTime;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getIsScoreMessage() {
		return isScoreMessage;
	}
	public void setIsScoreMessage(Boolean isScoreMessage) {
		this.isScoreMessage = isScoreMessage;
	}
	public static MessageBoardDto populateMessageBoardDto(MessageBoard messageBoard){
		{
			MessageBoardDto messageBoardDto = new MessageBoardDto();
			messageBoardDto.setMessageId(messageBoard.getMessageId());
			messageBoardDto.setUserDto(UserDto.populateUserDto(messageBoard.getUser()));
			messageBoardDto.setMessageDateTime(messageBoard.getMessageDateTime());
			messageBoardDto.setMessage(messageBoard.getMessage());
			messageBoardDto.setIsScoreMessage(messageBoard.getIsScoreMessage());
			
			return messageBoardDto;
		}
	}
}

