package com.hoveytracker.frontend.user.dto;

import java.util.Date;
import com.hoveytracker.backend.user.model.ClosersGrandTotal;


/**
 * 
 * @author harshitha
 * Created on may 17th,2021
 * Dto class for Closer Grand Total
 *
 */
public class ClosersGrandTotalDto{
	
	private Integer closerGrandTotalId;
	private Integer monTotal;
	private Integer tuesTotal;
	private Integer wedTotal;
	private Integer thursTotal;
	private Integer friTotal;
	private Date creationDate;
	private Date updationDate;
	private Integer grandTotal;
	
	
	public Integer getCloserGrandTotalId() {
		return closerGrandTotalId;
	}
	public void setCloserGrandTotalId(Integer closerGrandTotalId) {
		this.closerGrandTotalId = closerGrandTotalId;
	}
	public Integer getMonTotal() {
		return monTotal;
	}
	public void setMonTotal(Integer monTotal) {
		this.monTotal = monTotal;
	}
	public Integer getTuesTotal() {
		return tuesTotal;
	}
	public void setTuesTotal(Integer tuesTotal) {
		this.tuesTotal = tuesTotal;
	}
	public Integer getWedTotal() {
		return wedTotal;
	}
	public void setWedTotal(Integer wedTotal) {
		this.wedTotal = wedTotal;
	}
	public Integer getThursTotal() {
		return thursTotal;
	}
	public void setThursTotal(Integer thursTotal) {
		this.thursTotal = thursTotal;
	}
	public Integer getFriTotal() {
		return friTotal;
	}
	public void setFriTotal(Integer friTotal) {
		this.friTotal = friTotal;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	public Integer getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
	public static ClosersGrandTotalDto populateCloserGrandTotalDto(ClosersGrandTotal closersGrandTotal){
		ClosersGrandTotalDto closersGrandTotalDto	= new ClosersGrandTotalDto();
		closersGrandTotalDto.setCloserGrandTotalId(closersGrandTotal.getCloserGrandTotalId());
		closersGrandTotalDto.setMonTotal(closersGrandTotal.getMonTotal());
		closersGrandTotalDto.setTuesTotal(closersGrandTotal.getTuesTotal());
		closersGrandTotalDto.setWedTotal(closersGrandTotal.getWedTotal());
		closersGrandTotalDto.setThursTotal(closersGrandTotal.getThursTotal());
		closersGrandTotalDto.setFriTotal(closersGrandTotal.getFriTotal());
		closersGrandTotalDto.setCreationDate(closersGrandTotal.getCreationDate());
		closersGrandTotalDto.setUpdationDate(closersGrandTotal.getUpdationDate());
		if(null!=closersGrandTotalDto.getUpdationDate())
		{
			closersGrandTotalDto.setUpdationDate(closersGrandTotal.getUpdationDate());
		}
		closersGrandTotalDto.setGrandTotal(closersGrandTotal.getGrandTotal());
		
		return closersGrandTotalDto;
	}
	
}
