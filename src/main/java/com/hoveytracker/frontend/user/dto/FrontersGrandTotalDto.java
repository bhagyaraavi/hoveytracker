package com.hoveytracker.frontend.user.dto;

import java.util.Date;

import com.hoveytracker.backend.user.model.FrontersGrandTotal;

/**
 * 
 * @author harshitha
 * Created on may 17th,2021
 * Dto class for Fronter Grand Total
 *
 */
public class FrontersGrandTotalDto{
	
	private Integer fronterGrandTotalId;
	private Integer monTotal;
	private Integer tuesTotal;
	private Integer wedTotal;
	private Integer thursTotal;
	private Integer friTotal;
	private Integer grandTotal;
	
	
	public Integer getFronterGrandTotalId() {
		return fronterGrandTotalId;
	}
	public void setFronterGrandTotalId(Integer fronterGrandTotalId) {
		this.fronterGrandTotalId = fronterGrandTotalId;
	}

	public Integer getMonTotal() {
		return monTotal;
	}
	public void setMonTotal(Integer monTotal) {
		this.monTotal = monTotal;
	}
	public Integer getTuesTotal() {
		return tuesTotal;
	}
	public void setTuesTotal(Integer tuesTotal) {
		this.tuesTotal = tuesTotal;
	}
	public Integer getWedTotal() {
		return wedTotal;
	}
	public void setWedTotal(Integer wedTotal) {
		this.wedTotal = wedTotal;
	}
	public Integer getThursTotal() {
		return thursTotal;
	}
	public void setThursTotal(Integer thursTotal) {
		this.thursTotal = thursTotal;
	}
	public Integer getFriTotal() {
		return friTotal;
	}
	public void setFriTotal(Integer friTotal) {
		this.friTotal = friTotal;
	}
	
	public Integer getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
	public static FrontersGrandTotalDto populateFrontersGrandTotalDto(FrontersGrandTotal frontersGrandTotal){
		FrontersGrandTotalDto frontersGrandTotalDto	= new FrontersGrandTotalDto();
		frontersGrandTotalDto.setFronterGrandTotalId(frontersGrandTotal.getFronterGrandTotalId());
		frontersGrandTotalDto.setMonTotal(frontersGrandTotal.getMonTotal());
		frontersGrandTotalDto.setTuesTotal(frontersGrandTotal.getTuesTotal());
		frontersGrandTotalDto.setWedTotal(frontersGrandTotal.getWedTotal());
		frontersGrandTotalDto.setThursTotal(frontersGrandTotal.getThursTotal());
		frontersGrandTotalDto.setFriTotal(frontersGrandTotal.getFriTotal());
		frontersGrandTotalDto.setGrandTotal(frontersGrandTotal.getGrandTotal());
		
		
		return frontersGrandTotalDto;
	}
	
}
