package com.hoveytracker.frontend.user.dto;

import java.util.Date;

import com.hoveytracker.backend.user.model.ClosersBoardData;

/**
 * 
 * @author harshitha
 * Created on may 17th,2021
 * Dto class for closers Board Data
 *
 */
public class ClosersBoardDataDto{
	
	private Integer closerBoardDataId;
	private Integer lastWeek;
	private Integer weekNum;
	private Integer weekGoal;
	private Date creationDate;
	private Date updationDate;

	
	public Integer getCloserBoardDataId() {
		return closerBoardDataId;
	}
	public void setCloserBoardDataId(Integer closerBoardDataId) {
		this.closerBoardDataId = closerBoardDataId;
	}

	public Integer getLastWeek() {
		return lastWeek;
	}

	public void setLastWeek(Integer lastWeek) {
		this.lastWeek = lastWeek;
	}
	
	public Integer getWeekNum() {
		return weekNum;
	}

	public void setWeekNum(Integer weekNum) {
		this.weekNum = weekNum;
	}

	public Integer getWeekGoal() {
		return weekGoal;
	}
	public void setWeekGoal(Integer weekGoal) {
		this.weekGoal = weekGoal;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	
	
	public static ClosersBoardDataDto populateClosersBoardDataDto(ClosersBoardData closersBoardData){
		ClosersBoardDataDto closersBoardDataDto	= new ClosersBoardDataDto();
		closersBoardDataDto.setCloserBoardDataId(closersBoardData.getCloserBoardDataId());
		closersBoardDataDto.setLastWeek(closersBoardData.getLastWeek());
		closersBoardDataDto.setWeekNum(closersBoardData.getWeekNum());
		closersBoardDataDto.setWeekGoal(closersBoardData.getWeekGoal());
		closersBoardDataDto.setCreationDate(closersBoardData.getCreationDate());
		closersBoardDataDto.setUpdationDate(closersBoardData.getUpdationDate());
		if(null!=closersBoardData.getUpdationDate())
		{
			closersBoardDataDto.setUpdationDate(closersBoardData.getUpdationDate());
		}
		
		return closersBoardDataDto;
	}
	
}
	