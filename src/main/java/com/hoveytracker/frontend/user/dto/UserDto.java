package com.hoveytracker.frontend.user.dto;

import java.sql.Timestamp;

import java.util.Date;

import com.hoveytracker.backend.user.model.User;

/**
 * 
 * @author Bhagya
 * Created on may 07th,2021
 * Dto class for User
 *
 */
public class UserDto{
	
	private Integer userId;
	private String username;
	private String password;
	private String firstName;
	private String LastName;
	private String emailId;
	private String city;
	private String state;
	private String zipcode;
	private String userType;
	private String status;
	private Date statusUpdatedDate;
	private Date accountCreationDate;
	private Date accountUpdationDate;
	private String passwordToken;
	private Date passwordTokenExpiryDate;
	private String accountActivationToken;
	private Date activationTokenExpiryDate;
	private Boolean isActive;
	private Integer totalUsers;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getStatusUpdatedDate() {
		return statusUpdatedDate;
	}
	public void setStatusUpdatedDate(Date statusUpdatedDate) {
		this.statusUpdatedDate = statusUpdatedDate;
	}
	public Date getAccountCreationDate() {
		return accountCreationDate;
	}
	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}
	public Date getAccountUpdationDate() {
		return accountUpdationDate;
	}
	public void setAccountUpdationDate(Date accountUpdationDate) {
		this.accountUpdationDate = accountUpdationDate;
	}
	public void setAccountUpdationDate(Timestamp accountUpdationDate) {
		this.accountUpdationDate = accountUpdationDate;
	}
	public String getPasswordToken() {
		return passwordToken;
	}
	public void setPasswordToken(String passwordToken) {
		this.passwordToken = passwordToken;
	}
	public Date getPasswordTokenExpiryDate() {
		return passwordTokenExpiryDate;
	}
	public void setPasswordTokenExpiryDate(Date passwordTokenExpiryDate) {
		this.passwordTokenExpiryDate = passwordTokenExpiryDate;
	}
	public String getAccountActivationToken() {
		return accountActivationToken;
	}
	public void setAccountActivationToken(String accountActivationToken) {
		this.accountActivationToken = accountActivationToken;
	}
	public Date getActivationTokenExpiryDate() {
		return activationTokenExpiryDate;
	}
	public void setActivationTokenExpiryDate(Date activationTokenExpiryDate) {
		this.activationTokenExpiryDate = activationTokenExpiryDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Integer getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}
	
	
	public static UserDto populateUserDto(User user){
		UserDto userDto=new UserDto();
		userDto.setUserId(user.getUserId());
		userDto.setUsername(user.getUsername());
		userDto.setPassword(user.getPassword());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setCity(user.getCity());
		userDto.setState(user.getState());
		userDto.setZipcode(user.getZipcode());
		userDto.setEmailId(user.getEmailId());
		userDto.setUserType(user.getUserType());
		userDto.setStatus(user.getStatus());
		userDto.setStatusUpdatedDate(user.getStatusUpdatedDate());
		userDto.setAccountCreationDate(user.getAccountCreationDate());
		userDto.setPasswordToken(user.getPasswordToken());
		if(null!=user.getPasswordTokenExpiryDate()){
			userDto.setPasswordTokenExpiryDate(user.getPasswordTokenExpiryDate());
		}
		userDto.setIsActive(user.getIsActive());
		userDto.setAccountActivationToken(user.getAccountActivationToken());
		if(null!=user.getActivationTokenExpiryDate()) {
			userDto.setActivationTokenExpiryDate(user.getActivationTokenExpiryDate());
		}
		
		if(null!=user.getAccountUpdationDate())
		{
			userDto.setAccountUpdationDate(user.getAccountUpdationDate());
		}
		
		return userDto;
		
	}
	
}