package com.hoveytracker.frontend.user.dto;


import java.util.Date;

import com.hoveytracker.backend.user.model.Closer;

/**
 * 
 * @author harshitha
 * Created on may 17th,2021
 * Dto class for Closer 
 *
 */
public class CloserDto{

	private UserDto userDto;
	private Integer closerId;
	private Integer mon;
	private Integer tues;
	private Integer wed;
	private Integer thurs;
	private Integer fri;
	private Date creationDate;
	private Date updationDate;
	private Integer total;
	private Integer totalClosers;
	
	
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}

	public Integer getCloserId() {
		return closerId;
	}
	public void setCloserId(Integer closerId) {
		this.closerId = closerId;
	}

	public Integer getMon() {
		return mon;
	}
	public void setMon(Integer mon) {
		this.mon = mon;
	}

	public Integer getTues() {
		return tues;
	}
	public void setTues(Integer tues) {
		this.tues = tues;
	}

	public Integer getWed() {
		return wed;
	}
	public void setWed(Integer wed) {
		this.wed = wed;
	}

	public Integer getThurs() {
		return thurs;
	}
	public void setThurs(Integer thurs) {
		this.thurs = thurs;
	}

	public Integer getFri() {
		return fri;
	}
	public void setFri(Integer fri) {
		this.fri = fri;
	}

	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	public Integer getTotalClosers() {
		return totalClosers;
	}
	public void setTotalClosers(Integer totalClosers) {
		this.totalClosers = totalClosers;
	}
	public static CloserDto populateCloserDto(Closer closer){
		CloserDto closersDto	= new CloserDto();
		closersDto.setUserDto(UserDto.populateUserDto(closer.getUser()));
		closersDto.setCloserId(closer.getCloserId());
		closersDto.setMon(closer.getMon());
		closersDto.setTues(closer.getTues());
		closersDto.setWed(closer.getWed());
		closersDto.setThurs(closer.getThurs());
		closersDto.setFri(closer.getFri());
		closersDto.setCreationDate(closer.getCreationDate());
		closersDto.setUpdationDate(closer.getUpdationDate());
		if(null!=closer.getUpdationDate())
		{
			closersDto.setUpdationDate(closer.getUpdationDate());
		}
		closersDto.setTotal(closer.getTotal());
		
		return closersDto;
	}
	
	
}