package com.hoveytracker.frontend.user.dto;

import java.util.Date;

import com.hoveytracker.backend.user.model.FrontersBoardData;

/**
 * 
 * @author harshitha
 * Created on may 17th,2021
 * Dto class for Fronter Board Data
 *
 */
public class FrontersBoardDataDto{
	
	private Integer fronterBoardDataId;
	private Integer lastWeek;
	private Date creationDate;
	private Date updationDate;
	private UserDto userDto;
	private Integer weekNum;
	private Integer weekGoal;

	public Integer getFronterBoardDataId() {
		return fronterBoardDataId;
	}
	public void setFronterBoardDataId(Integer fronterBoardDataId) {
		this.fronterBoardDataId = fronterBoardDataId;
	}
	public Integer getLastWeek() {
		return lastWeek;
	}
	public void setLastWeek(Integer lastWeek) {
		this.lastWeek = lastWeek;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	
	public Integer getWeekNum() {
		return weekNum;
	}
	public void setWeekNum(Integer weekNum) {
		this.weekNum = weekNum;
	}
	public Integer getWeekGoal() {
		return weekGoal;
	}
	public void setWeekGoal(Integer weekGoal) {
		this.weekGoal = weekGoal;
	}
	public static FrontersBoardDataDto populateFrontersBoardDataDto(FrontersBoardData frontersBoardData){
		FrontersBoardDataDto frontersBoardDataDto	= new FrontersBoardDataDto();
		frontersBoardDataDto.setFronterBoardDataId(frontersBoardData.getFronterBoardDataId());
		frontersBoardDataDto.setLastWeek(frontersBoardData.getLastWeek());
		frontersBoardDataDto.setCreationDate(frontersBoardData.getCreationDate());
		frontersBoardDataDto.setUserDto(UserDto.populateUserDto(frontersBoardData.getUser()));
		if(null!=frontersBoardData.getUpdationDate())
		{
			frontersBoardDataDto.setUpdationDate(frontersBoardData.getUpdationDate());
		}
		frontersBoardDataDto.setWeekNum(frontersBoardData.getWeekNum());
		frontersBoardDataDto.setWeekGoal(frontersBoardData.getWeekGoal());
		return frontersBoardDataDto;
	}
	
}
	