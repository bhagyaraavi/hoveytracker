package com.hoveytracker.frontend.user.dto;

import java.util.Date;

import com.hoveytracker.backend.user.model.Fronter;

/**
 * 
 * @author harshitha
 * Created on may 17th,2021
 * Dto class for Fronter
 *
 */
public class FronterDto{

	private UserDto userDto;
	private Integer fronterId;
	private Integer mon;
	private Integer tues;
	private Integer wed;
	private Integer thurs;
	private Integer fri;
	private Date creationDate;
	private Date updationDate;
	private Integer total;
	private Integer totalFronters;
	
	
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public Integer getFronterId() {
		return fronterId;
	}
	public void setFronterId(Integer fronterId) {
		this.fronterId = fronterId;
	}
	public Integer getMon() {
		return mon;
	}
	public void setMon(Integer mon) {
		this.mon = mon;
	}


	public Integer getTues() {
		return tues;
	}
	public void setTues(Integer tues) {
		this.tues = tues;
	}
	public Integer getWed() {
		return wed;
	}
	public void setWed(Integer wed) {
		this.wed = wed;
	}
	public Integer getThurs() {
		return thurs;
	}
	public void setThurs(Integer thurs) {
		this.thurs = thurs;
	}
	public Integer getFri() {
		return fri;
	}
	public void setFri(Integer fri) {
		this.fri = fri;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	public Integer getTotalFronters() {
		return totalFronters;
	}
	public void setTotalFronters(Integer totalFronters) {
		this.totalFronters = totalFronters;
	}
	public static FronterDto populateFronterDto(Fronter fronter){
		FronterDto fronterDto	= new FronterDto();
		fronterDto.setUserDto(UserDto.populateUserDto(fronter.getUser()));
		fronterDto.setFronterId(fronter.getFronterId());
		fronterDto.setMon(fronter.getMon());
		fronterDto.setTues(fronter.getTues());
		fronterDto.setWed(fronter.getWed());
		fronterDto.setThurs(fronter.getThurs());
		fronterDto.setFri(fronter.getFri());
		fronterDto.setCreationDate(fronter.getCreationDate());
		if(null!=fronter.getUpdationDate())
		{
			fronterDto.setUpdationDate(fronter.getUpdationDate());
		}
		fronterDto.setTotal(fronter.getTotal());
		
		return fronterDto;
	}
	
	
}