package com.hoveytracker.frontend.user.dto;
import com.hoveytracker.backend.user.model.UserNotificationCount;

/**
 * 
 * @author harshitha
 * Created on June 16th,2021
 * Dto class for User Notification Count 
 *
 */
public class UserNotificationCountDto{
	
	private Integer countId;
	private Integer userId;
	private Integer notificationCount;
	
	public Integer getCountId() {
		return countId;
	}


	public void setCountId(Integer countId) {
		this.countId = countId;
	}

	
	

	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public Integer getNotificationCount() {
		return notificationCount;
	}


	public void setNotificationCount(Integer notificationCount) {
		this.notificationCount = notificationCount;
	}
	
	 public static UserNotificationCountDto populateUserNotificationCountDto(UserNotificationCount userNotificationCount){
		 UserNotificationCountDto userNotificationCountDto = new UserNotificationCountDto();
		 userNotificationCountDto.setCountId(userNotificationCount.getCountId());	
		 userNotificationCountDto.setUserId(userNotificationCount.getUserId());
		 userNotificationCountDto.setNotificationCount(userNotificationCount.getNotificationCount());
	 
		 return userNotificationCountDto;
	 }
}


	


	
	
	