package com.hoveytracker.frontend.user.dto;

import java.util.Date;

import com.hoveytracker.backend.user.model.Fronter;
import com.hoveytracker.backend.user.model.NotificationConfiguration;
import com.hoveytracker.backend.user.model.User;

/**
 * 
 * @author harshitha
 * Created on June 3rd,2021
 * Dto class for Notification Configuration 
 *
 */
public class NotificationConfigurationDto{
	
	private Integer configId;
	private UserDto userDto;
	private Boolean scoreNotification;
	private Boolean messageNotification;
	private Date creationDate;
	private Date updationDate;
	
	public Integer getConfigId() {
		return configId;
	}
	public void setConfigId(Integer configId) {
		this.configId = configId;
	}
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public Boolean getScoreNotification() {
		return scoreNotification;
	}
	public void setScoreNotification(Boolean scoreNotification) {
		this.scoreNotification = scoreNotification;
	}
	public Boolean getMessageNotification() {
		return messageNotification;
	}
	public void setMessageNotification(Boolean messageNotification) {
		this.messageNotification = messageNotification;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	
	  public static NotificationConfigurationDto populateNotificationConfigurationDto(NotificationConfiguration notificationConfiguration){
	  NotificationConfigurationDto notificationConfigurationDto = new NotificationConfigurationDto();
	  notificationConfigurationDto.setConfigId(notificationConfiguration.getConfigId());
	  notificationConfigurationDto.setUserDto(UserDto.populateUserDto(notificationConfiguration.getUser()));
	  notificationConfigurationDto.setScoreNotification(notificationConfiguration.getScoreNotification());
	  notificationConfigurationDto.setMessageNotification(notificationConfiguration.getMessageNotification());
	  notificationConfigurationDto.setCreationDate(notificationConfiguration.getCreationDate());
	  if(null!=notificationConfiguration.getUpdationDate()) {
	  notificationConfigurationDto.setUpdationDate(notificationConfiguration.getUpdationDate()); }
	  
	  return notificationConfigurationDto;
	  }
	 
}