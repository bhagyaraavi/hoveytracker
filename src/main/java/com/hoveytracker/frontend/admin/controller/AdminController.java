package com.hoveytracker.frontend.admin.controller;


import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoveytracker.common.exceptions.CloserBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.CloserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotFoundException;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.UserNotFoundException;
import com.hoveytracker.common.exceptions.UserNotSavedOrUpdatedException;
import com.hoveytracker.frontend.admin.dto.DailyScoreBoardDto;
import com.hoveytracker.frontend.admin.service.AdminService;
import com.hoveytracker.frontend.user.dto.ClosersBoardDataDto;
import com.hoveytracker.frontend.user.dto.FrontersBoardDataDto;
import com.hoveytracker.frontend.user.dto.UserDto;
import com.hoveytracker.frontend.user.service.UserService;
import com.hoveytracker.frontend.utility.dto.DisplayListBeanDto;


@Controller("adminController")
@RequestMapping("/admin")
public class AdminController {

	private static Logger log = Logger.getLogger(AdminController.class);

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "adminService")
	private AdminService adminService;

	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @param map
	 * @param
	 * @return
	 * 
	 *         Service for to initiate the Admin dashboard
	 */
	@RequestMapping(value = "/dashboard.do", method = RequestMethod.GET)
	public String initAdminUserDashboard(Map<String, Object> map) {
		log.info(" AdminController -> initAdminUserDashboard()");

		try {

			return "common/dashboard";

		}

		catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while initiating admin Dashboard " + e.getMessage());
			map.put("message", "Error while initiating admin Dashboard ");
			return "error";
		}

	}

	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @param map
	 * @param userDto
	 * @return
	 * 
	 *         Service for to initiate the user registration
	 */
	@RequestMapping(value = "/userregister.do", method = RequestMethod.GET)
	public String initUserRegistration(Map<String, Object> map, @ModelAttribute("userDto") UserDto userDto) {
		log.info(" AdminController -> initUserRegistration()");

		try {
			ArrayList<String> states = this.adminService.getListofStates();
			map.put("states", states);
			return "admin/registerUser";

		}

		catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while initiating registration  " + e.getMessage());
			map.put("message", "Error while initiating registration ");
			return "error";
		}

	}

	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @param map
	 * @param request
	 * @param response
	 * @param userDto
	 * @param redAttribs
	 * @return
	 * 
	 *         Service for to save or update user registration details
	 */

	@RequestMapping(value = "/userregister.do", method = RequestMethod.POST)
	public String userRegistration(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("userDto") UserDto userDto, RedirectAttributes redAttribs) {
		log.info(" AdminController -> userRegistration()");
		try {
			Integer savedResult = this.adminService.saveOrUpdateUser(userDto);

			if (savedResult > 0) {
				return "redirect:/admin/registersuccess.do?email=" + userDto.getEmailId();

			} else {
				throw new Exception();
			}

		} catch (UserNotSavedOrUpdatedException e) {
			map.put("message", "User Not Saved Or Updated Exception");
			return "error";
		}

		catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while saving  user registration information  " + e.getMessage());
			map.put("message", "Error while saving  user registration information");
			return "error";
		}
	}

	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @param username
	 * @return
	 * 
	 *         Method for to check the user Existance based on either username or
	 *         email
	 */
	@RequestMapping(value = "/checkuserexistance.do", method = RequestMethod.GET)
	@ResponseBody
	public String checkUsernameOrEmailExistance(@RequestParam("user") String user) {
		log.info("AdminController -> checkUsernameOrEmailExistance()");
		try {

			UserDto adminUserDto = this.userService.getUserByUsernameOrEmailId(user);
			if (null == adminUserDto) {
				throw new UserNotFoundException();
			} else {
				return "fail";
			}
		} catch (UserNotFoundException e) {
			return "success";
		} catch (Exception e) {
			log.error(" Error while check user existance " + e.getMessage());
			e.printStackTrace();
			return "fail";
		}

	}

	/**
	 * Created By Bhagya on May 10th, 2021
	 * 
	 * @param map
	 * @param email
	 * @return
	 * 
	 *         Method for to send registration confirmation message
	 */
	@RequestMapping(value = "/registersuccess.do", method = RequestMethod.GET)
	public String registrationSuccess(Map<String, Object> map, @RequestParam("email") String email) {
		log.info(" AdminController -> registrationSuccess()");
		try {
			String message = "Registered Successfully, A Mail has been sent to your email: " + email
					+ ", Please click on the activation link to activate your account ";
			String title = "Successfully Registered to Hovey Tracker, Please check your mail to activate the account";
			map.put("email", email);
			map.put("title", title);
			map.put("message", message);
			return "admin/registerSuccess";

		}

		catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while sending registration success information  " + e.getMessage());
			map.put("message", "Error while sending registration success information");
			return "error";
		}
	}

	/**
	 * Created By Bhagya On May 10th, 2021 Activating User Based on Account Token
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/activateuser.do")
	public String activateUserAccount(@RequestParam("token") String accountToken,
			@RequestParam("uname") String username, Map<String, Object> map, RedirectAttributes redAttribs) {
		log.info("inside AdminController -> activateUserAccount()");
		try {
			Integer activationResult = this.adminService.activateUserFromAccountToken(accountToken);
			UserDto adminUserDto = this.userService.getUserByUsernameOrEmailId(username);

			if (activationResult > 0) {
				return "redirect:/login.do";
			} else {
				throw new Exception();
			}
		} catch (UserNotFoundException e) {
			String message = "No User found, User may be activated already";
			String title = "User Activation";
			map.put("title", title);
			log.error("No User found, User may be activated already" + e.getMessage() + " " + e.toString());
			map.put("message", message);

			return "admin/registrationSuccess";
		} catch (Exception e) {
			String message = "Error while activating user";
			e.printStackTrace();
			log.error(message + " " + e.getMessage());
			map.put("message", message);
			return "error";
		}
	}

	/**
	 * Created By Bhagya on May 10th, 2021
	 * 
	 * @param userId
	 * @param map
	 * @return
	 * 
	 *         Method for to initiate the edit user profile
	 */
	@RequestMapping(value = "editprofile.do", method = RequestMethod.GET)
	public String initEditProfile(HttpServletRequest request,
			@RequestParam(value = "userId", required = false) Integer userId, Map<String, Object> map,
			@ModelAttribute("userDto") UserDto userDto) {
		log.info("AdminController -> initEditProfile() ");
		try {
			if (userId == null || userId == 0) {
				HttpSession session = request.getSession(true);
				userId = (Integer) session.getAttribute("userId");
			}

			UserDto userDataDto = this.userService.getUserByUserId(userId);
			ArrayList<String> states = this.adminService.getListofStates();
			map.put("states", states);
			map.put("userDto", userDataDto);
			return "admin/editProfile";
		} catch (UserNotFoundException e) {
			e.printStackTrace();
			String error = "User Not Found";
			map.put("message", error);
			return "error";
		}

		catch (Exception e) {
			e.printStackTrace();
			log.error("Error while edit registration  " + e.getMessage());
			String error = "Error while edit registration";
			map.put("message", error);
			return "error";
		}

	}

	/**
	 * Created By Bhagya on May 10th, 2021
	 * 
	 * @param UserDto
	 * @param redAttribs
	 * @param map
	 * @return
	 * 
	 *         Method for to update the user profile
	 */

	@RequestMapping(value = "editprofile.do", method = RequestMethod.POST)
	public String editUserProfile(@ModelAttribute("userDto") UserDto userDto, RedirectAttributes redAttribs,
			Map<String, Object> map) {
		log.info(" AdminController -> editUserProfile()");
		try {

			Integer updatedResult = this.adminService.saveOrUpdateUser(userDto);
			if (updatedResult > 0) {
				redAttribs.addFlashAttribute("status", "User Updated Successfully");
				return "redirect:/admin/viewusers.do";
			} else {
				throw new Exception();
			}
		} catch (UserNotFoundException e) {
			e.printStackTrace();
			String error = "User Not Found";
			map.put("message", error);
			return "error";
		}

		catch (Exception e) {
			e.printStackTrace();
			log.error("Error while updating user details  " + e.getMessage());
			String error = "Error while updating user details";
			map.put("message", error);
			return "error";
		}

	}

	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @return Method for to initiate the forgot password
	 */

	@RequestMapping(value = "/forgotpassword.do", method = RequestMethod.GET)
	public String initForgotPassword(Map<String, Object> map) {
		log.info("AdminController-> initForgotPassword()");
		try {
			/*
			 * String msg= ""; map.put("error", msg);
			 */
			return "admin/forgotPassword";
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while initiate forgot password " + e.getMessage());
			map.put("message", "Error while initiate forgot password");
			return "error";
		}

	}

	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @return
	 * 
	 *         Method for to perform the forgot password functionality
	 */
	@RequestMapping(value = "/forgotpassword.do", method = RequestMethod.POST)
	public String performForgotPassword(@RequestParam("emailId") String emailId, Map<String, Object> map) {
		log.info("AdminController -> performForgotPassword()");
		try {
			if (!emailId.equals("")) {
				String msg = "";
				Integer successMail = this.adminService.sendPasswordResetEmail(emailId);
				if (successMail == 1) {
					msg = "Password reset link has been sent to your registered email, please check your mail.";
					map.put("status", msg);
					return "login";
				} else {
					throw new Exception();
				}

			} else {
				String msg = "Please enter the email id.";
				map.put("error", msg);
				return "admin/forgotPassword";
			}
		} catch (UserNotFoundException e) {
			e.printStackTrace();
			String msg = "Email not registered, please Sign Up.";
			map.put("status", msg);
			return "login";
		} catch (Exception e) {
			log.error(" Error while performing forgot Password " + e.getMessage());
			String msg = "Error while performing forgot password";
			map.put("message", msg);
			e.printStackTrace();
			return "error";
		}

	}

	/**
	 * Created By bhagya On May 10th, 2021
	 * 
	 * @param token
	 * @param map
	 * @return
	 * 
	 *         Method for initiate the Reset Password
	 */
	@RequestMapping(value = "/resetpassword.do", method = RequestMethod.GET)
	public String initResetPassword(@RequestParam("token") String token, Map<String, Object> map) {
		log.info("AdminController -> initResetPassword()");
		try {
			Integer userId = this.adminService.getUserIdByPasswordResetToken(token);
			map.put("userId", userId);

			return "admin/resetPassword";
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while initiate reset Password " + e.getMessage());
			map.put("errorMessage", "This link is not valid any more");
			return "login";
		}
	}

	/**
	 * Created By Bhagya on May 10th, 2021
	 * 
	 * @param userId
	 * @param password
	 * @param confirmPassword
	 * @param map
	 * @return Method For Saving the reset Password
	 */
	@RequestMapping(value = "/resetpassword.do", method = RequestMethod.POST)
	public String performResetPassword(@RequestParam("userId") Integer userId,
			@RequestParam("password") String password, @RequestParam("confirmPassword") String confirmPassword,
			Map<String, Object> map) {
		log.info("AdminController ->performResetPassword()");
		try {
			map.put("userId", userId);
			if (!password.equals("") && !confirmPassword.equals("")) {
				if (password.length() < 8) {

					map.put("error", "Password must be more than 8 characters");
					return "admin/resetPassword";
				}
				if (password.equals(confirmPassword)) {
					this.adminService.handlePasswordReset(userId, password);
					map.put("status", "Password Reset Successfully");
					return "login";
				} else {
					map.put("error", "Confirmation Password doesn't match Password, Please enter again");
					return "admin/resetPassword";
				}
			} else {

				if (password.equals("") && confirmPassword.equals("")) {
					map.put("error", "Enter all the values");
				}
				return "admin/resetPassword";
			}
		} catch (UserNotFoundException e) {
			String error = "User Not Found";
			map.put("message", error);
			return "error";
		} catch (UserNotSavedOrUpdatedException e) {
			String error = "User Not Saved Or Updated";
			map.put("message", error);
			return "error";
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while reset Password " + e.getMessage());
			String error = "Problem While Reset Password, Please Try Again Later";
			map.put("message", error);
			return "error";
		}

	}

	/**
	 * Created By bhagya on may 12th,2021
	 * 
	 * @param map
	 * @param status
	 * @return
	 * 
	 *         service for to view users
	 */

	
	@RequestMapping(value="/viewusers.do")
	public String initViewUsers(Map<String,Object> map,@RequestParam(value="status",required=false) String status,
			@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info( " AdminController -> ViewUsers()");
		Integer totalResults;
		try{
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("username");
			}
			ArrayList<UserDto> userDtos=this.adminService.getListofUsers(listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSearchBy(),listBeanDto.getSortBy(),listBeanDto.getSortDirection());
			totalResults=userDtos.get(0).getTotalUsers();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("userDtos",userDtos);
			map.put("status",status);
			return "admin/manageUsers";

		}
		catch(UserNotFoundException e) {
			String error="No user was found";
			map.put("message",error);
			return "admin/manageUsers";
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating view users " + e.getMessage());
			map.put("message", "Error while initiating view users");
			return "error";
		}

	}

	/**
	 * Created By bhagya On May 10th, 2021
	 * 
	 * @param token
	 * @param map
	 * @return
	 * 
	 *         Method for initiate the Reset Password
	 */
	@RequestMapping(value = "/adminresetpassword.do", method = RequestMethod.GET)
	public String initAdminResetPassword(HttpServletRequest request,
			@RequestParam(value = "userId", required = false) Integer userId, Map<String, Object> map) {
		log.info("AdminController -> initAdminResetPassword()");
		try {
			if (userId == null || userId == 0) {
				HttpSession session = request.getSession(true);
				userId = (Integer) session.getAttribute("userId");
			}
			map.put("userId", userId);

			return "admin/adminResetPassword";
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while initiate reset Password " + e.getMessage());
			return "error";
		}
	}

	/**
	 * Created By Bhagya on May 10th, 2021
	 * 
	 * @param userId
	 * @param password
	 * @param confirmPassword
	 * @param map
	 * @return Method For Saving the reset Password
	 */
	@RequestMapping(value = "/adminresetpassword.do", method = RequestMethod.POST)
	public String performAdminResetPassword(@RequestParam("userId") Integer userId,
			@RequestParam("password") String password, Map<String, Object> map) {
		log.info("AdminController ->performAdminResetPassword()");
		try {
			map.put("userId", userId);
			this.adminService.handleAdminPasswordReset(userId, password);
			map.put("status", "Password Reset Successfully");
			return "admin/adminResetPassword";

		} catch (UserNotFoundException e) {
			String error = "User Not Found";
			map.put("message", error);
			return "error";
		} catch (UserNotSavedOrUpdatedException e) {
			String error = "User Not Saved Or Updated";
			map.put("message", error);
			return "error";
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while reset Password " + e.getMessage());
			String error = "Problem While Reset Password, Please Try Again Later";
			map.put("message", error);
			return "error";
		}
	}

	/**
	 * Created By Bhagya on may 13th, 2021
	 * 
	 * @param userId
	 * @param isActive
	 * @return
	 * 
	 *         Service for to change the user account status
	 */

	@ResponseBody
	@RequestMapping("/changeuserstatus.do")
	public String changeUserStatus(@RequestParam("userId") Integer userId, @RequestParam("isActive") Boolean isActive) {
		log.info(" AdminController -> changeUserStatus()");
		try {
			Integer savedResult = this.adminService.changeUserAccountStatus(userId, isActive);
			return "SUCCESS";
		} catch (UserNotFoundException e) {
			log.error(" User Not Found For Changing the user Status");
			return "ERROR";
		} catch (UserNotSavedOrUpdatedException e) {
			log.error("User Status Is Not Saved Or Updated");
			return "ERROR";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}

	}

	/**
	 * Created By BHagya On May 17th, 2021
	 * 
	 * @param map
	 * @param request
	 * @return
	 * 
	 *         Service for to get the daily scoreboard
	 */
	@RequestMapping(value = "/dailyscoreboard.do", method = RequestMethod.GET)
	public String initDailyScoreBoard(Map<String, Object> map, HttpServletRequest request) {
		log.info(" AdminController -> initDailyScoreBoard()");
		HttpSession session = request.getSession(true);
		Integer userId = (Integer) session.getAttribute("userId");
		try {
			map.put("userId", userId);
			DailyScoreBoardDto dailyScoreBoardDto = this.adminService.getDailyScoreBoard();
			map.put("dealsCount", dailyScoreBoardDto.getDealsCount());
			map.put("xfersCount", dailyScoreBoardDto.getXfersCount());
			return "admin/dailyScoreboard";

		} catch (DailyScoreBoardNotFoundException e) {
			log.info("AdminController -> initDailyScoreBoard() -> DailyScoreBoardNotFoundException()");
			map.put("userId", userId);
			map.put("dealsCount", "00");
			map.put("xfersCount", "00");
			return "admin/dailyScoreboard";
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" Error while initiating Daily Scoreboard " + e.getMessage());
			map.put("message", "Error while initiating Daily ScoreBoard ");
			return "error";
		}

	}

	/**
	 * Created By Bhagya on may 17th 2021
	 * 
	 * @param userId
	 * @param dealsCount
	 * @param xfersCount
	 * @return
	 * 
	 *         Service for to edit dailyScoreboard
	 */
	@ResponseBody
	@RequestMapping("/editdailyscoreboard.do")
	public String editDailyScoreBoard(@RequestParam("userId") Integer userId,
			@RequestParam("dealsCount") String dealsCount, @RequestParam("xfersCount") String xfersCount) {
		log.info(" AdminController -> editDailyScoreBoard()");
		try {
			DailyScoreBoardDto dailyScoreBoardDto = new DailyScoreBoardDto();
			dailyScoreBoardDto.setDealsCount(dealsCount);
			dailyScoreBoardDto.setXfersCount(xfersCount);
			Integer savedResult = this.adminService.saveOrUpdateDailyScoreBoard(dailyScoreBoardDto, userId);
			if (savedResult > 0) {
				return "SUCCESS";
			} else {
				throw new Exception();
			}

		} catch (DailyScoreBoardNotSavedOrUpdatedException e) {
			log.error("AdminController -> editDailyScoreBoard()-> DailyScoreBoardNotSavedOrUpdatedException ");
			return "ERROR";
		}

		catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}

	}

	/**
	 * Created By Bhagya on may 22nd, 2021
	 * 
	 * @param request
	 * @param fronterId 
	 * @param count
	 * @param dayName
	 * @return
	 * 
	 *         Service for to update the fronter board by admin
	 */
	@ResponseBody
	@RequestMapping("/editfronterboard.do")
	public String editFronterBoard(@RequestParam("adminUserId") Integer adminUserId,
			@RequestParam("fronterId") Integer fronterId, @RequestParam("count") Integer count,
			@RequestParam("dayName") String dayName) {
		log.info(" AdminController -> editFronterBoard()");
		try {

			Integer savedResult = this.adminService.updateFronterBoardByAdmin(adminUserId, fronterId, count, dayName);
			if (savedResult > 0) {
				return "SUCCESS";
			} else {
				throw new Exception();
			}

		} catch (FronterNotSavedOrUpdatedException e) {
			log.error("AdminController -> editfronterboard()-> FronterNotSavedOrUpdatedException " + e.toString());
			return "ERROR";
		}

		catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}

	}

	/**
	 * Created By Bhagya on May 22nd, 2021
	 * 
	 * @param adminUserId
	 * @param lastWeekCount
	 * @return
	 * 
	 *         Service for to update fronters board data
	 */
	@ResponseBody
	@RequestMapping("/updatefrontersboarddata.do")
	public String updateFrontersBoardData(@RequestParam("adminUserId") Integer adminUserId,
			@RequestParam("lastWeekCount") Integer lastWeekCount,@RequestParam("weekNum") Integer weekNum,
			@RequestParam("weekGoal") Integer weekGoal) {
		log.info(" AdminController -> updateFrontersBoardData()");
		try {
			FrontersBoardDataDto frontersBoardDataDto=new FrontersBoardDataDto();
			frontersBoardDataDto.setLastWeek(lastWeekCount);
			frontersBoardDataDto.setWeekNum(weekNum);
			frontersBoardDataDto.setWeekGoal(weekGoal);
			Integer savedResult = this.adminService.saveOrUpdateFrontersBoardData(adminUserId, frontersBoardDataDto);
			if (savedResult > 0) {
				return "SUCCESS";
			} else {
				throw new Exception();
			}

		} catch (FronterBoardDataNotSavedOrUpdatedException e) {
			log.error("AdminController -> updateFrontersBoardData()-> FronterBoardDataNotSavedOrUpdatedException "
					+ e.toString());
			return "ERROR";
		}

		catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}

	}

	/**
	 * Created By Harshitha on May 25th, 2021
	 * 
	 * @param adminUserId
	 * @param lastWeek
	 * @param WeekNum
	 * @param WeekGoal
	 * @return
	 * 
	 *         Service for to update closers board data
	 */
	@ResponseBody
	@RequestMapping("/updateclosersboarddata.do")
	public String updateClosersBoardData(@RequestParam("adminUserId") Integer adminUserId,
			@RequestParam("lastWeek") Integer lastWeek, @RequestParam("weekNum") Integer weekNum,
			@RequestParam("weekGoal") Integer weekGoal) {
		log.info(" AdminController -> updateClosersBoardData()");
		try {
			ClosersBoardDataDto closersBoardDataDto = new ClosersBoardDataDto();
			closersBoardDataDto.setLastWeek(lastWeek);
			closersBoardDataDto.setWeekNum(weekNum);
			closersBoardDataDto.setWeekGoal(weekGoal);

			Integer savedResult = this.adminService.saveOrUpdateClosersBoardData(adminUserId, closersBoardDataDto);
			if (savedResult > 0) {
				return "SUCCESS";
			} else {
				throw new Exception();
			}

		} catch (CloserBoardDataNotSavedOrUpdatedException e) {
			log.error("AdminController -> updateClosersBoardData()-> CloserBoardDataNotSavedOrUpdatedException "
					+ e.toString());
			return "ERROR";
		}

		catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}

	}

	/**
	 * Created By Harshitha on may 25th, 2021
	 * 
	 * @param request
	 * @param closerId
	 * @param count
	 * @param dayName
	 * @return
	 * 
	 *Service for to update the closer board by admin
	 */
	@ResponseBody
	@RequestMapping("/editcloserboard.do")
	public String editCloserBoard(@RequestParam("adminUserId") Integer adminUserId, @RequestParam("closerId") Integer closerId, @RequestParam("count") Integer count, @RequestParam("dayName") String dayName) {
		log.info(" AdminController -> editCloserBoard()");
		try {

			Integer savedResult = this.adminService.updateCloserBoardByAdmin(adminUserId, closerId, count, dayName);
			if (savedResult > 0) {
				return "SUCCESS";
			} else {
				throw new Exception();
			}

		} catch (CloserNotSavedOrUpdatedException e) {
			log.error("AdminController -> editcloserboard()-> CloserNotSavedOrUpdatedException " + e.toString());
			return "ERROR";
		}

		catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}

	}

  /**
	 * Created By Harshitha on May 27th, 2021
	 * @param fronterId
	 * @return
	 * 
	 * Service for to delete the fronter based on fronterId
  */
	@ResponseBody
	@RequestMapping("/deletefronter.do")
	public String deleteFronter(@RequestParam("fronterId")Integer fronterId){
		log.info(" AdminController -> deleteFronter()");
		try{
			Integer deletedResult=this.adminService.deleteFronter(fronterId);
			if(deletedResult>0) {
				return "SUCCESS";	
			}
			else {
				return "ERROR";
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
		
	}
	
	
	/**
	 * Created By Harshitha on May 27th, 2021
	 * @param closerId
	 * @return
	 * 
	 * Service for to delete the closer based on closerId
  */
	@ResponseBody
	@RequestMapping("/deletecloser.do")
	public String deleteCloser(@RequestParam("closerId")Integer closerId){
		log.info(" AdminController -> deleteCloser()");
		try{
			Integer deletedResult=this.adminService.deleteCloser(closerId);
			if(deletedResult>0) {
				return "SUCCESS";	
			}
			else {
				return "ERROR";
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return "ERROR";
		}
		
	}
				 

}