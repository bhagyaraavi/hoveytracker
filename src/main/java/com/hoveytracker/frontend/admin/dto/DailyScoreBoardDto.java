package com.hoveytracker.frontend.admin.dto;

import java.util.Date;

import com.hoveytracker.backend.admin.model.DailyScoreBoard;
import com.hoveytracker.frontend.user.dto.UserDto;
/**
 * 
 * @author Bhagya
 *Created On 17th may 2021
 *Dto class for Daily ScoreBoard Dto
 */

public class DailyScoreBoardDto{
	
	private Integer scoreId;
	private Date createdDate;
	private Date updatedDate;
	private UserDto userDto;
	private String dealsCount;
	private String xfersCount;
	
	public Integer getScoreId() {
		return scoreId;
	}
	public void setScoreId(Integer scoreId) {
		this.scoreId = scoreId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public String getDealsCount() {
		return dealsCount;
	}
	public void setDealsCount(String dealsCount) {
		this.dealsCount = dealsCount;
	}
	public String getXfersCount() {
		return xfersCount;
	}
	public void setXfersCount(String xfersCount) {
		this.xfersCount = xfersCount;
	}
	
	public static DailyScoreBoardDto populateDailyScoreBoardDto(DailyScoreBoard dailyScoreBoard) {
		
		DailyScoreBoardDto dailyScoreBoardDto=new DailyScoreBoardDto();
		dailyScoreBoardDto.setScoreId(dailyScoreBoard.getScoreId());
		if(null!=dailyScoreBoard.getCreatedDate())
		dailyScoreBoardDto.setCreatedDate(dailyScoreBoard.getCreatedDate());
		if(null!=dailyScoreBoard.getUpdatedDate())
		dailyScoreBoardDto.setUpdatedDate(dailyScoreBoard.getUpdatedDate());
		dailyScoreBoardDto.setDealsCount(dailyScoreBoard.getDealsCount());
		dailyScoreBoardDto.setXfersCount(dailyScoreBoard.getXfersCount());
		
		return dailyScoreBoardDto;
	}
}