package com.hoveytracker.frontend.admin.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hoveytracker.backend.admin.dao.AdminDao;
import com.hoveytracker.backend.admin.model.DailyScoreBoard;
import com.hoveytracker.backend.admin.model.State;
import com.hoveytracker.backend.user.dao.UserDao;
import com.hoveytracker.backend.user.model.Closer;
import com.hoveytracker.backend.user.model.ClosersBoardData;
import com.hoveytracker.backend.user.model.Fronter;
import com.hoveytracker.backend.user.model.FrontersBoardData;
import com.hoveytracker.backend.user.model.FrontersGrandTotal;
import com.hoveytracker.backend.user.model.User;
import com.hoveytracker.common.exceptions.CloserBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.CloserNotFoundException;
import com.hoveytracker.common.exceptions.CloserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.ClosersBoardDataNotFoundException;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotFoundException;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterGrandTotalNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterNotFoundException;
import com.hoveytracker.common.exceptions.FronterNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FrontersBoardDataNotFoundException;
import com.hoveytracker.common.exceptions.FrontersGrandTotalNotFoundException;
import com.hoveytracker.common.exceptions.MailNotSentException;
import com.hoveytracker.common.exceptions.NotificationConfigurationNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.StateNotFoundException;
import com.hoveytracker.common.exceptions.UserNotFoundException;
import com.hoveytracker.common.exceptions.UserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.UserNotificationCountNotSavedOrUpdatedException;
import com.hoveytracker.frontend.admin.dto.DailyScoreBoardDto;
import com.hoveytracker.frontend.user.dto.ClosersBoardDataDto;
import com.hoveytracker.frontend.user.dto.FrontersBoardDataDto;
import com.hoveytracker.frontend.user.dto.FrontersGrandTotalDto;
import com.hoveytracker.frontend.user.dto.NotificationConfigurationDto;
import com.hoveytracker.frontend.user.dto.UserDto;
import com.hoveytracker.frontend.user.dto.UserNotificationCountDto;
import com.hoveytracker.frontend.user.service.UserService;
import com.hoveytracker.frontend.user.service.UserServiceImpl;
import com.hoveytracker.frontend.utility.EmailSender;

@Transactional
@Service("adminService")
public class AdminServiceImpl implements AdminService{
	
	private static Logger log = Logger.getLogger(UserServiceImpl.class);

	@Resource(name = "adminDao")
	private AdminDao adminDao;
	
	@Resource(name = "userService")
	private UserService userService;
	
	@Resource(name = "emailSender")
	private EmailSender emailSender;
	
	@Resource(name = "userDao")
	private UserDao userDao;
	
	@Autowired
	private org.springframework.security.crypto.password.PasswordEncoder bCryptEncoder;
	
	
	
	/**
	 * Created By Bhagya on May 10th, 2021
	 * @param userDto
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Service for to save or update user details
	 */
	public Integer saveOrUpdateUser(UserDto userDto) throws UserNotSavedOrUpdatedException {
		log.info("AdminServiceImpl -> saveOrUpdateUser()");
		User user=null;
		Boolean newUser=false;
		try {
			user=this.userDao.getUserByUserId(userDto.getUserId());
			user.setAccountUpdationDate(new Date());
			
		}
		catch(UserNotFoundException e) {
			newUser=true;
			user=new User();
			user.setAccountCreationDate(new Date());
			user.setIsActive(false);
			user.setAccountActivationToken(this.generateToken());
			 Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				cal.add(Calendar.DATE, 1);
				Date validity = cal.getTime();
				user.setActivationTokenExpiryDate(validity);
		}
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setEmailId(userDto.getEmailId());
		user.setUsername(userDto.getUsername());
		user.setCity(userDto.getCity());
		user.setState(userDto.getState());
		user.setZipcode(userDto.getZipcode());
		user.setUserType(userDto.getUserType());
		if(null!=userDto.getPassword()){
			String encryptedPassword = bCryptEncoder.encode(userDto.getPassword());
			user.setPassword(encryptedPassword);
			}
		
		Integer savedUserId=this.userDao.saveOrUpdateUser(user);
		
		
		  if(null!=savedUserId && newUser==true){
			  //send confirmation mail
			  UserDto savedUserDto=UserDto.populateUserDto(user); 
			  try{
				  
				  this.emailSender.sendConfirmationMail(savedUserDto); 
				  } 
			  catch(MailNotSentException e) { // TODO Auto-generated catch block
				  e.printStackTrace(); 
				  }
			
			  /**
				 * Created By Bhagya on june 17th, 2021
				 *  to save the Notification configuration
				 */
			  System.out.println("userId at admin service"+savedUserId);
			  NotificationConfigurationDto notificationConfigurationDto=new NotificationConfigurationDto();
			  notificationConfigurationDto.setScoreNotification(true);
			  notificationConfigurationDto.setMessageNotification(true);
			  try {
				this.userService.saveOrUpdateNotificationConfiguration(notificationConfigurationDto, savedUserId);
			} catch (NotificationConfigurationNotSavedOrUpdatedException e) {
				
			}
			  /**
				 * Created By harshitha on june 17th, 2021
				 *  to get the user notification count
				 */
			  UserNotificationCountDto userNotificationCountDto = new UserNotificationCountDto();
			  userNotificationCountDto.setNotificationCount(0);
			  
			  try {
				this.userService.saveOrUpdateUserNotificationCount(userNotificationCountDto, savedUserId);
			} catch (UserNotificationCountNotSavedOrUpdatedException e) {
				
			}
	}//end of if
		 
		return savedUserId;
	}
	
	/**
	 * Created By Bhagya On May 10th, 2021 This Method return random UUID
	 * java.util.UUID class represents an immutable universally unique
	 * identifier (UUID)
	 * 
	 */
	public String generateToken() {
		log.info("AdminServiceImpl-> generatePasswordToken()");
		String uuid = UUID.randomUUID().toString();
		String token = uuid.toString().replaceAll("-", "").toUpperCase();
		return token;
	}
	
	
	/**
	 * Created By bhagya on May 10th, 2021
	 * 
	 * Method to Activate User
	 * 1. Gets User based on Account TOken..
	 * 2. Updates isApproved(true)
	 * 3. Delete Account Token..
	 * 4. Update User.
	 * 5. Send Response Back
	 */
	public Integer activateUserFromAccountToken(String token)throws Exception{
		log.info("AdminServiceImpl-> activateUserFromAccountToken()");
		Integer result = 0;
		User user=this.adminDao.getUserByAccountToken(token);
		Date activationTokenExpiryDate = user.getActivationTokenExpiryDate();
		if (activationTokenExpiryDate.after(new Date())) {
			user.setIsActive(true);
			user.setAccountActivationToken(null);
			user.setActivationTokenExpiryDate(null);
		 result=this.userDao.saveOrUpdateUser(user);
		}
		
		return result;
	}
	
	
	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @param email
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for to send the password reset email to user
	 * @throws MailNotSentException
	 */
	public Integer sendPasswordResetEmail(String email)throws UserNotFoundException, UserNotSavedOrUpdatedException,MailNotSentException {
		log.info("AdminServiceImpl-> sendPasswordResetEmail()");
		User user = this.userDao.getUserforAuthentication(email);
		String passwordToken = this.generateToken();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		Date validity = cal.getTime();
		user.setPasswordTokenExpiryDate(validity);
		user.setPasswordToken(passwordToken);
		this.userDao.saveOrUpdateUser(user);
		UserDto userDto = UserDto.populateUserDto(user);
		this.emailSender.sendForgotPasswordMail(userDto);
		return 1;
	}
	
	/**
	 * Created By Bhagya On May 10th,2021
	 * 
	 * @param token
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method For Getting the UserId By Password Reset TOken
	 */
	public Integer getUserIdByPasswordResetToken(String token)throws UserNotFoundException {
		log.info("AdminServiceImpl-> getUserIdByPasswordResetToken()");
		User user = this.adminDao.getUserByPasswordResetToken(token);
		return user.getUserId();
	}
	
	
	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @param userId
	 * @param password
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for hadling the process of password reset
	 */
	public Integer handlePasswordReset(Integer userId, String password)throws UserNotFoundException, UserNotSavedOrUpdatedException {
		log.info("AdminServiceImpl-> handlePasswordReset()");
		Integer result = 0;
		User user = this.userDao.getUserByUserId(userId);
		Date passwordTokenExpiryDate = user.getPasswordTokenExpiryDate();
		if (passwordTokenExpiryDate.after(new Date())) {
			String encryptedPassword = bCryptEncoder.encode(password);
			user.setPassword(encryptedPassword);
			user.setPasswordToken(null);
			user.setPasswordTokenExpiryDate(null);
			result = this.userDao.saveOrUpdateUser(user);
		}
		return result;
	}
	/**
	 * Created By BHagya on May 12th, 2021
	 * @return
	 * @throws StateNotFoundException
	 * 
	 * Service for to get the list of states
	 */
	public ArrayList<String> getListofStates() throws StateNotFoundException{
		log.info("AdminServiceImpl-> getListofStates()");
		ArrayList<String> statesList=new ArrayList<String>();
		ArrayList<State> states=this.adminDao.getStatesListFromDB();
		for(State state:states) {
			statesList.add(state.getStateName());
		}
		return statesList;
	}
	/**
	 * Created By Bhagya On May 10th, 2021
	 * 
	 * @param userId
	 * @param password
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for hadling the process of admin password reset
	 */
	public Integer handleAdminPasswordReset(Integer userId, String password)throws UserNotFoundException, UserNotSavedOrUpdatedException {
		log.info("AdminServiceImpl-> handlePasswordReset()");
		Integer result = 0;
		User user = this.userDao.getUserByUserId(userId);
			String encryptedPassword = bCryptEncoder.encode(password);
			user.setPassword(encryptedPassword);
			result = this.userDao.saveOrUpdateUser(user);
		return result;
	}
	
	
	/**
	 * Created By Bhagya on may 13th, 2021
	 * Service for to get the list of users
	 */
	public ArrayList<UserDto> getListofUsers(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws  UserNotFoundException{
    	log.info(" AdminServiceImpl -> getListofUsers() ");
  	  ArrayList<UserDto> userDtos=new ArrayList<UserDto>();
  	  ArrayList<User> users=this.adminDao.getListOfUsers(pageNo,pageSize,searchBy,sortBy,ascending);
  	  for(User user:users){ 
  		  UserDto userDto=UserDto.populateUserDto(user);
  		  
  		  userDtos.add(userDto);
  	  }
  	  userDtos.get(0).setTotalUsers(users.get(0).getTotalUsers());
  	  return userDtos;
    }
	/**
	 * Created By Bhagya on May 13th, 2021
	 * @param userId
	 * @param isActive
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Service for to change the user account status
	 */
	
	public Integer changeUserAccountStatus(Integer userId,Boolean isActive) throws UserNotFoundException, UserNotSavedOrUpdatedException{
    	log.info("AdminServiceImpl -> changeUserAccountStatus");	    	
    	User user=this.userDao.getUserByUserId(userId);
    	user.setIsActive(isActive);
    	Integer updatedResult=this.userDao.saveOrUpdateUser(user);
    	return updatedResult;
    }
	/**
	 * Created By Bhagya on May 17th, 2021
	 * @param dailyScoreBoardDto
	 * @param userId
	 * @return
	 * @throws DailyScoreBoardNotSavedOrUpdatedException
	 * 
	 * Service for to save or update dailyScoreboard
	 */
	public Integer saveOrUpdateDailyScoreBoard(DailyScoreBoardDto dailyScoreBoardDto,Integer userId) throws DailyScoreBoardNotSavedOrUpdatedException {
		log.info("AdminServiceImpl -> saveOrUpdateDailyScoreBoard()");
		DailyScoreBoard dailyScoreBoard=null;
		
		try {
			dailyScoreBoard=this.adminDao.getDailyScoreBoard();
			dailyScoreBoard.setUpdatedDate(new Date());
		}
		catch(DailyScoreBoardNotFoundException e) {
			dailyScoreBoard=new DailyScoreBoard();
			dailyScoreBoard.setCreatedDate(new Date());
		}
		
		dailyScoreBoard.setDealsCount(dailyScoreBoardDto.getDealsCount());
		dailyScoreBoard.setXfersCount(dailyScoreBoardDto.getXfersCount());
		
		Integer savedResult=this.adminDao.saveOrUpdateDailyScoreBoard(dailyScoreBoard);
		
		return savedResult;
	}
	/**
	 * Created By Bhagya on May 17th, 2021
	 * @return
	 * @throws DailyScoreBoardNotFoundException
	 * 
	 * Service for to get the dailyScoreboard
	 */
	public DailyScoreBoardDto getDailyScoreBoard() throws DailyScoreBoardNotFoundException {
		log.info("AdminServiceImpl -> getDailyScoreBoard()");
		DailyScoreBoard dailyScoreBoard=this.adminDao.getDailyScoreBoard();
		DailyScoreBoardDto dailyScoreBoardDto=DailyScoreBoardDto.populateDailyScoreBoardDto(dailyScoreBoard);
		return dailyScoreBoardDto;
	}
	/**
	 * Created By Bhagya On May 22nd, 2021
	 * @param adminUserId
	 * @param fronterId
	 * @param count
	 * @param dayName
	 * @return
	 * @throws FronterNotSavedOrUpdatedException
	 * 
	 * Service for to update Fronter board by admin
	 */
	
	public Integer updateFronterBoardByAdmin(Integer adminUserId,Integer fronterId,Integer count,String dayName) throws FronterNotSavedOrUpdatedException {
		log.info("AdminServiceImpl -> updateFronterBoardByAdmin()");
		Integer updatedResult=0;
			try {
				Fronter fronter=this.userDao.getFronterByFronterId(fronterId);
				try {
					User adminUser=this.userDao.getUserByUserId(adminUserId);
					fronter.setAdminUser(adminUser);
				} catch (UserNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				fronter.setIsUpdateByAdmin(true);
				fronter.setAdminUpdationDate(new Date());
				Integer totalCount=0;
				// monday
				if(dayName.equalsIgnoreCase("MONDAY")) {
					fronter.setMon(count);	
				}
				// tuesday
				if(dayName.equalsIgnoreCase("TUESDAY")) {
					fronter.setTues(count);	
				}
				// wednesday
				if(dayName.equalsIgnoreCase("WEDNESDAY")) {
					fronter.setWed(count);	
				}
				// thursday
				if(dayName.equalsIgnoreCase("THURSDAY")) {
					fronter.setThurs(count);	
				}
				// friday
				if(dayName.equalsIgnoreCase("FRIDAY")) {
					fronter.setFri(count);	
				}
				// calculating total
				try {
					totalCount=totalCount+fronter.getMon();
				}
				catch(NullPointerException e) {
					
				}
				try {
					totalCount=totalCount+fronter.getTues();
				}
				catch(NullPointerException e) {
					
				}
				try {
					totalCount=totalCount+fronter.getWed();
				}
				catch(NullPointerException e) {
					
				}
				try {
					totalCount=totalCount+fronter.getThurs();
				}
				catch(NullPointerException e) {
					
				}
				try {
					totalCount=totalCount+fronter.getFri();
				}
				catch(NullPointerException e) {
					
				}
				// end of total calculation
				fronter.setTotal(totalCount);
				updatedResult=this.userDao.saveOrUpdateFronter(fronter);
				if(updatedResult>0) {
					this.userService.saveOrUpdateFrontersGrandTotal();
				}
			} 
			catch (FronterNotFoundException e) {
			log.info("AdminServiceImpl -> updateFronterBoardByAdmin() -> FronterNotFoundException "+e.toString());
			}
		
		return updatedResult;
		
		
	}
	
	/**
	 * Created By Bhagya on may 22nd, 2021
	 * @param adminUserId
	 * @param lastWeekCount
	 * @return
	 * @throws FronterBoardDataNotSavedOrUpdatedException
	 * 
	 * Service for to save or update fronters board data
	 */
	public Integer saveOrUpdateFrontersBoardData(Integer adminUserId,FrontersBoardDataDto frontersBoardDataDto) throws FronterBoardDataNotSavedOrUpdatedException {
		log.info("AdminServiceImpl -> saveOrUpdateFrontersBoardData ()");
		FrontersBoardData frontersBoardData=null;
		try {
			 frontersBoardData=this.userDao.getFrontersBoardData();
			 frontersBoardData.setUpdationDate(new Date());
		}
		catch(FrontersBoardDataNotFoundException e) {
			frontersBoardData=new FrontersBoardData();
			frontersBoardData.setCreationDate(new Date());
		}
		try {
			User adminUser=this.userDao.getUserByUserId(adminUserId);
			frontersBoardData.setUser(adminUser);
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frontersBoardData.setLastWeek(frontersBoardDataDto.getLastWeek());
		frontersBoardData.setWeekNum(frontersBoardDataDto.getWeekNum());
		frontersBoardData.setWeekGoal(frontersBoardDataDto.getWeekGoal());
		Integer savedResult=this.userDao.saveOrUpdateFronterBoarddata(frontersBoardData);
		
		return savedResult;
	}
	
	
	/**
	 * Created By Harshitha on may 25th, 2021
	 * @param adminUserId
	 * @param lastWeek
	 * @param WeekNum
	 * @param WeekGoal
	 * @return
	 * @throws CloserBoardDataNotSavedOrUpdatedException
	 * 
	 * Service for t save or update closers board data
	 */
	public Integer saveOrUpdateClosersBoardData(Integer adminUserId,ClosersBoardDataDto closersBoardDataDto) throws CloserBoardDataNotSavedOrUpdatedException {
		log.info("AdminServiceImpl -> saveOrUpdateClosersBoardData ()");
		ClosersBoardData closersBoardData=null;
		try {
			closersBoardData=this.userDao.getClosersBoardData();
			closersBoardData.setUpdationDate(new Date());
		}
		catch(ClosersBoardDataNotFoundException e) {
			closersBoardData=new ClosersBoardData();
			closersBoardData.setCreationDate(new Date());
		}
		try {
			User adminUser=this.userDao.getUserByUserId(adminUserId);
			closersBoardData.setUser(adminUser);
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		closersBoardData.setLastWeek(closersBoardDataDto.getLastWeek());
		closersBoardData.setWeekNum(closersBoardDataDto.getWeekNum());
		closersBoardData.setWeekGoal(closersBoardDataDto.getWeekGoal());
		Integer savedResult=this.userDao.saveOrUpdateClosersBoarddata(closersBoardData);
		
		return savedResult;
	}
	
	/**
	 * Created By Harshitha On May 25th, 2021
	 * @param adminUserId
	 * @param closerId
	 * @param count
	 * @param dayName
	 * @return
	 * @throws CloserNotSavedOrUpdatedException
	 * 
	 * Service for to update closer board by admin
	 */
	
	public Integer updateCloserBoardByAdmin(Integer adminUserId,Integer closerId,Integer count,String dayName) throws CloserNotSavedOrUpdatedException {
		log.info("AdminServiceImpl -> updateCloserBoardByAdmin()");
		Integer updatedResult=0;
			try {
				Closer closer=this.userDao.getCloserByCloserId(closerId);
				try {
					User adminUser=this.userDao.getUserByUserId(adminUserId);
					closer.setAdminUser(adminUser);
				} catch (UserNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				closer.setIsUpdateByAdmin(true);
				closer.setAdminUpdationDate(new Date());
				Integer totalCount=0;
				// monday
				if(dayName.equalsIgnoreCase("MONDAY")) {
					closer.setMon(count);	
				}
				// tuesday
				if(dayName.equalsIgnoreCase("TUESDAY")) {
					closer.setTues(count);	
				}
				// wednesday
				if(dayName.equalsIgnoreCase("WEDNESDAY")) {
					closer.setWed(count);	
				}
				// thursday
				if(dayName.equalsIgnoreCase("THURSDAY")) {
					closer.setThurs(count);	
				}
				// friday
				if(dayName.equalsIgnoreCase("FRIDAY")) {
					closer.setFri(count);	
				}
				// calculating total
				try {
					totalCount=totalCount+closer.getMon();
				}
				catch(NullPointerException e) {
					
				}
				try {
					totalCount=totalCount+closer.getTues();
				}
				catch(NullPointerException e) {
					
				}
				try {
					totalCount=totalCount+closer.getWed();
				}
				catch(NullPointerException e) {
					
				}
				try {
					totalCount=totalCount+closer.getThurs();
				}
				catch(NullPointerException e) {
					
				}
				try {
					totalCount=totalCount+closer.getFri();
				}
				catch(NullPointerException e) {
					
				}
				// end of total calculation
				closer.setTotal(totalCount);
				updatedResult=this.userDao.saveOrUpdateClosers(closer);
				if(updatedResult>0) {
					this.userService.saveOrUpdateCloserGrandTotal();
				}
			} 
			catch (CloserNotFoundException e) {
			log.info("AdminServiceImpl -> updateCloserBoardByAdmin() -> CloserNotFoundException "+e.toString());
			}
		
		return updatedResult;
		
		
	}
	
	/**
	 * Created By Harshitha on May 27th, 2021
	 * @param fronterId
	 * @return
	 * 
	 * Service for to delete the fronter
	 */
	
	public Integer deleteFronter(Integer fronterId) {
		log.info("AdminServiceImpl -> deleteFronter()");
		Integer deletedResult=0;
		
		try {
			Fronter fronter=this.userDao.getFronterByFronterId(fronterId);
			deletedResult=this.adminDao.deleteFronter(fronter);
			this.userService.saveOrUpdateFrontersGrandTotal();
		}
		catch(FronterNotFoundException e) {
		}
		return deletedResult;
	}
	
	/**
	 * Created By Harshitha on May 27th, 2021
	 * @param closerId
	 * @return
	 * 
	 * Service for to delete the closer
	 */
	
	public Integer deleteCloser(Integer closerId) {
		log.info("AdminServiceImpl -> deleteCloser()");
		Integer deletedResult=0;
		
		try {
			Closer closer=this.userDao.getCloserByCloserId(closerId);
			deletedResult=this.adminDao.deleteCloser(closer);
			this.userService.saveOrUpdateCloserGrandTotal();
		}
		catch(CloserNotFoundException e) {
		}
		
		return deletedResult;
	}
	
}