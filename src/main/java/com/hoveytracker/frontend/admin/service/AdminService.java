package com.hoveytracker.frontend.admin.service;

import java.util.ArrayList;

import com.hoveytracker.common.exceptions.CloserBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.CloserNotFoundException;
import com.hoveytracker.common.exceptions.CloserNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotFoundException;
import com.hoveytracker.common.exceptions.DailyScoreBoardNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterBoardDataNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.FronterNotFoundException;
import com.hoveytracker.common.exceptions.FronterNotSavedOrUpdatedException;
import com.hoveytracker.common.exceptions.MailNotSentException;
import com.hoveytracker.common.exceptions.StateNotFoundException;
import com.hoveytracker.common.exceptions.UserNotFoundException;
import com.hoveytracker.common.exceptions.UserNotSavedOrUpdatedException;
import com.hoveytracker.frontend.admin.dto.DailyScoreBoardDto;
import com.hoveytracker.frontend.user.dto.ClosersBoardDataDto;
import com.hoveytracker.frontend.user.dto.FrontersBoardDataDto;
import com.hoveytracker.frontend.user.dto.UserDto;

public interface AdminService{
	
	public Integer saveOrUpdateUser(UserDto userDto) throws UserNotSavedOrUpdatedException ;
	public Integer activateUserFromAccountToken(String token)throws Exception;
	public Integer sendPasswordResetEmail(String email)throws UserNotFoundException, UserNotSavedOrUpdatedException,MailNotSentException;
	public Integer getUserIdByPasswordResetToken(String token)throws UserNotFoundException;
	public Integer handlePasswordReset(Integer userId, String password)throws UserNotFoundException, UserNotSavedOrUpdatedException;
	public ArrayList<String> getListofStates() throws StateNotFoundException;
	public Integer handleAdminPasswordReset(Integer userId, String password)throws UserNotFoundException, UserNotSavedOrUpdatedException;
	public ArrayList<UserDto> getListofUsers(Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws  UserNotFoundException;
	public Integer changeUserAccountStatus(Integer userId,Boolean isActive) throws UserNotFoundException, UserNotSavedOrUpdatedException;
	public Integer saveOrUpdateDailyScoreBoard(DailyScoreBoardDto dailyScoreBoardDto,Integer userId) throws DailyScoreBoardNotSavedOrUpdatedException;
	public DailyScoreBoardDto getDailyScoreBoard() throws DailyScoreBoardNotFoundException;
	public Integer updateFronterBoardByAdmin(Integer adminUserId,Integer fronterId,Integer count,String dayName) throws FronterNotSavedOrUpdatedException;
	public Integer saveOrUpdateFrontersBoardData(Integer adminUserId,FrontersBoardDataDto frontersBoardDataDto) throws FronterBoardDataNotSavedOrUpdatedException;
	
	/**
	 * Created By Harshitha on May 25th, 2021
	 */
	public Integer saveOrUpdateClosersBoardData(Integer adminUserId,ClosersBoardDataDto closersBoardDataDto) throws CloserBoardDataNotSavedOrUpdatedException;
	public Integer updateCloserBoardByAdmin(Integer adminUserId,Integer closerId,Integer count,String dayName) throws CloserNotSavedOrUpdatedException;
	
	/**
	 * Created By Harshitha on May 27th, 2021
	 */
	 public Integer deleteFronter(Integer fronterId);
	 public Integer deleteCloser(Integer closerId);

}