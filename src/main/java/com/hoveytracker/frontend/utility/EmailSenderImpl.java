package com.hoveytracker.frontend.utility;

import java.io.File;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.hoveytracker.common.exceptions.MailNotSentException;
import com.hoveytracker.frontend.user.dto.UserDto;


/**
 * Created By Bhagya On May 10th, 2021
 * @author KNS-ACCONTS
 * Class for email Sender Impl
 */
public class EmailSenderImpl implements EmailSender {
	
private static Logger log=Logger.getLogger(EmailSenderImpl.class);
	
	/**
	 * handles sending mail. Autowired using the bean defined in name mailSender
	 */
	@Autowired
	private JavaMailSender mailSender;

	/**
	 * 
	 * holds the server url (used to build the forgot password link in mail body)
	 * 
	 */
	private String serverHost;
	/**
	 * Template engine. Autowired using the bean defined in the name velocityEngine
	 */
	@Autowired
	private VelocityEngine velocityEngine;

	
	public String getServerHost() {
		return serverHost;
	}


	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}
	
	
	private String imagesPath;
	
		
	public String getImagesPath() {
		return imagesPath;
	}


	public void setImagesPath(String imagesPath) {
		this.imagesPath = imagesPath;
	}
	
	
	
	
	/**
	 * Created By bhagya on may 10th, 2021
	 * Method for send mail to users for forgot mail
	 * @param userDto
	 */
	public void sendForgotPasswordMail(final UserDto userDto) throws MailNotSentException{
		log.info("inside  sendForgotPasswordMail()");
		
		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
				message.setFrom("no-reply@idlemonitor.com");
				message.setTo(userDto.getEmailId());
				message.setSubject("Hovey Energy Tracker Password Reset Link");
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("email", userDto.getEmailId());
				if(userDto.getFirstName()!=null && userDto.getFirstName().trim().length()>0){
					map.put("firstname", userDto.getFirstName());
				}
                if(userDto.getLastName()!=null && userDto.getLastName().trim().length()>0){
					map.put("lastname", userDto.getLastName());
				}
				map.put("passwordToken", userDto.getPasswordToken());
				map.put("user", userDto);
				map.put("serverURL", serverHost);
				
				String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/hoveytracker/templates/forgotPassword.vm","UTF-8", map);
				message.setText(content,true);
        		
				
				  FileSystemResource res = new FileSystemResource(new File( imagesPath+
				  "/logo.png")); message.addInline("logo", res);
				  
				  FileSystemResource res2=new FileSystemResource(new
				  File(imagesPath+"/facebook.png")); message.addInline("facebook", res2);
				  
				  FileSystemResource res3=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("twitter", res3);
				  
				  FileSystemResource res4=new FileSystemResource(new
				  File(imagesPath+"/passwordreset.png")); message.addInline("passwordreset",
				  res4);
				 
				 				 
			}
		};		
		this.mailSender.send(preparator);
	}
	
	
	/**
	 * Created By Bhagya on may 10th, 2021
	 * 
	 * Registration Confirmation Email
	 *
	 */
	public void sendConfirmationMail( final UserDto userDto)throws MailNotSentException{ 
		log.info("inside  EmailSenderImpl -> sendConfirmationMail()");
		
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
		 public void prepare(MimeMessage mimeMessage) throws Exception {            	
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
                message.setFrom("no-reply@idlemonitor.com");
                message.setTo(userDto.getEmailId());             
                message.setSubject("Hovey Energy Tracker Registration Confirmation");                
                Map<String, Object> map=new HashMap<String, Object>();
                map.put("email", userDto.getEmailId());
                if(userDto.getFirstName()!=null && userDto.getFirstName().trim().length()>0){
					map.put("firstname", userDto.getFirstName());
				}
                if(userDto.getLastName()!=null && userDto.getLastName().trim().length()>0){
					map.put("lastname", userDto.getLastName());
				}
              
        		map.put("user", userDto);
        		map.put("serverURL", serverHost);
        		
        		String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/hoveytracker/templates/confirmRegistration.vm","UTF-8", map);
        		
				message.setText(content,true);
				
				  FileSystemResource res = new FileSystemResource(new File( imagesPath+
				  "/logo.png")); message.addInline("logo", res);
				  
				  FileSystemResource res2=new FileSystemResource(new
				  File(imagesPath+"/facebook.png")); message.addInline("facebook", res2);
				  
				  FileSystemResource res3=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("twitter", res3);
				  
				  FileSystemResource res4=new FileSystemResource(new
				  File(imagesPath+"/verify.png")); message.addInline("verify", res4);
				 
				 
				
				        
            }
        };
        this.mailSender.send(preparator);		
	}
	
	
}