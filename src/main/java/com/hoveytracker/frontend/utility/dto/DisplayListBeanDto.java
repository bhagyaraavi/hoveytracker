package com.hoveytracker.frontend.utility.dto;


/***
 * Created by Bhagya on May 10th, 2021
 * Class for DisplayingBeansWithPaginationSearchandSort
 * 
 * @author KNS-ACCONTS
 *
 */
public class DisplayListBeanDto {

	private PagerDto pagerDto=new PagerDto();
	private String sortBy;
	private String searchBy;
	private Boolean sortDirection=true;
	private Integer period;
	private String selectedDate;
	
	
	public Boolean getSortDirection() {
		return sortDirection;
	}
	public void setSortDirection(Boolean sortDirection) {
		this.sortDirection = sortDirection;
	}
	public PagerDto getPagerDto() {
		return pagerDto;
	}
	public void setPagetDto(PagerDto pagerDto) {
		this.pagerDto = pagerDto;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	public Integer getPeriod() {
		return period;
	}
	public void setPeriod(Integer period) {
		this.period = period;
	}
	public String getSelectedDate() {
		return selectedDate;
	}
	public void setSelectedDate(String selectedDate) {
		this.selectedDate = selectedDate;
	}
	
	
	
	
}
