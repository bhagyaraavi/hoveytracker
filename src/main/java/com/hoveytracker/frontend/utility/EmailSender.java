package com.hoveytracker.frontend.utility;

import java.util.ArrayList;

import com.hoveytracker.common.exceptions.MailNotSentException;
import com.hoveytracker.frontend.user.dto.UserDto;




/**
 * Created By bhagya on may 28th, 2020
 * @author KNS-ACCONTS
 * 
 * Interface for email Sender
 *
 */
public interface EmailSender{
	
	public void sendForgotPasswordMail(final UserDto userDto) throws MailNotSentException;
	public void sendConfirmationMail( final UserDto adminUserDto)throws MailNotSentException;
	
}